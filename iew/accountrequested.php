<?php include 'include/config.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sitename; ?></title>
<link rel="stylesheet" href="include/lib/css/main.css" type="text/css" />
<script type="text/javascript" src="<?php echo $level; ?>include/CalendarPopup.js" language="javascript"></script>
<script type="text/javascript" language="javascript">var cal = new CalendarPopup();</script>

<style type="text/css">
<!--
.backg{
background-image:url(images/527back.jpg);
background-repeat:no-repeat;
}
.picture {
	border: 15px #FFFFFF;
}
.textbox {
background-color:#FFCECE;
width:98%;
font-family:Verdana, Arial, Helvetica, sans-serif;
font-size:13px;
font-weight:bold;
}
.smalltext {
font-family:Verdana, Arial, Helvetica, sans-serif;
font-size:10px;
font-weight:bold;
}

-->
</style>

</head>
<body leftmargin="0" topmargin="0" bgcolor="#ffffff">

<?php include 'include/header.php'; ?>
<!------------------------------------------------------------------------------------------------------->

<table align="center" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" width=98% style="margin-top:10px;margin-bottom:10px;">
  <tr bgcolor="#efeed1">
    <td align="left" style="font-weight:bold;">ACCOUNT CREATED</td>
  </tr>  
  <tr valign="top">
    <td align="left" width="100%" bgcolor="#FFFFDD" style="padding-left:20px;padding-top:30px;padding-bottom:30px;">
      <table bgcolor="#cccccc" width="600" align=left border="0" cellpadding="10" cellspacing="1">
        <tr bgcolor="#ffffff" style="line-height:20px;">
          <td valign="top" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:14px;padding-top:20px;padding-bottom:20px;">
Thank you. Your account has been created.  It will be reviewed by an administrator and activated in the next 24 hours.<br /><br />
          </td>
        </tr>
        <tr bgcolor="#ffffff">
          <td valign="top" align="center" style="color:#666666;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;"><a href='http://160.135.123.244/<?php echo $sitefolder ?>'>Main Website</a></td>
        </tr>
        <tr bgcolor="#ffffff">
          <td valign="top" align="center" style="color:#666666;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:9px;">#RD <?php echo $sitename; ?> Webworks <?php echo date('Y'); ?></td>
        </tr>
      </table>
    </td>
  </tr>
</table>   

<!------------------------------------------------------------------------------------------------------->

<?php include 'include/footer.php'; ?>

</body>
</html>