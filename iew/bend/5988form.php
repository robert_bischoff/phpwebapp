<?php

$debug = "off";
$totals = "off";
$textx='a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a';
$texty='b b b b b b b b b b b b b b b';
$the5988id=$_GET['5988id'];
include ('../include/lib/pdf/class.ezpdf.php');

//--------------------------------------------------------------------------------------------------------------------------------
// report pages

if($debug=="on"){
  $pdf->ezText('monthnum:'.$thismonthnum,7);  
  $pdf->ezText('nextmonthnum:'.$nextmonthnum,7);  
  $pdf->ezText('startofmonth:'.$startofmonth,7);
  $pdf->ezText('endofmonth:'.$endofmonth,7);    
}

include "../include/config.php";


$userlogin=$_SESSION['userlogin'];
$usertype=$_SESSION['usertype'];

$data = array();	  
$query1 = "SELECT * FROM `5988` WHERE id = $the5988id";
$result1 = mysql_query($query1) or die ("Error on ".$query1);
$row1 = mysql_fetch_array($result1);
$num_rows1 = mysql_num_rows($result1);
$id=$row1['id']; if($id==''){$id='N/A';}
$workid=$row1['workid']; if($workid==''){$workid='N/A';} 
$workpoc=$row1['workpoc']; if($workpoc==''){$workpoc='N/A';}  
$worktype=$row1['worktype']; if($worktype==''){$worktype='N/A';}
$worksubtype =$row1['worksubtype']; if($worksubtype==''){$worksubtype='N/A';}
$org  =$row1['org']; if($org==''){$org='N/A';}
$status=$row1['jobstatus']; if($status==''){$status='N/A';}
$prioritylevel =$row1['prioritylevel']; if($prioritylevel==''){$prioritylevel='N/A';} 
$title =$row1['jobtitle']; if($title==''){$title='N/A';}     
$taskdesc=$row1['jobdesc']; if($taskdesc==''){$taskdesc='N/A';}
$taskdesc=nl2br($taskdesc);
$taskdescarray = split('<br />',$taskdesc);
foreach ($taskdescarray as $x) {
  $linecount = $linecount + ceil(strlen($x)/100);
  if($linecount<20){
      $taskdesc1.=$x;
  }else{
      $taskdesc2.=$x;  
  }
}

$equipment=$row1['equipment']; if($equipment==''){$equipment='N/A';}
$building=$row1['building']; if($building==''){$building='N/A';}
$room=$row1['room']; if($room==''){$room='N/A';}
$site=$row1['site']; if($site==''){$site='N/A';}
$system=$row1['system']; if($system==''){$system='N/A';}
$funding=$row1['funding']; if($funding==''){$funding='N/A';}
$actiondate=$row1['startdate']; if($actiondate==''){$actiondate='N/A';}
$power=$row1['power']; if($power==0){$power='NONE';}else{$power='YES';}
$bom=$row1['bom']; if($bom==''){$bom='N/A';}
$fund=$row1['fund']; if($fund==''){$fund='N/A';}
$files=$row1['files']; if($files==''){$files='N/A';}
$files=$row1['files']; if($files==''){$files='N/A';}
$created=$row1['started']; if($created=='0000-00-00'){$created='N/A';}
$currentworkorderid=$row1['id'];
$owner=$row1['username'];  

// -------------------------------------------------------------------------------------------------------

$pdf =& new Cezpdf(letter);
$pdf->selectFont('../include/lib/fonts/Helvetica-Bold');
$pdf->ezStartPageNumbers(594,5,10,'','',1);

$pdf->ezSetY(774);

$pdf->setColor(0,0,0);
$pdf->addText(530,763,9,"PD: ".date('m/d/Y'),0);
$pdf->ezText('532ND MILITARY INTELLIGENCE BATTALION - B COMPANY',9,array('justification'=>'center'));
//$pdf->addText(45,763,9,"PD:".date('m/d/Y'),0);
$pdf->addText(20,763,9,"WKEB0",0);

//-----------------------------------------------------------------------------------------

$pdf->ezSetY(755);
$pdf->ezText("----------------------------------------------------------------------------- EQUIPMENT DATA -----------------------------------------------------------------------------",9,array('justification'=>'center'));

//-----------------------------------------------------------------------------------------
$yval=730;
$pdf->addText(33,$yval,9,"ADMIN NUM: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("12345678901234567890",9,array('left'=>70, 'justification'=>'left'));

$pdf->addText(250,$yval,9,"EQUIPMENT SERIAL NUM: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("12345678901234567890",9,array('left'=>340, 'justification'=>'left'));

$yval=$yval-10;

$pdf->addText(33,$yval,9,"EQUIP MODEL: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("12345678901234567890",9,array('left'=>70, 'justification'=>'left'));

$pdf->addText(250,$yval,9,"REGISTRATION NUM: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("12345678901234567890",9,array('left'=>340, 'justification'=>'left'));

$yval=$yval-10;

$pdf->addText(33,$yval,9,"EQUIP NOUN: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("12345678901234567890",9,array('left'=>70, 'justification'=>'left'));

$pdf->addText(250,$yval,9,"TYPE INSPECTION: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("12345678901234567890",9,array('left'=>340, 'justification'=>'left'));

$yval=$yval-10;

$pdf->addText(33,$yval,9,"EQUIP NSN: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("12345678901234567890",9,array('left'=>70, 'justification'=>'left'));

$pdf->addText(250,$yval,9,"CURRENT READING: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("12345678901234567890",9,array('left'=>340, 'justification'=>'left'));

//-------------------------------------------------------------------------------------

$pdf->ezSetY(690);
$pdf->ezText("------------------------------------------------------------------------------ PUBLICATIONS -----------------------------------------------------------------------------",9,array('justification'=>'left'));

$yval=$yval-25;

for($i=0;$i<5;$i++){
$yval=$yval-10;

$pdf->addText(33,$yval,9,"PUBLICATION: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText("123456789012345678901234567890",9,array('left'=>70, 'justification'=>'left'));

$pdf->addText(270,$yval,9,"DATE: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText(date('m/d/Y'),9,array('left'=>269, 'justification'=>'left'));

$pdf->addText(410,$yval,9,"CHANGE NUMBER: ",0);
$pdf->ezSetY(($yval+11));
$pdf->ezText(date('m/d/Y'),9,array('left'=>470, 'justification'=>'left'));
}

//-----------------------------------------------------------------------------------------

$yval=$yval-20;

$pdf->addText(33,$yval,9,"INSPECTORS LIC#: ",0);
$pdf->ezSetY(($yval+10.5));
$pdf->ezText("1234567890",9,array('left'=>90, 'justification'=>'left'));

$pdf->addText(190,$yval,9,"TIME: ",0);
$pdf->ezSetY(($yval+10.5));
$pdf->ezText(date('m/d/Y'),9,array('left'=>187, 'justification'=>'left'));

$pdf->addText(290,$yval,9,"SIGNATURE: ",0);
$pdf->ezSetY(($yval+10.5));
$pdf->ezText(date('m/d/Y'),9,array('left'=>319, 'justification'=>'left'));

$pdf->addText(430,$yval,9,"TIME: ",0);
$pdf->ezSetY(($yval+10.5));
$pdf->ezText(date('m/d/Y'),9,array('left'=>427, 'justification'=>'left'));

//$pdf->addText(20,647,7,"7. REQUIRMENT: (Impact to mission if disapproved) ",0);
//$pdf->ezSetY(640);
//$pdf->ezText($taskdesc1,11,array('right'=>0, 'justification'=>'left'));
//if($taskdesc2!=''){
//  $pdf->ezText(" ",7,array('left'=>5, 'right'=>0, 'justification'=>'center'));
//  $pdf->ezText("( continued on next page... )",8,array('left'=>5, 'right'=>0, 'justification'=>'center'));
//}

//-----------------------------------------------------------------------------------------

$pdf->ezSetY(600);
$pdf->ezText("------------------------------------------------------------------------------ PARTS REQUESTED --------------------------------------------------------------------------",9,array('justification'=>'left'));

$yval=$yval-27;
$pdf->addText(33,$yval,9,"FAULT                DOC NUM                NIIN                NOUN                QTY DUE/REC                STATUS DATE                DATE COMP ",0);

$yval=$yval-3;

for($i=0;$i<rand(5,20);$i++){
	$yval=$yval-12;
	$pdf->addText(33,$yval,9,"1234567890",0);
	$pdf->addText(102,$yval,9,"1234567890",0);
	$pdf->addText(185,$yval,9,"1234567890",0);
	$pdf->addText(243,$yval,9,"1234567890",0);
	$pdf->addText(310,$yval,9,"1234567890",0);
	$pdf->addText(412,$yval,9,"1234567890",0);
	$pdf->addText(514,$yval,9,"1234567890",0);
}

//-----------------------------------------------------------------------------------------

$pdf->ezSetY($yval-10);
$pdf->ezText("------------------------------------------------------------------------------ MAINTENANCE FAULTS ---------------------------------------------------------------------",9,array('justification'=>'left'));

$yval=$yval-33;
$pdf->addText(33,$yval,9,"ITEM NUM        FAULT DATE        FAULT STATUS        FAULT DESC        CORRECTIVE ACTION             HOURS           OPER LIC#",0);

$yval=$yval-3;

for($i=0;$i<rand(10,24);$i++){
	$yval=$yval-12;
	$pdf->addText(33,$yval,9,$i,0);
	$pdf->addText(97,$yval,9,"1234567890",0);
	$pdf->addText(173,$yval,9,"1234567890",0);
	$pdf->addText(262,$yval,9,"1234567890",0);
	$pdf->addText(340,$yval,9,"1234567890",0);
	$pdf->addText(468,$yval,9,"1234567890",0);
	$pdf->addText(528,$yval,9,"1234567890",0);
}

//-----------------------------------------------------------------------------------------

$pdf->setColor(0.9,0.9,0.9);
$pdf->filledrectangle(18,18,575,72);
$pdf->rectangle(18,18,575,72);
$pdf->setColor(0,0,0);
$pdf->addText(20,82,7,"REMARKS: ",0);

//-----------------------------------------------------------------------------------------

$pdf->ezSetY(40);
$pdf->setColor(0.5,0.5,0.5);
$pdf->addText(18,5,10,'532ND MI BDE IEW FORM '.date('mdYHis'),0);

//---------------------------------------------------------output-----------------------------------------------------------------

$pdf->ezStream();
$pdfcode = $pdf->output();
fclose($fp);
?>