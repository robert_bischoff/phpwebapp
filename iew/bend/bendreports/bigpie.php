<?php
include '../../include/lib/php-ofc-library/open-flash-chart.php';
include "../../include/config.php";
$connessione = mysql_connect($DBhost,$DBuser,$DBpass);
@mysql_select_db("$DBname");

$sql1 = "SELECT count(item) FROM `targetdate_dessert` UNION ALL ";
$sql1 .= "SELECT count(item) FROM `targetdate_main` UNION ALL ";
$sql1 .= "SELECT count(item) FROM `targetdate_salad` UNION ALL ";
$sql1 .= "SELECT count(item) FROM `targetdate_short` UNION ALL ";
$sql1 .= "SELECT count(item) FROM `targetdate_side`";
$result1 = mysql_query($sql1);

$num_row1 = mysql_num_rows($result1);
for($i=0;$i<$num_row1;$i++){
  $row1 = mysql_fetch_array($result1);
  $totalrows = $totalrows + $row1[0];
}

$title = new title( "TOP 10 FOODS ALL DATA (".$totalrows." items)" );
$title->set_style('color: #000000; font-size: 18px; font-weight:bold;');

$sql2 = "SELECT `item`, count(item) FROM `targetdate_dessert` GROUP BY `item` UNION ALL ";
$sql2 .= "SELECT `item`, count(item) FROM `targetdate_main` GROUP BY `item` UNION ALL ";
$sql2 .= "SELECT `item`, count(item) FROM `targetdate_salad` GROUP BY `item` UNION ALL ";
$sql2 .= "SELECT `item`, count(item) FROM `targetdate_short` GROUP BY `item` UNION ALL ";
$sql2 .= "SELECT `item`, count(item) FROM `targetdate_side` GROUP BY `item` ORDER BY `count(item)` DESC LIMIT 50";	
					  
$result2 = mysql_query($sql2);
$num_row2 = mysql_num_rows($result2);

$data = array();

for($i=0;$i<$num_row2;$i++){
  $row2 = mysql_fetch_array($result2);
  $tmp = new pie_value(($row2[1]/$totalrows*100), "");
  if($i<=9){
    $tmp->set_label(strtoupper($row2[0]), '#432BAF', 18 );
  }
  $data[] = $tmp;
}

$pie = new pie();
$pie->start_angle(0)
    ->add_animation( new pie_fade())
    ->add_animation( new pie_bounce(10))
    //->gradient_fill()
    ->tooltip("#val#%")
    ->colours(
        array(
            '#990000',    // <-- blue
            '#348484',    // <-- grey
            '#336699',    // <-- grey			
            '#356aa0',    // <-- green
            '#C79810',    // <-- yellow	
            '#000000',    // <-- yellow
            '#616D7E',    // <-- yellow
            '#25383C',    // <-- yellow
            '#15317E',    // <-- yellow
            '#98AFC7',    // <-- yellow
            '#D2B9D3',    // <-- yellow
            '#717D7D',    // <-- yellow
            '#41627E',    // <-- yellow
            '#8BB381',    // <-- yellow
            '#EDE275',    // <-- yellow
            '#C9BE62',    // <-- yellow
            '#7E3817',    // <-- yellow
            '#C47451',    // <-- yellow
            '#7F525D',    // <-- yellow
            '#C8B560'    // <-- yellow		
        )
		);
$pie->set_values( $data );
$chart = new open_flash_chart();
$chart->set_title( $title );
$chart->add_element( $pie );
echo $chart->toPrettyString();
?>