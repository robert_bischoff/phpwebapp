<?php
  include_once '../include/config.php';
  include_once '../include/functions.php';
  session_start();
  if (login_check($conn) == true) :
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php echo $sitename; ?></title>
<meta http-equiv=content-type content="text/html; charset=UTF-8">
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/thickbox.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="../include/lib/js/jquery.js"></script>
<script>
	var $j = jQuery.noConflict();
</script>
<script type="text/javascript" src="../include/lib/js/thickbox.js"></script>
<script src="../include/lib/js/prototype.js" type="text/javascript"></script> 
<script src="../include/lib/js/menu.js" type="text/javascript"></script>

<script type="text/javascript">
  function goURL(theUrl){
    top.document.location.href = theUrl;
  }
</script>

<script language="javascript" type="text/javascript">
  function MM_openBrWindow(theURL,winName,features) { //v2.0
    window.open(theURL,winName,features);
  }
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }
</script>

<script language="JavaScript">
<!--
function confirmdelete(menudate)
	{	
	  if (confirm('Delete menu for '+menudate+'?    ')){
	    return true;
	    window.location.reload(true);
	  } else {  
	    return false;
	  }  
    }
  //-->
</script>
<body bgcolor="#cccccc">
<?php include '../include/bendheader.php'; ?>
<!-- ------------------------------  content start -------------------------------------- -->
<div style="width:1024;background-color:#FFFFFF;margin-left:0px;margin-top:-11px;top:0px;height:900px;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
Blank App
</div>
<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>