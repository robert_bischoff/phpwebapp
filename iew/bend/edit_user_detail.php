<?php
 include_once '../include/config.php';
  include_once '../include/functions.php';
  session_start();
  if (login_check($conn) == true) :
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sitename; ?></title>
<link href="../include/lib/css/main.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../include/lib/js/CalendarPopup.js" language="javascript"></script>
<script type="text/javascript" language="javascript">var cal = new CalendarPopup();</script>
  
<script src="../include/lib/js/prototype.js" type="text/javascript"></script> 
<script src="../include/lib/js/menu.js" type="text/javascript"></script>

</head>
<body bgcolor="#cccccc">
<?php
  //print_r($_POST);

  include '../include/bendheader.php';
  
  
  $action = $_GET['act'];
  
  if($_POST['username']!=''){$_POST['edituser']=$_POST['username'];}
  
  $sqludd = "SELECT `unit` from `dd_unit`";
  $resultudd = mysql_query($sqludd);
  $uddarray = array();
  while ($x = mysql_fetch_array($resultudd)){
   $uddarray[] = current($x);
  }
  
  $sqlrank = "SELECT `rank` from `dd_rank`";
  $resultrank = mysql_query($sqlrank);
  $rankarray = array();
  while ($x = mysql_fetch_array($resultrank)){
   $rankarray[] = current($x);
  }  

  $sqlusers = "SELECT `username` from `user` WHERE (`usertype`='admin' OR `usertype`='simo')";
  $resultusers = mysql_query($sqlusers);
  $usersarray = array();
  while ($x = mysql_fetch_array($resultusers)){
   $usersarray[] = current($x);
  }

  $sql1 = "SELECT * from `user` WHERE `username`='".$_POST['edituser']."'";
  //echo $sql1."<br>";  
  $result1 = mysql_query($sql1);
  $row = mysql_fetch_array($result1);

  if($_POST['action']=='save'){
	if($row['pass']!=$_POST['pass']){
      $sql0 = "UPDATE `user` SET `username`='".strtolower($_POST['username'])."', `usertype`='".strtolower($_POST['usertype'])."', `goto`='".strtolower($_POST['goto'])."', `tasking`='".strtolower($_POST['tasking'])."', `unit`='".strtolower($_POST['unit'])."', `rank`='".strtolower($_POST['rank'])."', `first`='".strtolower($_POST['first'])."', `last`='".strtolower($_POST['last'])."', `dsn`='".strtolower($_POST['dsn'])."', `email`='".strtolower($_POST['email'])."@mi.army.mil', `deros`='".strtolower($_POST['deros'])."', `pass`=PASSWORD('".strtolower($_POST['pass'])."') WHERE `id`='".$_POST['id']."'";
	}else{
      $sql0 = "UPDATE `user` SET `username`='".strtolower($_POST['username'])."', `usertype`='".strtolower($_POST['usertype'])."', `goto`='".strtolower($_POST['goto'])."', `tasking`='".strtolower($_POST['tasking'])."', `unit`='".strtolower($_POST['unit'])."', `rank`='".strtolower($_POST['rank'])."', `first`='".strtolower($_POST['first'])."', `last`='".strtolower($_POST['last'])."', `dsn`='".strtolower($_POST['dsn'])."', `email`='".strtolower($_POST['email'])."@mi.army.mil', `deros`='".strtolower($_POST['deros'])."' WHERE `id`='".$_POST['id']."'";
	}
	//echo $sql0."<br>";
    mysql_query($sql0);
    $action = 'saved';
  }
  
  $sql1 = "SELECT * from `user` WHERE `username`='".$_POST['edituser']."'";
  //echo $sql1."<br>";  
  $result1 = mysql_query($sql1);
  $row = mysql_fetch_array($result1);  
?>

<!-- ------------------------------  content start -------------------------------------- -->
<div style="width:1024;margin-left:0px;margin-top:-11px;height:50%;background:#FFFFFF;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
<form method="post" name="edituserform">
<input type=hidden name=action value=save>
<input type=hidden name=id value="<?php echo $_POST['id']; ?>">
<table align="center">
  <tr>
    <td style='padding-top:6px;'>
      <table width='100%' bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0" >        
        <tr bgcolor="#FFFFFF" style="font-size:17px; font-weight:bold;line-height:12px;">
          <td width=700 valign="middle" style="padding-left:5px;padding-top:5px;font-size:20px;">EDIT USER INFORMATION FOR <?php echo strtoupper($_POST['edituser']); ?></td>
          <?php
		    if($action=='saved'){
              echo "<td valign='middle' align='right'><a href='edit_users.php'><img src='../images/backani.gif' border='0' width='170' height='30'></a></td>";
			}else{
              echo "<td valign='middle' align='right'><a href='edit_users.php'><img src='../images/back.gif' border='0' width='170' height='30'></a></td>";			
			}
		  ?>
        </tr>  
      </table>
    </td>
  </tr>      
  <tr>
    <td width='100%' align="center"> 
      <table width='99%' bgcolor="#666666" border="0" cellpadding="5" cellspacing="1">    
        <tr>
          <td bgcolor="#FFFFCC" align="left" width='150' style="font-size:12px; font-weight:bold;">Username</td>
          <td bgcolor="#DDDDDD" width='670'><input type="text" name=username style="font-size:12px;width:100%;font-weight:bold;" value="<?php echo ucfirst($row['username']); ?>"></td>
          <td bgcolor="#FFFFCC" width='150'><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>          
        </tr>     
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Usertype</td>
          <?php
		    //print_r($row);
            echo "<td align=left bgcolor=#DDDDDD>\n";
              echo "<select name='usertype' style='width:100%;font-weight:bold;' onChange='submit();'>\n";
              $queryut = "SELECT `usertype` FROM dd_usertype";
              $resultut = mysql_query($queryut);
	          while ($srow = mysql_fetch_array($resultut)){
			    if($srow['usertype']==strtoupper($row[2])){$selected = "selected";}else{$selected="";}
		        echo "<option value=\"$srow[usertype]\" $selected>$srow[usertype]</option>\n";
              }
              echo "</select>\n";
           	echo "</td>\n";
		  ?>
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>          
        </tr>
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Goto</td>
          <td bgcolor="#DDDDDD"><input type=text name=goto style="font-size:12px;width:100%;font-weight:bold;" value="<?php echo ucfirst($row['goto']); ?>"></td>
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>
        </tr> 
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Add task</td>
          <?php
              if($row['4']!=''){$selected='selected';}else{$selected='';$row[4]='&nbsp;';}
        	  echo "<td align=left bgcolor='#DDDDDD'>\n";
              echo "<select name='tasking' style='width:100%;font-size:12px;font-weight:bold;' onChange='submit();'>\n";
              echo "<option value=\"\" $selected></option>\n";	
              echo "<option value=\"email\" $selected>Email</option>\n";	
              echo "</select>\n";
        	  echo "</td>\n";		  
		  ?>
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>  
        </tr>  
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Unit</td>
          <?php
              echo "<td align=left bgcolor=#DDDDDD>\n";			
              echo "<select name='unit' style='width:100%;font-size:12px;font-weight:bold;' onChange='submit();'>\n";
	          for($j=0;$j<count($uddarray);$j++){
			    if($uddarray[$j]==$row['5']){$selected = "selected";}else{$selected="";}
		        echo "<option value=\"".$uddarray[$j]."\" $selected>".$uddarray[$j]."</option>\n";
              }
              echo "</select>\n";
        	  echo "</td>\n";
		  ?>
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>      
        </tr> 
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Rank</td>
          <?php
              echo "<td align=left bgcolor=#DDDDDD>\n";			
              echo "<select name='rank' style='width:100%;font-size:12px;font-weight:bold;' onChange='submit();'>\n";
	          for($j=0;$j<count($rankarray);$j++){
			    if(strtoupper($rankarray[$j])==strtoupper($row['6'])){$selected = "selected";}else{$selected="";}
		        echo "<option value=\"".$rankarray[$j]."\" $selected>".$rankarray[$j]."</option>\n";
              }
              echo "</select>\n";				
        	  echo "</td>\n";			  
		  ?>
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>   
        </tr> 
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">First Name</td>
          <td bgcolor="#DDDDDD"><input type=text name=first style="font-size:12px;width:100%;font-weight:bold;" value="<?php echo ucfirst($row['first']); ?>"></td>
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>
        </tr> 
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Last Name</td>
          <td bgcolor="#DDDDDD"><input type=text name=last style="font-size:12px;width:100%;font-weight:bold;" value="<?php echo ucfirst($row['last']); ?>"></td>
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>
        </tr> 
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">DSN</td>
          <td bgcolor="#DDDDDD"><input type=text name=dsn style="font-size:12px;width:100%;font-weight:bold;" value="<?php echo ucfirst($row['dsn']); ?>"></td>
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>   
        </tr>
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Email</td>
          <td bgcolor="#DDDDDD">
            <table border="0" cellpadding="0" cellspacing="0" width=700 bgcolor="#DDDDDD">
              <tr>
                <td bgcolor="#DDDDDD"><input type=text name=email style="font-size:12px;width:100%;font-weight:bold;" value="<?php echo str_replace('@mi.army.mil','',ucfirst($row['email'])); ?>"></td>
                <td bgcolor="#DDDDDD" style="font-size:11px;font-weight:bold;" width=65%>&nbsp;@MI.ARMY.MIL (must be your MI email account)</td>               
              </tr>
            </table>
          </td>                
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>   
        </tr>
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Deros</td>
          <td bgcolor="#DDDDDD">
            <table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor="#DDDDDD">
              <tr>
                <td width=90% bgcolor="#DDDDDD"><input type=text name="deros" style="font-size:12px;width:100%;font-weight:bold;" value="<?php echo ucfirst($row['deros']); ?>"></td>
                <td width=10% bgcolor="#41627E" align="center"><a href="#" onClick="cal.select(document.forms['edituserform'].deros,'anchor1','yyyy-MM-dd'); return false;" NAME="anchor1" ID="anchor1"><img src="../images/calendar.png" border="0" align="bottom"></a></td>                   
              </tr>
            </table>
          </td>           
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>   
        </tr>
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Password</td>
          <td bgcolor="#DDDDDD">
            <table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor="#DDDDDD">
              <tr>
                <td bgcolor="#DDDDDD"><input type=password name=pass style="font-size:12px;width:100%;font-weight:bold;" value="<?php echo ucfirst($row['pass']); ?>"></td>      
              </tr>
            </table>
          </td>          
          <td bgcolor="#FFFFCC"><input type="submit" value="Save Changes" style="font-size:11px;width:100%;font-weight:bold;padding:2px;"></td>
        </tr>        
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Emailcheck</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['emailcheck']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr>                                                                                   
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Active</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['active']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr>          
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Rejected</td>   
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['rejected']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr>
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Requested</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['requested']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr>
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Confirmed</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['confirmed']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr>                  
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Opened</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['opened']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr> 
        <tr>   
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Closed</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['closed']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr> 
        <tr style="font-size:12px;">          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Grantedby</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['grantedby']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr> 
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Lastlogin</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['lastlogin']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr> 
        <tr>          
          <td bgcolor="#FFFFCC" align="left" style="font-size:12px; font-weight:bold;">Timestamp</td>
          <td bgcolor="#DDDDDD"><input type=text style="font-size:12px;width:100%;background-color:#CCCCCC;" value="<?php echo $row['timestamp']; ?>" readonly></td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr>       
      </table>
    </td>
  </tr>  
  <tr><td><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td></tr>
</table>
</form>
</div>

<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>