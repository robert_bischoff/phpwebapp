<?php
 include_once '../include/config.php';
  include_once '../include/functions.php';
  session_start();
  if (login_check($conn) == true) :
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo strtoupper($sitename); ?></title>
<link href="../include/lib/css/main.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
  
<script src="../include/lib/js/prototype.js" type="text/javascript"></script> 
<script src="../include/lib/js/menu.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--
function confirmdelete(username)
	{	
	  if (confirm('Permently delete '+username+'?')){
	    return true;
	    window.location.reload(true);
	  } else {  
	    return false;
	  }  
    }
  //-->
</script>
<SCRIPT LANGUAGE="JavaScript">
<!--
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }	
  //-->
</script>

</head>
<body bgcolor="#cccccc">
<?php
  //print_r($_POST);

  include '../include/bendheader.php';

  $action = $_GET['act'];
    
  $sqludd = "SELECT `unit` from `dd_unit`";
  $resultudd = mysql_query($sqludd);
  $uddarray = array();
  while ($x = mysql_fetch_array($resultudd)){
   $uddarray[] = current($x);
  }

  $sqlusers = "SELECT `username` from `user` WHERE (`usertype`='admin' OR `usertype`='super')";
  $resultusers = mysql_query($sqlusers);
  $usersarray = array();
  while ($x = mysql_fetch_array($resultusers)){
   $usersarray[] = current($x);
  }

  if($_POST['action']=='savetype'){
    $sql0 = "UPDATE `user` SET `usertype`='".$_POST['usertype']."' WHERE `id`='".$_POST['id']."'";
	//echo $sql0."<br><br>";
    mysql_query($sql0);
    $action = '';
  }

  if($_POST['action']=='saveunit'){
    $sql0 = "UPDATE `user` SET `unit`='".$_POST['unit']."' WHERE `id`='".$_POST['id']."'";
	//echo $sql0."<br><br>";
    mysql_query($sql0);
    $action = '';
  }

  if($_POST['action']=='savegoto'){
    $sql0 = "UPDATE `user` SET `goto`='".$_POST['goto']."' WHERE `id`='".$_POST['id']."'";
	//echo $sql0."<br><br>";
    mysql_query($sql0);
    $action = '';
  }

  if(md5('deluser')==$action){
    $sql="DELETE FROM `user` WHERE `id`='".$_GET['id']."'";
    mysql_query($sql);
    $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);
    $sqlact="INSERT INTO `watchdog_action_log` VALUES (NULL, '".$_SESSION['username']."', '20', 'User deleted ".$_GET['id']."', '".$_SERVER['REMOTE_ADDR']."', '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."', '".$nowtime."')";
    mysql_query($sqlact);	
  }

  if(md5('delnone')==$action){
    $sql="DELETE FROM `users_addons` WHERE `id`='".$_GET['id']."'";
    mysql_query($sql);
    $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);
    $sqlact="INSERT INTO `watchdog_action_log` VALUES (NULL, '".$_SESSION['username']."', '21', 'User deleted addon ".$_GET['id']."', '".$_SERVER['REMOTE_ADDR']."', '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."', '".$nowtime."')";
    mysql_query($sqlact);	
  }

  if(md5('makeactive')==$action){
    $sql="UPDATE `user` SET `active`='1' WHERE `id`='".$_GET['id']."'";
    mysql_query($sql);
    $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);
    $sqlact="INSERT INTO `watchdog_action_log` VALUES (NULL, '".$_SESSION['username']."', '22', 'User activated by ".strtoupper($_SESSION['username'])."', '".$_SERVER['REMOTE_ADDR']."', '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."', '".$nowtime."')";
    mysql_query($sqlact);
  }

  if(md5('makeinactive')==$action){
    $sql="UPDATE `user` SET `active`='0' WHERE `id`='".$_GET['id']."'";
    mysql_query($sql);
    $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);
    $sqlact="INSERT INTO `watchdog_action_log` VALUES (NULL, '".$_SESSION['username']."', '23', 'User de-activated by ".strtoupper($_SESSION['username'])."', '".$_SERVER['REMOTE_ADDR']."', '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."', '".$nowtime."')";
    mysql_query($sqlact);	
  }  
    
  if($_POST['action']=='changepass' && $_POST['cpass']!=''){
    $sql0 = "UPDATE `user` SET `pass` = PASSWORD('".$_POST['newpass']."') WHERE `id`='".$_POST['id']."'";
	//echo $sql0;
    mysql_query($sql0);
    $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);	
    $sqlact="INSERT INTO `watchdog_action_log` VALUES (NULL, '".$_SESSION['username']."', '24', 'User changed password ".strtoupper($_SESSION['username'])."', '".$_SERVER['REMOTE_ADDR']."', '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."', '".$nowtime."')";
    mysql_query($sqlact);			
    $changepass='done';
  }else{
    $changepass='error';
  }

  $sqlt = "SELECT * from `user` WHERE `usertype`='tech' ORDER BY `last`";
  $resultt = mysql_query($sqlt);
  $num_rowst=mysql_num_rows($resultt);
  //echo $num_rowsz;

  $sqlx = "SELECT * from `user` WHERE `usertype`='admin' ORDER BY `last`";
  $resultx = mysql_query($sqlx);
  $num_rowsx=mysql_num_rows($resultx);
  //echo $num_rowsx; 
  
  $sqly = "SELECT * from `user` WHERE `usertype`='super' ORDER BY `last`";
  $resulty = mysql_query($sqly);
  $num_rowsy=mysql_num_rows($resulty);
  //echo $num_rowsy;   

  $sqlz = "SELECT * from `user` WHERE `usertype`='user' ORDER BY `last`";
  $resultz = mysql_query($sqlz);
  $num_rowsz=mysql_num_rows($resultz);
  //echo $num_rowsz;
  
  $sqla = "SELECT * from `users_addons` ORDER BY `last`";
  $resulta = mysql_query($sqla);
  $num_rowsa=mysql_num_rows($resulta);
  //echo $num_rowsz;  
?>

<!-- ------------------------------  content start -------------------------------------- -->
<div style="width:1024;margin-left:0px;margin-top:-11px;height:50%;background:#FFFFFF;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
<table>
  <tr>
    <td width='100%' align="center"> 
      <table width='100%' bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0" >        
      <tr bgcolor="#FFFFFF" style="font-size:17px; font-weight:bold;line-height:12px;"><td><br>ADMIN</td></tr>  
      </table>
      <table width='99%' bgcolor="#666666" border="0" cellpadding="4" cellspacing="1" >    
        <tr bgcolor="#CCCCCC" style="font-size:12px;">
          <td width='150'>Username</td>
          <td width='200'>User</td>
          <td width='66'>Usertype</td>
          <td width='66'>Unit</td>
          <td width='190'>Supervisor</td>          
          <td width='220'>Password</td>
          <td width='80'>Deros</td>
          <td width='1%' align=center>Active</td>
          <td width='1%' align=center>Del</td>
        </tr>
        <?php
          for($i=0;$i<=$num_rowsx;$i++){
              $row = mysql_fetch_array($resultx);
        	  //print_r($row);
        	  if ($i%2==0) {$rowcolor='#DDDDDD';}else{$rowcolor='#EEEEEE';}			
              if ($row[1]!='' && $row[1]!='admin'){
                if ($_POST['action']=='changepass' && $_POST['id']==$row[0]){
                  echo "<tr bgcolor=#C3FDB8 style='font-size:12px;line-height:15px;'>";					
				}else{
                  echo "<tr id=\"cell1$i\" onmouseover=\"changeColor('#C9C299', this.id);\" onmouseout=\"changeColor('$rowcolor', this.id);\" bgcolor=$rowcolor style='font-size:12px;line-height:15px;'>";
				}
				if(strtolower($_SESSION['username'])==strtolower($row[1])){$currusercolor="#FFFFCC";}else{$currusercolor=$bgcolor;}
        		echo "<td onclick=\"javascript:document.forms['edituserone$i'].submit();\" align=left style='font-size:11px;'><a style='text-decoration:underline;color:blue;cursor:pointer;font-weight:bold;'>".ucfirst($row[8]).", ".ucfirst($row[7])."</a></td>\n";
        		echo "<td onclick=\"javascript:document.forms['edituserone$i'].submit();\" align=left><b>".$row[1]."</b></td>\n";


			    echo "<form method='post' name='admincp$i'>\n";
			    echo "<input type='hidden' name='action' value='savetype'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";
        		echo "<td align=left>\n";
                echo "<select name='usertype' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
                $queryut = "SELECT `usertype` FROM dd_usertype";
                $resultut = mysql_query($queryut);
	            while ($srow = mysql_fetch_array($resultut)){
			      if($srow['usertype']=='ADMIN'){ $selected = "selected";}else{$selected="";}
		          echo "<option value=\"$srow[usertype]\" $selected>$srow[usertype]</option>\n";
                }
                echo "</select>\n";
        		echo "</td>\n";
			    echo "</form>\n";

                //unit
			    echo "<form method='post' name='unit$i'>\n";
			    echo "<input type='hidden' name='action' value='saveunit'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";				
        		echo "<td align=left>\n";			
                echo "<select name='unit' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
	            for($j=0;$j<count($uddarray);$j++){
			      if($uddarray[$j]==$row['5']){$selected = "selected";}else{$selected="";}
		          echo "<option value=\"".$uddarray[$j]."\" $selected>".$uddarray[$j]."</option>\n";
                }
                echo "</select>\n";				
        		echo "</td>\n";
        		echo "</form>\n";

                if($row['3']!=''){$selected='selected';}else{$selected='';$row[3]='&nbsp;';}
		        if($_POST['id']==$row[0] && $_POST['action']=='changepass'  && $changepass='done'){
        		  echo "<td align=left></td>";
			    }else{
        		  echo "<td align=left></td>";					
				}
				
				
				if($_SESSION['usertype']=='admin' || strtolower($_SESSION['username'])==strtolower($row[1])){
                  if($_POST['action']=='changepass' && $_POST['id']==$row[0] && $changepass=='error'){$bgcolor="bgcolor=#C11B17";}else{$bgcolor="";}					  					
        	   	  echo "
				  <form name='changepass1' method='post'>
				  <td $bgcolor align=center align=left>
				  <input type=hidden name=action value='changepass'>
                  <input type=hidden name=id value='".$row[0]."'>
				  <input type=hidden name=lastname value='".$row[8]."'>
				  <input type=checkbox name=cpass value='".$row[0]."'>					
				  <input type=password name=newpass style='font-size:10px;background-color:#FFFFFF'>
				  <input type='submit' value='Change' style='font-size:10px;'>				
				  </td>
				  </form>\n";
				}else{
				  echo "<td align=center align=left>&nbsp;</td>";
				}
        		echo "<td align=center align=left style='font-size:10px;font-weight:bold;'>".$row[14]."</td>\n";
                if($row['12']=='1'){
        		  echo "<td align=center><a href='?id=".$row[0]."&act=".md5('makeinactive')."'><img src='../images/checked.gif' border=0></a></td>\n";
				}else{
        		  echo "<td align=center><a href='?id=".$row[0]."&act=".md5('makeactive')."'><img src='../images/unchecked.gif' border=0></a></td>\n";
				}
        		echo "<td align=center><a href='?id=".$row[0]."&act=".md5('deluser')."' onClick='return confirmdelete(\"".strtoupper($row[1])."\")'><img src='../images/delete.jpg' border=0></a></td>\n";
                echo "</tr>\n";
                echo "</form>\n\n\n";

                echo "<form name='edituserone$i' action='edit_user_detail.php' method=POST>";
                echo "<input type='hidden' name='id' value='".$row[0]."'>";				
                echo "<input type='hidden' name='edituser' value='".$row[1]."'>";
			    echo "</form>";
        	  }
          }
        ?>
      </table>
    </td>
  </tr>

  <!-- SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER SUPER  -->

  <tr>
    <td width='100%' align="center"> 
      <table width='100%' bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0" >        
      <tr bgcolor="#FFFFFF" style="font-size:17px; font-weight:bold;line-height:12px;"><td><br>SUPERVISOR</td></tr>  
      </table>
      <table width='99%' bgcolor="#666666" border="0" cellpadding="4" cellspacing="1" >    
        <tr bgcolor="#CCCCCC" style="font-size:12px;">
          <td width='150'>Username</td>
          <td width='200'>User</td>
          <td width='66'>Usertype</td>
          <td width='66'>Unit</td>
          <td width='190'>Supervisor</td>          
          <td width='220'>Password</td>
          <td width='80'>Deros</td>
          <td width='1%' align=center>Active</td>
          <td width='1%' align=center>Delx</td>
        </tr>
        <?php
          for($i=0;$i<=$num_rowsy;$i++){
              $row = mysql_fetch_array($resulty);
        	  //print_r($row);
        	  if ($i%2==0) {$rowcolor='#DDDDDD';}else{$rowcolor='#EEEEEE';}	
              if ($row[1]!='' && $row[1]!='super'){
                if ($_POST['action']=='changepass' && $_POST['id']==$row[0]){
                  echo "<tr bgcolor=#C3FDB8 style='font-size:12px;line-height:15px;'>";					
				}else{
                  echo "<tr id=\"cell2$i\" onmouseover=\"changeColor('#C9C299', this.id);\" onmouseout=\"changeColor('$rowcolor', this.id);\" bgcolor=$rowcolor style='font-size:12px;line-height:15px;'>";
				}
				if(strtolower($_SESSION['username'])==strtolower($row[1])){$currusercolor="#FFFFCC";}else{$currusercolor=$bgcolor;}
                //if(strtolower($_SESSION['username'])==strtolower($row[1])){$meicon=" <img src='../images/happy.gif'>";}else{$meicon="";}
        		echo "<td onclick=\"javascript:document.forms['editusertwo$i'].submit();\" align=left style='font-size:11px;'><a style='text-decoration:underline;color:blue;cursor:pointer;font-weight:bold;'>".ucfirst($row[8]).", ".ucfirst($row[7])."</a></td>\n";
        		echo "<td align=left><b>".$row[1]."</b></td>\n";
				
			    echo "<form method='post' name='superusercp$i'>\n";
			    echo "<input type='hidden' name='action' value='savetype'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";					
				
        		echo "<td align=left>\n";
                echo "<select name='usertype' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
                $queryut = "SELECT `usertype` FROM dd_usertype";
                $resultut = mysql_query($queryut);
	            while ($srow = mysql_fetch_array($resultut)){
			      if($srow['usertype']=='SUPER'){ $selected = "selected";}else{$selected="";}
		          echo "<option value=\"$srow[usertype]\" $selected>$srow[usertype]</option>\n";
                }
                echo "</select>\n";
        		echo "</td>\n";
				echo "</form>";

                //unit
			    echo "<form method='post' name='unit$i'>\n";
			    echo "<input type='hidden' name='action' value='saveunit'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";				
        		echo "<td align=left>\n";			
                echo "<select name='unit' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
	            for($j=0;$j<count($uddarray);$j++){
			      if($uddarray[$j]==$row['5']){$selected = "selected";}else{$selected="";}
		          echo "<option value=\"".$uddarray[$j]."\" $selected>".$uddarray[$j]."</option>\n";
                }
                echo "</select>\n";				
        		echo "</td>\n";
        		echo "</form>\n";
				
                if($row['3']!=''){$selected='selected';}else{$selected='';$row[3]='&nbsp;';}
        		echo "<td align=left>\n";			
        		echo "</td>\n";	
				
				if($_SESSION['usertype']=='admin'){
				  if($_POST['action']=='changepass' && $_POST['id']==$row[0] && $changepass=='error'){$bgcolor="bgcolor=#C11B17";}else{$bgcolor="";}
        	   	  echo "
				  <form name='changepass2' method='post'>
				  <td $bgcolor align=center align=left>
				  <input type=hidden name=action value='changepass'>
                  <input type=hidden name=id value='".$row[0]."'>
				  <input type=hidden name=lastname value='".$row[8]."'>
				  <input type=checkbox name=cpass value='".$row[0]."'>					
				  <input type=password name=newpass style='font-size:10px;background-color:#FFFFFF'>
				  <input type='submit' value='Change' style='font-size:10px;'>				
				  </td>
				  </form>\n";
				}else{
				  echo "<td align=center align=left>&nbsp;</td>";
				}
				
        		echo "<td align=center align=left style='font-size:10px;font-weight:bold;'>".$row[14]."</td>\n";
                if($row['12']=='1'){
        		  echo "<td align=center><a href='?id=".$row[0]."&act=".md5('makeinactive')."'><img src='../images/checked.gif' border=0></a></td>\n";
				}else{
        		  echo "<td align=center><a href='?id=".$row[0]."&act=".md5('makeactive')."'><img src='../images/unchecked.gif' border=0></a></td>\n";
				}
        		echo "<td align=center><a href='?id=".$row[0]."&act=".md5('deluser')."' onClick='return confirmdelete(\"".strtoupper($row[1])."\")'><img src='../images/delete.jpg' border=0></a></td>\n";
                echo "</tr>\n";
                echo "</form>\n\n\n";				
        	  }
			  
			    if($_SESSION['usertype']=='admin'){
                  echo "<form name='editusertwo$i' action='edit_user_detail.php' method=POST>";
                  echo "<input type='hidden' name='id' value='".$row[0]."'>";				
			      echo "<input type='hidden' name='edituser' value='".$row[1]."'>";
			      echo "</form>";
			    }					  
			  
          }
        ?>
      </table>
    </td>
  </tr>

  <!-- TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH TECH  -->
  
  <tr>
    <td width='100%' align="center"> 
      <table width='100%' bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0" >        
      <tr bgcolor="#FFFFFF" style="font-size:17px; font-weight:bold;line-height:12px;"><td><br>TECH</td></tr>  
      </table>
      <table width='99%' bgcolor="#666666" border="0" cellpadding="4" cellspacing="1" >    
        <tr bgcolor="#CCCCCC" style="font-size:12px;">
          <td width='150'>Username</td>
          <td width='200'>User</td>
          <td width='66'>Usertype</td>
          <td width='66'>Unit</td>
          <td width='190'>Supervisor</td>          
          <td width='220'>Password</td>
          <td width='80'>Deros</td>
          <td width='1%' align=center>Active</td>
          <td width='1%' align=center>Del</td>
        </tr>
        <?php
          for($i=0;$i<=$num_rowst;$i++){
              $row = mysql_fetch_array($resultt);
        	  //print_r($row);
        	  if ($i%2==0) {$rowcolor='#DDDDDD';}else{$rowcolor='#EEEEEE';}
              if ($row[1]!='' && $row[1]!='user'){
                if ($_POST['action']=='changepass' && $_POST['id']==$row[0]){
                  echo "<tr bgcolor=#C3FDB8 style='font-size:12px;line-height:15px;'>";					
				}else{
                  echo "<tr id=\"cell4$i\" onmouseover=\"changeColor('#C9C299', this.id);\" onmouseout=\"changeColor('$rowcolor', this.id);\" bgcolor=$rowcolor style='font-size:12px;line-height:15px;'>";
				}
				if(strtolower($_SESSION['username'])==strtolower($row[1])){$currusercolor="#FFFFCC";}else{$currusercolor=$bgcolor;}
        		echo "<td onclick=\"javascript:document.forms['edituserfour$i'].submit();\" align=left style='font-size:11px;'><a style='text-decoration:underline;color:blue;cursor:pointer;font-weight:bold;'>".ucfirst($row[8]).", ".ucfirst($row[7])."</a></td>\n";
        		echo "<td align=left><b>".$row[1]."</b></td>\n";

			    echo "<form method='post' name='usercp$i'>\n";
			    echo "<input type='hidden' name='action' value='savetype'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";

        		echo "<td align=left>\n";
                echo "<select name='usertype' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
                $queryut = "SELECT `usertype` FROM dd_usertype";
                $resultut = mysql_query($queryut);
	            while ($srow = mysql_fetch_array($resultut)){
			      if($srow['usertype']=='TECH'){$selected = "selected";}else{$selected="";}
		          echo "<option value=\"$srow[usertype]\" $selected>$srow[usertype]</option>\n";
                }
                echo "</select>\n";
        		echo "</td>\n";
                echo "</form>";
				
                //unit
			    echo "<form method='post' name='unit$i'>\n";
			    echo "<input type='hidden' name='action' value='saveunit'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";				
        		echo "<td align=left>\n";			
                echo "<select name='unit' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
	            for($j=0;$j<count($uddarray);$j++){
			      if($uddarray[$j]==$row['5']){$selected = "selected";}else{$selected="";}
		          echo "<option value=\"".$uddarray[$j]."\" $selected>".$uddarray[$j]."</option>\n";
                }
                echo "</select>\n";				
        		echo "</td>\n";
        		echo "</form>\n";

                //super
			    echo "<form method='post' name='super$i'>\n";
			    echo "<input type='hidden' name='action' value='savegoto'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";
                if($row['3']!=''){$selected='selected';}else{$selected='';$row[3]='&nbsp;';}
        		echo "<td align=left>\n";			
                echo "<select name='goto' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
                echo "<option value=\"\" $selected></option>\n";					
	            for($k=0;$k<count($usersarray);$k++){
			      if($usersarray[$k]==$row['3']){$selected = "selected";}else{$selected="";}
		          echo "<option value=\"".$usersarray[$k]."\" $selected>".$usersarray[$k]."</option>\n";
                }
                echo "</select>\n";				
        		echo "</td>\n";
        		echo "</form>\n";

				if($_SESSION['usertype']=='admin'){
                    if($_POST['action']=='changepass' && $_POST['id']==$row[0] && $changepass=='error'){$bgcolor="bgcolor=#C11B17";}else{$bgcolor="";}					  
        	   	    echo "
				    <form name='changepass4' method='post'>
				    <td $bgcolor align=center align=left>
				    <input type=hidden name=action value='changepass'>
                    <input type=hidden name=id value='".$row[0]."'>
				    <input type=hidden name=lastname value='".$row[8]."'>
				    <input type=checkbox name=cpass value='".$row[0]."'>
				    <input type=password name=newpass style='font-size:10px;background-color:#FFFFFF'>
				    <input type='submit' value='Change' style='font-size:10px;'>
				    </td>
				    </form>\n";
				  }else{
				    echo "<td align=center align=left>&nbsp;</td>";
				  }
				
        		echo "<td align=center align=left style='font-size:10px;font-weight:bold;'>".$row[14]."</td>\n";
                if($row['12']=='1'){
        		  echo "<td align=center><a href='?id=".$row[0]."&act=".md5('makeinactive')."'><img src='../images/checked.gif' border=0></a></td>\n";
				}else{
        		  echo "<td align=center><a href='?id=".$row[0]."&act=".md5('makeactive')."'><img src='../images/unchecked.gif' border=0></a></td>\n";
				}
        		echo "<td align=center><a href='?id=".$row[0]."&act=".md5('deluser')."' onClick='return confirmdelete(\"".strtoupper($row[1])."\")'><img src='../images/delete.jpg' border=0></a></td>\n";
                echo "</tr>\n";
                echo "</form>\n\n\n";				
        	  }
			  
              echo "<form name='edituserfour$i' action='edit_user_detail.php' method=POST>";
              echo "<input type='hidden' name='id' value='".$row[0]."'>";				
			  echo "<input type='hidden' name='edituser' value='".$row[1]."'>";
			  echo "</form>";
          }
        ?>
      </table>
    </td>
  </tr>
   
  <!-- USER USER USER USER USER USER USER USER USER USER USER USER USER USER USER USER USER USER USER USER USER USER  -->
  
  <tr>
    <td width='100%' align="center"> 
      <table width='100%' bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="0" >        
      <tr bgcolor="#FFFFFF" style="font-size:17px; font-weight:bold;line-height:12px;"><td><br>USER</td></tr>  
      </table>
      <table width='99%' bgcolor="#666666" border="0" cellpadding="4" cellspacing="1" >    
        <tr bgcolor="#CCCCCC" style="font-size:12px;">
          <td width='150'>Username</td>
          <td width='200'>User</td>
          <td width='66'>Usertype</td>
          <td width='66'>Unit</td>
          <td width='190'>Supervisor</td>          
          <td width='220'>Password</td>
          <td width='80'>Deros</td>
          <td width='1%' align=center>Active</td>
          <td width='1%' align=center>Del</td>
        </tr>
        <?php
          for($i=0;$i<=$num_rowsz;$i++){
              $row = mysql_fetch_array($resultz);
        	  //print_r($row);
        	  if ($i%2==0) {$rowcolor='#DDDDDD';}else{$rowcolor='#EEEEEE';}
              if ($row[1]!='' && $row[1]!='user'){
                if ($_POST['action']=='changepass' && $_POST['id']==$row[0]){
                  echo "<tr bgcolor=#C3FDB8 style='font-size:12px;line-height:15px;'>";					
				}else{
                  echo "<tr id=\"cell3$i\" onmouseover=\"changeColor('#C9C299', this.id);\" onmouseout=\"changeColor('$rowcolor', this.id);\" bgcolor=$rowcolor style='font-size:12px;line-height:15px;'>";
				}
				if(strtolower($_SESSION['username'])==strtolower($row[1])){$currusercolor="#FFFFCC";}else{$currusercolor=$bgcolor;}
        		echo "<td onclick=\"javascript:document.forms['edituserthree$i'].submit();\" align=left style='font-size:11px;'><a style='text-decoration:underline;color:blue;cursor:pointer;font-weight:bold;'>".ucfirst($row[8]).", ".ucfirst($row[7])."</a></td>\n";
        		echo "<td align=left><b>".$row[1]."</b></td>\n";

			    echo "<form method='post' name='usercp$i'>\n";
			    echo "<input type='hidden' name='action' value='savetype'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";
        		echo "<td align=left>\n";
                echo "<select name='usertype' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
                $queryut = "SELECT `usertype` FROM dd_usertype";
                $resultut = mysql_query($queryut);
	            while ($srow = mysql_fetch_array($resultut)){
			      if($srow['usertype']=='USER'){$selected = "selected";}else{$selected="";}
		          echo "<option value=\"$srow[usertype]\" $selected>$srow[usertype]</option>\n";
                }
                echo "</select>\n";
        		echo "</td>\n";
                echo "</form>";
				
                //unit
			    echo "<form method='post' name='unit$i'>\n";
			    echo "<input type='hidden' name='action' value='saveunit'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";
        		echo "<td align=left>\n";
                echo "<select name='unit' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
	            for($j=0;$j<count($uddarray);$j++){
			      if($uddarray[$j]==$row['5']){$selected = "selected";}else{$selected="";}
		          echo "<option value=\"".$uddarray[$j]."\" $selected>".$uddarray[$j]."</option>\n";
                }
                echo "</select>\n";				
        		echo "</td>\n";
        		echo "</form>\n";

                //super
			    echo "<form method='post' name='super$i'>\n";
			    echo "<input type='hidden' name='action' value='savegoto'>\n";
			    echo "<input type='hidden' name='id' value='".$row[0]."'>\n";
                if($row['3']!=''){$selected='selected';}else{$selected='';$row[3]='&nbsp;';}
        		echo "<td align=left>\n";			
                echo "<select name='goto' style='width:100%;font-size:10px;font-weight:bold;' onChange='submit();'>\n";
                echo "<option value=\"\" $selected></option>\n";					
	            for($k=0;$k<count($usersarray);$k++){
			      if($usersarray[$k]==$row['3']){$selected = "selected";}else{$selected="";}
		          echo "<option value=\"".$usersarray[$k]."\" $selected>".$usersarray[$k]."</option>\n";
                }
                echo "</select>\n";				
        		echo "</td>\n";
        		echo "</form>\n";

				if($_SESSION['usertype']=='admin'){
                    if($_POST['action']=='changepass' && $_POST['id']==$row[0] && $changepass=='error'){$bgcolor="bgcolor=#C11B17";}else{$bgcolor="";}					  
        	   	    echo "
				    <form name='changepass3' method='post'>
				    <td $bgcolor align=center align=left>
				    <input type=hidden name=action value='changepass'>
                    <input type=hidden name=id value='".$row[0]."'>
				    <input type=hidden name=lastname value='".$row[8]."'>
				    <input type=checkbox name=cpass value='".$row[0]."'>
				    <input type=password name=newpass style='font-size:10px;background-color:#FFFFFF'>
				    <input type='submit' value='Change' style='font-size:10px;'>
				    </td>
				    </form>\n";
				  }else{
				    echo "<td align=center align=left>&nbsp;</td>";
				  }
				
        		echo "<td align=center align=left style='font-size:10px;font-weight:bold;'>".$row[14]."</td>\n";
                if($row['12']=='1'){
        		  echo "<td align=center><a href='?id=".$row[0]."&act=".md5('makeinactive')."'><img src='../images/checked.gif' border=0></a></td>\n";
				}else{
        		  echo "<td align=center><a href='?id=".$row[0]."&act=".md5('makeactive')."'><img src='../images/unchecked.gif' border=0></a></td>\n";
				}
        		echo "<td align=center><a href='?id=".$row[0]."&act=".md5('deluser')."' onClick='return confirmdelete(\"".strtoupper($row[1])."\")'><img src='../images/delete.jpg' border=0></a></td>\n";
                echo "</tr>\n";
                echo "</form>\n\n\n";				
        	  }
			  
              echo "<form name='edituserthree$i' action='edit_user_detail.php' method=POST>";
              echo "<input type='hidden' name='id' value='".$row[0]."'>";				
			  echo "<input type='hidden' name='edituser' value='".$row[1]."'>";
			  echo "</form>";
          }
        ?>
      </table>
    </td>
  </tr>
  <tr><td><br></td></tr>
  
  <tr><td style="font-size:11px;">* Accounts will be automatically deactivated on DEROS date.</td></tr>
  <tr><td style="font-size:11px;">* Accounts not updated with new information will be deleted 45 days after DEROS date.</td></tr>
  <tr><td style="font-size:11px;">* User passwords will be sent to the MI email address provided on file.</td></tr>
  <tr><td><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td></tr>
  <tr><td><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td></tr>
</table>
</div>

<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>