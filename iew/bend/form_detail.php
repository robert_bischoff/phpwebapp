<?php
  include_once '../include/config.php';
  include_once '../include/functions.php';
  session_start();
  if (login_check($conn) == true) :
?>
<?php
  function microtime_float()
  {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
  }
  $savechanges = "false";
  //print_r($_POST);

  if($_GET['id']!=''){$id=$_GET['id'];}
  if($_POST['id']!=''){$id=$_POST['id'];}

  
  //echo "<br>-------------------------------------------------------<br>";
  $sql0 = "SELECT * FROM `formtracker` WHERE `id`='".$id."'";
  //echo  $sql0."<br>";
  $result0 = mysql_query($sql0);
  $row0 = mysql_fetch_array($result0);
  //print_r($row0);
  //echo "-------------------------------------------------------<br>";
  
  $sqluser = "SELECT * FROM `user` WHERE `last`='".substr($row0['owner'],0,strripos($row0['owner'],','))."'";
   //echo $sqluser."<br>";
  $resultuser = mysql_query($sqluser);
  $rowuser = mysql_fetch_array($resultuser);
  //print_r($rowuser);

  if($_POST['action']=='save'){
    if($_POST['loc']!=$row0['location'] && $_POST['loc']!=""){
      $dd1 = "SELECT * FROM `dd_location` WHERE `loc`='".$_POST['loc']."'";
	  $resultdd1= mysql_query($dd1);
	  if(mysql_num_rows($resultdd1)==0){
        $sqldd1 = "INSERT INTO `s1tracker`.`dd_location` (`id` ,`loc` ) VALUES (NULL , '".strtoupper($_POST['loc'])."')";
	    mysql_query($sqldd1);
      }	  
	
      $sqlupdate = "UPDATE `formtracker` SET `location`='".$_POST['loc']."' WHERE `id`='".$id."'";
      $resultsql = mysql_query($sqlupdate);		
	  //echo $sqlupdate."<br>";
	  $changetext1 = "<b>Location</b> changed to ".strtoupper($_POST['loc']);  
	  $savechanges = "true";	  
	}
	
    if($_POST['status']!=$row0['status'] && $_POST['status']!=""){
      $dd2 = "SELECT * FROM `dd_status` WHERE `status`='".$_POST['status']."'";
	  $resultdd2= mysql_query($dd2);
	  if(mysql_num_rows($resultdd2)==0){
        $sqldd2 = "INSERT INTO `s1tracker`.`dd_status` (`id` ,`status` ) VALUES (NULL , '".strtoupper($_POST['status'])."')";
	    mysql_query($sqldd2);
      }	  
      $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);
      $sqlupdate = "UPDATE `formtracker` SET `status`='".$_POST['status']."',`instatus`='".$nowtime."' WHERE `id`='".$id."'";
      $resultsql = mysql_query($sqlupdate);		
	  //echo $sqlupdate."<br>";
	  $changetext2 = "<b>Status</b> changed to ".strtoupper($_POST['status']);
	  $savechanges = "true";
	}

    if($savechanges=="true"){
      if($changetext1!='' && $changetext2!=''){
        $changetext = $changetext1." and ".$changetext2;
	  }else{
        $changetext = $changetext1."".$changetext2;		
	  }
      $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);
      $sqladdtrack = "INSERT INTO `s1tracker`.`formhistory` (`id` ,`formid` ,`changetype` ,`changeinfo` ,`changeby` ,`timestamp` ) VALUES ('".$id."' , '".$_POST['formid']."', 'flag', '".$changetext."', '".$_SESSION['username']."', '".$nowtime."' )";
      //echo $sqladdtrack."<br>";
      $resultaddtrack = mysql_query($sqladdtrack);
	  $savechanges="false";
	  //echo savechanges."<br>";
      header("location: form_detail.php?id=".$id);
	}
	
  }
 
  if($_POST['action']=='addnote'){
    if($_POST['note']!=''){
      $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);		
      $sqladdnote = "INSERT INTO `s1tracker`.`formhistory` (`id` ,`formid` ,`changetype` ,`changeinfo` ,`changeby` ,`timestamp` ) VALUES ('".$id."' , '".$_POST['formid']."', 'note', '".($_POST['note'])."', '".$_SESSION['username']."', '".$nowtime."' )";
      //echo $sqladdnote."<br>";
      mysql_query($sqladdnote);
      header("location: form_detail.php?id=".$id);
	}
  } 
 
function getTime($timex){
  $timey = substr($timex,0,4);
  $timem = substr($timex,5,2);
  $timed = substr($timex,8,2);
  $timeh = substr($timex,11,2);
  $timemin = substr($timex,14,2);
  $times = substr($timex,17,2);
  $timeopen = mktime ($timeh,$timemin,$times,$timem,$timed,$timey);
  $timenow = time();
  $timenow = $timenow + 32400; //+9 hours for timezone
  $timex = round(($timenow-$timeopen),2);

  $timedays=floor($timex/(24*60*60));
  if($timedays<10){$timedays="0".$timedays;}
  $timehours=floor((($timex-($timedays*(24*60*60)))/60)/60);
  if($timehours<10){$timehours="0".$timehours;}
  $timeminutes=floor(($timex-(($timedays*(24*60*60))+($timehours*(60*60))))/60);
  if($timeminutes<10){$timeminutes="0".$timeminutes;}
  $timex = $timedays."d ".$timehours."h ".$timeminutes."m";
  return $timex;
}  

function getLineColor($timex){
  $timey = substr($timex,0,4);
  $timem = substr($timex,5,2);
  $timed = substr($timex,8,2);
  $timeh = substr($timex,11,2);
  $timemin = substr($timex,14,2);
  $times = substr($timex,17,2);
  $timeopen = mktime ($timeh,$timemin,$times,$timem,$timed,$timey);
  $timenow = time();
  $timenow = $timenow + 32400; //+9 hours for timezone  
  $timex = round(($timenow-$timeopen),2);
  
  $linecolor = "#347C17";
  if($timex > 86400){$linecolor = "#C58917";}
  if($timex > 259200){$linecolor = "#990000";}
  if($timex > 345600){$linecolor = "#666666";}   
  
  return $linecolor;
}   

$sqlstatus = "SELECT `status` from `dd_status` ORDER BY `status`";
$resultstatus = mysql_query($sqlstatus);
$statusarray = array();
while ($x = mysql_fetch_array($resultstatus)){
 $statusarray[] = current($x);
}  

$_POST['loc']=$row0[3];
?>
<?php include "include/config.php"; ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $sitename; ?></title>
<meta http-equiv=content-type content="text/html; charset=UTF-8">
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/thickbox.css" rel="stylesheet" type="text/css" media="screen" />
<script src="../include/lib/js/jquery.js" type="text/javascript"></script>
<script src="../include/lib/js/thickbox.js" type="text/javascript"></script>
<script type="text/javascript" src="../include/lib/js/CalendarPopup.js" language="javascript"></script>
<script type="text/javascript" language="javascript">var cal = new CalendarPopup();</script>

<link rel="STYLESHEET" type="text/css" href="../include/lib/dhtml/dhtmlxCombo/codebase/dhtmlxcombo.css">

<script> window.dhx_globalImgPath="../include/lib/dhtml/img/"; </script>
        
<script src="../include/lib/dhtml/dhtmlxCombo/codebase/dhtmlxcommon.js"></script>
<script src="../include/lib/dhtml/dhtmlxCombo/codebase/dhtmlxcombo.js"></script>
<script  src="../include/lib/dhtml/dhtmlxCombo/codebase/ext/dhtmlxcombo_extra.js"></script>

<script src="../include/lib/js/prototype.js" type="text/javascript"></script>
<script src="../include/lib/js/menu.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--
  function confirmdelete(id,xxx,xcolor){
    xxx.style.background='#FFD1E1';	
	if (confirm('Delete security request '+id+'?')){
	  return true;
	  window.location.reload(true);
	} else {
      xxx.style.background=xcolor;	  
	  return false;
	}  
  } 
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }
  function toggle(i) {
	var ele = document.getElementById("toggle"+i);
	var ele2 = document.getElementById("toggle2"+i);
	if(ele.style.display == "block") {
      ele.style.display = "none";
  	} else {
	  ele.style.display = "block";
	}
	if(ele2.style.display == "block") {
      ele2.style.display = "none";
  	} else {
	  ele2.style.display = "block";
	}	
  }  
  //-->
</script>
<script type="text/javascript">
  function noError(){return true;}
  window.onerror = noError;
</script>

</head>
<body bgcolor="#cccccc">
<?php include '../include/bendheader.php'; ?>
<!-- ------------------------------  content start -------------------------------------- -->
<div style="width:1024;background-color:#FFFFFF;margin-left:-1px;margin-top:-13px;top:0px;height:900px;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
  <table width=100% border="0" bordercolor="#666666" cellpadding="0" cellspacing="0" bgcolor="#FFFFCC" style='padding-top:0px;padding-bottom:500px;'>
    <tr>
      <td>    
        <table align="left" width=100% border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center" style='padding-left:3px;padding-bottom:7px;'>                
              <table width="990" border=0 cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                    <table width="100%" border=0 cellpadding="0" cellspacing="1">
                      <tr>
                        <td>
                          <table border=0 cellpadding="2" cellspacing="2">
                            <tr>
                              <td align="right" valign="top" colspan="3" style="padding-top:10px;padding-left:0px;">
                                <table bgcolor="#000000" width="750" border=0 cellpadding="10" cellspacing="1" align="left">
                                  <tr bgcolor="#FFFFFF">
                                    <td width=5% bgcolor="<?php echo getLineColor($row0['instatus']); ?>" align=center><img src='../images/0.png'></td>
                                    <td width=95% bgcolor="#EEEEEE" style="padding-left:10px;font-size:16px;font-weight:bold;line-height:25px;"><?php echo $row0[2]; ?></td>                                               
                                  </tr>
                                </table>
                              </td>
                              <td align="center" valign="top" colspan="3" style="padding-top:10px;padding-left:5px;" rowspan="2">
                                <table bgcolor="#FFFFFF" width="227" border=0 cellpadding="0" cellspacing="2">
                                  <tr bgcolor="#41627E">
                                    <td width=90 style="color:#fff;font-size:10px;font-weight:bold;padding-left:3px;padding-top:4px;padding-bottom:3px;">Action For</td>
                                    <td width=135 style='font-size:10px;font-weight:bold;padding-left:5px;' align="left" bgcolor="#EEEEEE"><?php echo $row0['owner']; ?></td>                                               
                                  </tr>
                                  <tr bgcolor="#41627E">
                                    <td style="color:#fff;font-size:10px;font-weight:bold;padding-left:3px;padding-top:4px;padding-bottom:3px;">Made By</td>
                                    <td style='font-size:10px;font-weight:bold;padding-left:5px;' align="left" bgcolor="#EEEEEE"><?php echo $row0['producedby']; ?></td>                                               
                                  </tr>
                                  <tr bgcolor="<?php echo getLineColor($row0['instatus']); ?>">
                                    <td style="color:#fff;font-size:10px;font-weight:bold;padding-left:3px;padding-top:4px;padding-bottom:3px;">Time in status</td>
                                    <td style='font-size:10px;font-weight:bold;padding-left:5px;' align="left" bgcolor="#EEEEEE"><?php echo getTime($row0['instatus']); ?></td>                                               
                                  </tr>                      
                                  <tr bgcolor="<?php echo getLineColor($row0['timestamp']); ?>">
                                    <td style="color:#fff;font-size:10px;font-weight:bold;padding-left:3px;padding-top:4px;padding-bottom:3px;">Time Submitted</td>
                                    <td style='font-size:10px;font-weight:bold;padding-left:5px;' align="left" bgcolor="#EEEEEE"><?php echo $row0['timestamp']; ?></td>                                               
                                  </tr>
                                  <tr bgcolor="<?php echo getLineColor($row0['timestamp']); ?>">
                                    <td style="color:#fff;font-size:10px;font-weight:bold;padding-left:3px;padding-top:4px;padding-bottom:3px;">Total Age</td>
                                    <td style='font-size:10px;font-weight:bold;padding-left:5px;' align="left" bgcolor="#EEEEEE"><?php echo getTime($row0['timestamp']); ?></td>                                               
                                  </tr>
                                </table>
                              </td>                   
                            </tr>

                            <form name="form" method="post" action="form_detail.php">
                            <input type=hidden name=action value=save>
                            <input type=hidden name="id" value=<?php echo $id; ?>>
                            <input type=hidden name="formid" value=<?php echo $row0['formid']; ?>>
                            
                            <tr>
                              <td style="padding-top:5px;padding-left:0px;" valign="top">
                                <table bgcolor="#000000" width="300" border=0 cellpadding="4" cellspacing="1">
                                  <tr bgcolor="#FFFF99">
                                    <td style="font-size:10px;line-height:8px;padding-left:3px;font-weight:bold;">Current Location (clear box for more choices)</td>
                                  </tr>                      
                                  <tr bgcolor="#616D7E">
	                                <td valign="top" align="left" style="z-index:1;" bgcolor=<?php echo $locbg; ?>>
                                      <div id="combo_zone1" style="height:23px;z-index:0;width:100%"></div>
                                      <script>
                                      var y=new dhtmlXCombo("combo_zone1","loc",300);
                                      y.enableFilteringMode(true);
                                      y.loadXML("getloc.php?loc=<?php echo $row0['location'] ?>");
						              //var x1 = "<?php //echo $row0['location'] ?>";
					                  //alert("x="+x);						  
                                      //y.attachEvent("onChange",function(){
						            	//alert("x="+x);
						            	//alert(z.getActualValue());
                                        //alert(z.getSelectedValue());
                                        //alert(z.getComboText());
                                        //alert(z.getSelectedText());
						            	//z.setComboText('asdf');
						            	//if(x1 != y.getActualValue() && y.getActualValue()!='')
						            	//{
                                        //  document.form.submit();							  
					            		//  //alert("submit");
					            		//}
                                      //})
                                      </script>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td style="padding-top:5px;" valign="top">
                                <table bgcolor="#000000" width="290" border=0 cellpadding="4" cellspacing="1">
                                  <tr bgcolor="#FFFF99">
                                    <td style="font-size:10px;line-height:8px;padding-left:3px;font-weight:bold;">Status</td>
                                  </tr>                      
                                  <tr bgcolor="#616D7E">
	                                <td valign="top" align="left" style="z-index:1;" bgcolor=<?php echo $locbg; ?>>
                                      <div id="combo_zone2" style="height:23px;z-index:0;width:100%"></div>
                                      <script>
                                      var z=new dhtmlXCombo("combo_zone2","status",290);
                                      z.enableFilteringMode(true);
                                      z.loadXML("getstatus.php?status=<?php echo $row0['status'] ?>");
					            	  //var x2 = "<?php //echo $row0['status'] ?>";					  
                                      //z.attachEvent("onChange",function(){
					            		//if(x2 != z.getActualValue() && z.getActualValue()!='')
					            		//{
                                        //  document.form.submit();
					            		//}
                                      //})
                                      </script>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td style="padding-top:5px;" valign="top" align="center"><input type="image" src='../images/updateform.gif'></td>                                                                            
                            </tr>                  
                          </table>            
                        </td>          
                      </tr>
                      
                      </form>
          
                      <tr>
                        <td>
                          <table bgcolor="#000000" width="990" border=0 cellpadding="1" cellspacing="0">
                            <tr bgcolor="#FFFFFF">
                              <td style="padding-top:0px;padding-left:1px;">
                                <table bgcolor="#000000" width="100%" border=0 cellpadding="0" cellspacing="0">
                                  <tr bgcolor="#FFFFFF">
                                    <td width="100%" bgcolor="#AAAAAA" style="padding:3px;">
                                      <iframe name="formhistory" id="formhistory" src="dg_formhistory.php?id=<?php echo $id; ?>" width="100%" height=250 frameborder="0" scrolling="yes"></iframe>
                                    </td>                                               
                                  </tr>
                                </table>
                              </td>                 
                            </tr>                  
                          </table>            
                        </td>          
                      </tr>

                      <tr>
                        <td>
                          <form name="addnote" method="post" action="form_detail.php">
                          <input type="hidden" name="action" value="addnote">
                          <input type="hidden" name="id" value="<?php echo $id; ?>">
                          <input type="hidden" name="formid" value="<?php echo $row0['formid']; ?>">              
                          <table bgcolor="#000000" width="990" border=0 cellpadding="1" cellspacing="0">
                            <tr bgcolor="#FFFFFF">
                              <td>
                                <table bgcolor="#000000" width="100%" border=0 cellpadding="5" cellspacing="0">
                                  <tr>
                                    <td bgcolor="#FFFFCC" valign="bottom" style="font-size:12px;font-weight:bold;padding:0px;" colspan="2">Add note to form:</td>
                                  </tr>
                                  <tr bgcolor="#FFFFFF">
                                    <td width="99%" bgcolor="#AAAAAA" style="padding-left:3px;" align="center">
                                      <textarea name="note" rows="4" style="width=99%"></textarea>
                                     </td>
                                    <td width="1%" bgcolor="#AAAAAA" align="left"><input type="image" src="../images/addnote.gif"></td>
                                  </tr>
                                </table>
                              </td>                 
                            </tr>                  
                          </table>
                          </form>            
                        </td>          
                      </tr>

                      <tr>
                        <td style="padding-bottom:15px;padding-top:6px;padding-left:1px;">
                          <table cellpadding="0" cellspacing="0">
                            <tr>
                              <td>
                                <table width="100%" border=0 cellpadding="0" cellspacing="0">
                                  <tr>  
                                    <td align="left" width="75" style="padding-top:0px;">
                                      <table width="100%" bgcolor="#000000" border=0 cellpadding="5" cellspacing="1">
                                        <tr bgcolor="#41627E">
                                          <td style="color:#fff;font-size:10px;line-height:6px;padding-left:3px;font-weight:bold;">Routing #</td>
                                        </tr>                      
                                        <tr bgcolor="#FFFFFF">
                                          <td bgcolor="#EEEEEE" style="line-height:21px;font-size:10px;padding-left:7px;font-weight:bold;"><?php echo $row0['formid']; ?></td>                                               
                                        </tr>
                                      </table>
                                    </td>                                   
                                    <td style="padding-left:5px;">
                                      <table bgcolor="#000000" width="100" border=0 cellpadding="5" cellspacing="1">
                                        <tr bgcolor="#41627E">
                                          <td style="color:#fff;font-size:10px;line-height:6px;padding-left:3px;font-weight:bold;">Action For</td>
                                        </tr>                      
                                        <tr bgcolor="#FFFFFF">
                                          <td bgcolor="#EEEEEE" style="line-height:21px;font-size:10px;padding-left:7px;font-weight:bold;"><?php echo $row0['owner']; ?></td>                                               
                                        </tr>
                                      </table>
                                    </td> 
                                    <td style="padding-left:5px;">
                                      <table bgcolor="#000000" width="100" border=0 cellpadding="5" cellspacing="1">
                                        <tr bgcolor="#41627E">
                                          <td style="color:#fff;font-size:10px;line-height:6px;padding-left:3px;font-weight:bold;">Made By</td>
                                        </tr>                      
                                        <tr bgcolor="#FFFFFF">
                                          <td bgcolor="#EEEEEE" style="line-height:21px;font-size:10px;padding-left:7px;font-weight:bold;"><?php echo $row0['producedby']; ?></td>                                               
                                        </tr>
                                      </table>
                                    </td>                                                      
                                    <td style="padding-left:5px;">
                                      <table bgcolor="#000000" width="70" border=0 cellpadding="5" cellspacing="1">
                                        <tr bgcolor="#41627E">
                                          <td style="color:#fff;font-size:10px;line-height:6px;padding-left:3px;font-weight:bold;">Rank</td>
                                        </tr>                      
                                        <tr bgcolor="#FFFFFF">
                                          <td bgcolor="#EEEEEE" style="line-height:21px;font-size:10px;padding-left:7px;font-weight:bold;">
				            	      	  <?php
				            		        if($rowuser['rank']==''){
					            	          echo "&nbsp;";
                                            }else{
					            	          echo $rowuser['rank'];
                                            }
                                          ?>
                                          </td>  
                                        </tr>
                                      </table>                    
                                    </td>
                                    <td style="padding-left:5px;">
                                      <table bgcolor="#000000" width="244" border=0 cellpadding="5" cellspacing="1">
                                        <tr bgcolor="#41627E">
                                          <td style="color:#fff;font-size:10px;line-height:6px;padding-left:3px;font-weight:bold;">Owner Full Name</td>
                                        </tr>                      
                                        <tr bgcolor="#FFFFFF">
                                          <td bgcolor="#EEEEEE" style="line-height:21px;font-size:10px;padding-left:7px;font-weight:bold;">
					                      <?php
				            		        if($rowuser['last']==''){
				            		          echo "&nbsp;";
                                            }else{
				            		          echo strtoupper($rowuser['last'].", ".$rowuser['first']);
                                            }
                                          ?>                                       
                                        </tr>
                                      </table>                    
                                    </td>
                                    <td style="padding-left:5px;">
                                      <table bgcolor="#000000" width="50" border=0 cellpadding="5" cellspacing="1">
                                        <tr bgcolor="#41627E">
                                          <td style="color:#fff;font-size:10px;line-height:6px;padding-left:3px;font-weight:bold;">Unit</td>
                                        </tr>                      
                                        <tr bgcolor="#FFFFFF">
                                          <td bgcolor="#EEEEEE" style="line-height:21px;font-size:10px;padding-left:7px;font-weight:bold;">
					                      <?php
					            	        if($rowuser['unit']==''){
					                  	    echo "&nbsp;";
                                            }else{
					            	          echo $rowuser['unit'];
                                            }
                                          ?>
                                          </td>
                                        </tr>
                                      </table>                    
                                    </td>  
                                    <td style="padding-left:5px;">
                                      <table bgcolor="#000000" width="75" border=0 cellpadding="5" cellspacing="1">
                                        <tr bgcolor="#41627E">
                                          <td style="color:#fff;font-size:10px;line-height:6px;padding-left:3px;font-weight:bold;">DSN</td>
                                        </tr>                      
                                        <tr bgcolor="#FFFFFF">
                                          <td bgcolor="#EEEEEE" style="line-height:21px;font-size:10px;padding-left:7px;font-weight:bold;">
					                  	  <?php
					                  	    if($rowuser['dsn']==''){
					            	          echo "&nbsp;";
                                            }else{
					            	          echo $rowuser['dsn'];
                                            }
                                          ?>						
					            	      </td>                                               
                                        </tr>
                                      </table>                    
                                    </td>
                                    <td style="padding-left:5px;">
                                      <table bgcolor="#000000" width="223" border=0 cellpadding="5" cellspacing="1">
                                        <tr bgcolor="#41627E">
                                          <td style="color:#fff;font-size:10px;line-height:6px;padding-left:3px;font-weight:bold;">E-mail</td>
                                        </tr>                      
                                        <tr bgcolor="#FFFFFF">
                                          <td bgcolor="#EEEEEE" style="line-height:21px;font-size:10px;padding-left:7px;font-weight:bold;">
				            		      <?php
				            		        if($rowuser['email']==''){
					            	          echo "&nbsp;";
                                            }else{
					            	          echo "<a href='mailto:".$rowuser['email']."'>".strtoupper($rowuser['email'])."</a>";
                                            }
                                          ?>                        
                                          </td>
                                        </tr>
                                      </table>                    
                                    </td>                                                                       
                                  </tr>                  
                                </table>
                              </td>
                            </tr>
                          </table>        
                        </td>          
                      </tr>          
                    </table>
                  </td>            
                </tr>    
              </table>
            </td>            
          </tr>    
        </table>
      </td>            
    </tr>    
  </table>    
</div>
<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>


