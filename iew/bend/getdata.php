<?php
//error_reporting(0);
header("Content-type: text/xml");

include "../include/config.php";


$sql = "SELECT `formname` FROM `dd_forms` ORDER BY `formname` ASC";

$stmt = $conn->prepare($sql);
$stmt->execute();

$result = $stmt->get_result();
$num_rows = $result->num_rows;

$xml_output = "<?xml version=\"1.0\" ?>\n";
$xml_output .= "<complete>\n";
for($i=0;$i<$num_rows;$i++){ 
  $row = $result->fetch_array($result);
  if(strtoupper($row[0])==strtoupper($_GET['formname'])){ $selected = "selected='selected'";}else{$selected="";}  
  $xml_output .= "\t<option value='".$row[0]."' $selected>";
  $xml_output .= strtoupper($row[0]);
  $xml_output .= "</option>\n";
}
$xml_output .= "</complete>";
echo $xml_output;
?>