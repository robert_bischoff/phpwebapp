<?php
  include_once '../include/config.php';
  include_once '../include/functions.php';
  session_start();
  if (login_check($conn) == true) :

  
    
  if($_SESSION['fid']!=''){$fid=$_SESSION['fid'];}
  if($_SESSION['5988id']!=''){$the5988id=$_SESSION['5988id'];}

  //print_r($_GET);
  //echo "<br><br>";
  //print_r($_POST);
  //echo "<br><br>";
  //print_r($_SESSION);
  //echo "<br><br>";
  //echo $fid;
  //echo "<br><br>";  
  //echo $the5988id;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8" />
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<title>CMS</title>
<SCRIPT>
  function toggle(i) {
	var ele = document.getElementById("toggle"+i);
	var ele2 = document.getElementById("toggle2"+i);
	if(ele.style.display == "block") {
      ele.style.display = "none";
  	} else {
	  ele.style.display = "block";
	}
	if(ele2.style.display == "block") {
      ele2.style.display = "none";
  	} else {
	  ele2.style.display = "block";
	}	
  }
  function confirmdelete(id,xxx,xcolor){
    xxx.style.background='#FFD1E1';	
	if (confirm('Delete Form '+id+'?')){
	  return true;
	  window.location.reload(true);
	} else {
      xxx.style.background=xcolor;	  
	  return false;
	}  
  } 
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }	
</script>
<script type="text/javascript">
  function GoToUrl(theUrl){
    document.location.href = theUrl;
  }
</script>
</head>
<body bgcolor="#FFFFCC" leftmargin="0" topmargin="0">
<!-- ------------------------------  content start -------------------------------------- -->
<form action="vt_workorder_a.php" name="workorderform" method="post">
<table width=977 border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="5" cellspacing="1">
        <tr style='font-family:Verdana, Geneva, sans-serif;font-size:11px;font-weight:bold;' bgcolor='#CCCCCC'>			
		<td width=30 align='center'>Stat</td>
		<td width=50 align='center'>aNum</td>	
		<td width=50 align='center'>ItemNum</td>
		<td width=100 align='center'>FaultStatus</td>
		<td width=40% align='left'>FaultDesc</td>
		<td width=100 align='center'>AssignedTo</td>	
		<td width=100 align='center'>SubmitTime</td> 
        </tr>
	  <?php 
        $query1 = "SELECT `5988`.`id`,`5988`.`adminNum`,`5988_fault`.* FROM `5988`,`5988_fault` WHERE (`5988_fault`.`5988id`=`5988`.`id` AND `statNum`='1') ORDER BY `faultSubmitTime` DESC";						
        $stmt1 = $conn->prepare($query1);
        $stmt1->execute();
        $result1 = $stmt1->get_result();
        //echo "$sql1";
        $numrows = $result1->num_rows;
        //echo "<br><br>nr=$numrows";
	 	for($i=0;$i<$numrows;$i++){
          $row = $result1->fetch_array();
		  if($i%2==0){$rowcolor='#ffffff';}else{$rowcolor='#f0f0f0';}	  
		  echo "<tr id=\"cell$i\" onclick=\"javascript:GoToUrl('iframe_admin_fault_detail.php?fid=".$row['id']."&the5988id=".$row['5988id']."')\" onmouseover=\"changeColor('#F0E6C2', this.id);\" onmouseout=\"changeColor('$rowcolor', this.id);\" bgcolor='$rowcolor' style='font-family:Verdana, Geneva, sans-serif;font-size:11px;height:27px'>";  
          echo "<td align='center' bgcolor='#CCFFCC' onClick='return'><img src='../images/icons/".$row['statNum'].".gif'></td>";
          echo "<td align='center' style='font-weight:bold;'>".$row['adminNum']."</td>";
		  echo "<td align='center'>$row[faultItem]</td>";
		  echo "<td align='center'>$row[faultStatus]</td>";
		  echo "<td align='left'>$row[faultDesc]</td>";
		  echo "<td align='center'>$row[faultActionOwnerName]</td>";		  
		  echo "<td align='center'>$row[faultSubmitTime]</td>";
		  echo "</tr>";
		}

		if($i<27){
		  for($j=$i;$j<((27+$i)-$i);$j++){
		  if ($j%2==0) {$rowcolor='#ffffff';}else{$rowcolor='#f0f0f0';}				  
		  echo "<tr bgcolor='$rowcolor' style='font-family:Verdana, Geneva, sans-serif;font-size:11px;height:27px'>";			
		  echo "<td align='center' bgcolor='#FFFFFF'>&nbsp;</td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";		  
		  echo "</tr>";
		  }
		}

		$_SESSION['fid']="";
		$_SESSION['5988id']="";
      ?>
      </table>
    </td>
  </tr>
</table>
</form>
<!-- -------------------------------  content finish-------------------------------------- -->
</body>
</html>

<?php else: header("Location: ../lockout.php"); ?>
<?php endif; ?>