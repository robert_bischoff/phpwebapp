<style type="text/css">
.textdefaultblack{
  padding-left:5px;
  font-weight:bold;
  font-size:12px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  color:#000000;
  line-height:20px;  
  height:35px;
}
.textdefaultwhite{
  padding-left:12px;
  font-weight:bold;
  font-size:10px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  color:#FFFFFF;
  height:25px;  
}
.text8{
  font-size:10px;
  font-family:Arial, Helvetica, sans-serif;
  line-height:10px;
  color:#000000;
}
.text10{
  font-weight:bold;
  font-size:10px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  line-height:10px;
  color:#FFFFFF;
}
.text18{
  font-weight:bold;
  font-size:18px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  line-height:20px;
  color:#FFFFFF;
}
.text16{
  font-weight:bold;
  font-size:14px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  line-height:10px;
  color:#000000;
}
.textboxsmall100{
  font-weight:bold;
  font-size:10px;
  width:100%;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  line-height:20px;
  color:#000000;
}
</style>
<html>
<script type="text/javascript">
  function toggle(i) {
	var ele = document.getElementById("toggle"+i);
	var ele2 = document.getElementById("toggle2"+i);
	if(ele.style.display == "block") {
      ele.style.display = "none";
  	} else {
	  ele.style.display = "block";
	}
	if(ele2.style.display == "block") {
      ele2.style.display = "none";
  	} else {
	  ele2.style.display = "block";
	}	
  }
  function MM_openBrWindow(theURL,winName,features) { //v2.0
    window.open(theURL,winName,features);
  }
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }
  function confirmdelete(id,xxx,xcolor){
    xxx.style.background='#FFD1E1';	
	if (confirm('Delete this definition?')){
	  return true;
	  window.location.reload(true);
	} else {
      xxx.style.background=xcolor;	  
	  return false;
	}  
  }  
</script>
<body leftmargin="0" topmargin="0">
<?php
  session_start();
  //print_r($_SESSION);
  //print_r($_POST);
  $rowcount = 0;		
  include "../include/config.php";
  
  if($_SESSION['tsys']!='' && $_SESSION['anum']!='' && $_SESSION['tnum']!=''){
	  echo "<table bgcolor='#ffffff' border='0' cellpadding='2' cellspacing='0'>";
		echo "<tr>";
		  echo "<td align=left>";
			echo "<table align=left width='970' border='0' bordercolor='#dddddd' cellpadding='1' cellspacing='1' bgcolor='$usercolor'>";
			
			$query3 = "SELECT * FROM `pmcs_title`,`pmcs_steps` WHERE `pmcs_title`.`id`=`pmcs_steps`.`pmcsid` ORDER BY `pmcs_steps`.`id` ASC";
			$stmt3 = $conn->prepare($query3);
			$stmt3->execute();
			
			echo "<br>".$query3."<br><br>";
			$result3 = $stmt3->fetch_result($query3);
			$num_rows = $result3->num_rows;
			if($num_rows>0){
			for($i=0;$i<$num_rows;$i++){
			  $row = $result3->fetch_assoc();
			  if ($i%2==0) {$rowcolor='#ffffff';}else{$rowcolor='#f0f0f0';}
				if($i==0){
				  echo "<tr>";	
				  echo "<td>";
				  echo "<table width=100% bgcolor=#6666666 align=center class='textdefaultwhite' width=150 border=0 cellpadding=0 cellspacing=1>";

				  echo "<tr style='line-height:20px;'>";
				  echo "<td align=center width=100 bgcolor=#EEEEEE style='color:#000000'>SYS:</td>";
				  echo "<td align=center width=142 bgcolor=#555555>".$_SESSION['tsys']."</td>";			  
				  echo "<td align=center width=100 bgcolor=#EEEEEE style='color:#000000'>PMSC:</td>";
				  echo "<td align=center width=142 bgcolor=#555555>".$_SESSION['anum']."</td>";
				  echo "<td align=center width=100 bgcolor=#EEEEEE style='color:#000000'>USER:</td>";
				  echo "<td align=center width=142 bgcolor=#555555>".$_SESSION['username']."</td>";
				  echo "<td align=center width=100 bgcolor=#EEEEEE style='color:#000000'>STARTED:</td>";
				  echo "<td align=center width=142 bgcolor=#555555>Unstarted</td>";				  
				  echo "</tr>";

				  echo "<tr>";
				  echo "<form action='iframe_pmcs.php' method='post'>";
				  echo "<td align=center bgcolor=#FFFFCC colspan=8 valign=middle style='padding:10px;'>";
				  echo "<input type=submit value='START&nbsp;THIS&nbsp;PMCS' style='font-size:13px;font-weight:bold;line-height:50px;'>";
				  echo "</td>";
				  echo "</form>";
				  echo "</tr>";
				  			  
				  echo "</table>";
				  echo "</td>";
				  echo "</tr>";
				  echo "<tr>";	
				  echo "<td></td>";
				  echo "</tr>";
				}
				echo "<tr id=\"cell$rowcount\" onmouseover=\"changeColor('#F0E6C2', this.id);\" onmouseout=\"changeColor('$rowcolor', this.id);\" bgcolor=$rowcolor>";
				  echo "<form action='iframe_pmcs.php' method='post'>";
				  echo "<input type=hidden name=action value=update>";
				  echo "<input type=hidden name=id value=".$row['step'].">";			  
				  echo "<td width=100% align=left>";
				  
				  echo "<div id='toggle$i' style='display: block'>";
				  echo "<table bgcolor=#AAAAAA border=0 cellpadding=4 cellspacing=1 width=100%>";
				  echo "<tr>";	
				  echo "<td width=80 bgcolor=#646D7E align=center class='textdefaultwhite' width=150>".stripslashes($row['step'])."</td>";			  
				  echo "<td width=707 bgcolor=#EEEEEE align=left class='textdefaultblack' style='padding:35px;'>".stripslashes($row['desc'])."</td>";
				  echo "<td width=80 bgcolor=#EEEEEE align=center width=1><a id=\"display$i\" href=\"javascript:toggle($i);\">GO</a></td>";
				  echo "<td width=80 bgcolor=#EEEEEE align=center width=1><a id=\"display$i\" href=\"javascript:toggle($i);\">NOGO</a></td>";
				  echo "</tr>";
				  echo "</table>";
				  echo "</div>";	
							  
				  echo "<div id='toggle2$i' style='display: none'>";
				  echo "<table bgcolor=#FFFFCC border=0 cellpadding=4 cellspacing=1 width=100%>";
				  echo "<tr>";			  
				  echo "<td width=120 bgcolor=#CCCCCC align=center class='textdefaultblack' width=100>Please describe the problem.</td>";
				  echo "<td bgcolor=#CCCCCC align=center width=600><textarea name='problem' style='width:100%' rows=4></textarea></td>";
				  echo "<td bgcolor=#CCCCCC align=center width=100><input type=submit value='Submit&nbsp;Problem' style='font-size:12px'></td>";			  
				  echo "<td bgcolor=#CCCCCC align=center width=1><a id=\"display$i\" href=\"javascript:toggle($i);\"><img align=right src='../images/7.gif' border=0></a></td>";
				  echo "</tr>";
				  echo "</table>";
				  echo "</div>";
				  echo "</td>";			  
				  echo "</form>";			  	  			  
				echo "</tr>";
				$rowcount++;			
			  }
			}
			echo "</table>";
		  echo "</td>";
		echo "</tr>";
	  echo "</table>";
  }else{
	  echo "<table width='100%' align=center bgcolor='#ffffff' border='1' cellpadding='2' cellspacing='0'>";
		echo "<tr>";
		  echo "<td>";
			echo "<table width='100%' border='0' bgcolor='#EEEEEE' cellpadding='1' cellspacing='1' bgcolor='$usercolor'>";
			  echo "<tr>";
				echo "<td align=center style='height:550px'>PLEASE COMPLETE THE FORM ABOVE TO CONTINUE</td>";
			  echo "</tr>";
			echo "</table>";		  
		  echo "</td>";
		echo "</tr>";
	  echo "</table>";
  }
?>
</body>
</html>