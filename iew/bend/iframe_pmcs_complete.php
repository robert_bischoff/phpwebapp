<style type="text/css">
.textdefaultblack{
  padding-left:5px;
  font-weight:bold;
  font-size:12px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  color:#BBBBBB;
  line-height:20px;  
  height:35px;
}
.textdefaultwhite{
  padding-left:12px;
  font-weight:bold;
  font-size:10px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  color:#FFFFFF;
  height:25px;  
}
.text8{
  font-size:10px;
  font-family:Arial, Helvetica, sans-serif;
  line-height:10px;
  color:#000000;
}
.text10{
  font-weight:bold;
  font-size:10px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  line-height:10px;
  color:#FFFFFF;
}
.text18{
  font-weight:bold;
  font-size:18px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  line-height:20px;
  color:#FFFFFF;
}
.text16{
  font-weight:bold;
  font-size:14px;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  line-height:10px;
  color:#000000;
}
.textboxsmall100{
  font-weight:bold;
  font-size:10px;
  width:100%;
  font-family:Verdana, Arial, Helvetica, sans-serif;
  line-height:20px;
  color:#000000;
}
</style>
<html>
<script type="text/javascript">
  function toggle(i) {
	var ele = document.getElementById("toggle"+i);
	var ele2 = document.getElementById("toggle2"+i);
	if(ele.style.display == "block") {
      ele.style.display = "none";
  	} else {
	  ele.style.display = "block";
	}
	if(ele2.style.display == "block") {
      ele2.style.display = "none";
  	} else {
	  ele2.style.display = "block";
	}	
  }
  function MM_openBrWindow(theURL,winName,features) { //v2.0
    window.open(theURL,winName,features);
  }
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }
  function confirmdelete(id,xxx,xcolor){
    xxx.style.background='#FFD1E1';	
	if (confirm('Delete this definition?')){
	  return true;
	  window.location.reload(true);
	} else {
      xxx.style.background=xcolor;	  
	  return false;
	}  
  }  
</script>
<body leftmargin="0" topmargin="0">
<?php
  session_start();
  //print_r($_SESSION);
  //print_r($_POST);
  $rowcount = 0;		
  include "../include/config.php";
  
  echo "<table width='100%' align=center bgcolor='#ffffff' border='1' cellpadding='2' cellspacing='0'>";
  echo "<tr>";
  echo "<td>";
  echo "<table width='100%' border='0' bgcolor='#EEEEEE' cellpadding='1' cellspacing='1' bgcolor='$usercolor'>";
  echo "<tr>";
  echo "<td>";	  
  echo "<table width='100%' border='0' bgcolor='#EEEEEE' cellpadding='1' cellspacing='1' bgcolor='$usercolor'>";
  echo "<tr>";
  echo "<td align=center style='height:550px'>PMCS COMPLETE: ".date_at_timezone("Y-m-d H:i:s", "Asia/Seoul")."</td>";
  echo "</tr>";
  echo "</table>";
  echo "</td>";
  echo "</tr>";
  echo "</table>";
  echo "</td>";
  echo "</tr>";
  echo "</table>";
  
?>
</body>
</html>