<?php
include_once '../include/config.php';
include_once '../include/functions.php';
session_start();
if (login_check($conn) == true) :
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8" />
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<title>CMS</title>
<SCRIPT LANGUAGE="JavaScript">
<!--
  function toggle(i) {
	var ele = document.getElementById("toggle"+i);
	var ele2 = document.getElementById("toggle2"+i);
	if(ele.style.display == "block") {
      ele.style.display = "none";
  	} else {
	  ele.style.display = "block";
	}
	if(ele2.style.display == "block") {
      ele2.style.display = "none";
  	} else {
	  ele2.style.display = "block";
	}	
  }
  function confirmdelete(id,xxx,xcolor){
    xxx.style.background='#FFD1E1';	
	if (confirm('Delete Form '+id+'?')){
	  return true;
	  window.location.reload(true);
	} else {
      xxx.style.background=xcolor;	  
	  return false;
	}  
  } 
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }	
  //-->
</script>
<script type="text/javascript">
  function GoToUrl(theUrl){
    document.location.href = theUrl;
  }
</script>
</head>
<body bgcolor="#FFFFCC" leftmargin="0" topmargin="0">
<!-- ------------------------------  content start -------------------------------------- -->
<form action="vt_workorder_a.php" name="workorderform" method="post">	
<table width=977 border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="5" cellspacing="1">     
      <?php
		echo "<tr style='font-family:Verdana, Geneva, sans-serif;font-size:11px;font-weight:bold;' bgcolor='#CCCCCC'>";
		//echo "<td width=75 align='center'>5988Id</td>";
		echo "<td width=20 align='center'>stat</td>";
		echo "<td width=40 align='center'>a#</td>";		
		echo "<td width=40 align='center'>item#</td>";
		echo "<td width=100 align='center'>faultStatus</td>";
		echo "<td width=50% align='left'>problemDesc</td>";
		echo "<td width=172 align='center'>submitTime</td>";
		echo "<td width=140 align='center'>assignedTo</td>";
        echo "</tr>";	  
	  
        //$sql1 = "SELECT * FROM `5988_fault` WHERE `5988id`='".$the5988id."' ORDER BY `faultSubmitTime` DESC";
		$sql1 = "SELECT
		`5988`.`id`,
		`5988_fault`.`5988id`,
		`5988_fault`.`statNum`,
		`5988`.`adminNum`,
		`5988_fault`.`faultItem`,
		`5988_fault`.`faultStatus`,
		`5988_fault`.`faultDesc`,
		`5988_fault`.`faultOwner`,
		`5988_fault`.`faultSubmitTime`,
		`5988_fault`.`faultActionOwnerName`
		FROM `5988_fault`, `5988` 
		WHERE (`5988_fault`.`5988id`=`5988`.`id` AND `5988_fault`.`faultOwner`='".$_SESSION['username']."')
		ORDER BY `faultSubmitTime` DESC";
        $result1 = mysql_query($sql1);
        //echo "$sql1";
        $numrows = mysql_num_rows($result1);
        //echo "<br><br>nr=$numrows";
	 	for($i=0;$i<$numrows;$i++){
          $row = mysql_fetch_array($result1);
		  if($i%2==0){$rowcolor='#ffffff';}else{$rowcolor='#f0f0f0';}			
		  echo "<tr id=\"cell$i\" onmouseover=\"changeColor('#F0E6C2', this.id);\" onmouseout=\"changeColor('$rowcolor', this.id);\" bgcolor='$rowcolor' style='font-family:Verdana, Geneva, sans-serif;font-size:11px;height:27px'>";
          switch($row['statNum']){
			  case 0: $faultbgcolor='#FFDDDD';break;//red
			  case 1: $faultbgcolor='#B5EAAA';break;//green
			  case 2: $faultbgcolor='#B5EAAA';break;//green
			  case 3: $faultbgcolor='#BBDDFF';break;//blue
			  case 4: $faultbgcolor='#FFDDDD';break;//red
			  case 5: $faultbgcolor='#DDDDDD';break;//grey
			  case 6: $faultbgcolor='#FFFFCC';break;//yellow			  
		  }
          echo "<td align='center' bgcolor='$faultbgcolor' onClick='return'><img src='../images/icons/".$row['statNum'].".gif' alt='asdf'></td>";
		  echo "<td align='center'>$row[adminNum]</td>";
		  echo "<td align='center'>$row[faultItem]</td>";
		  echo "<td align='center'>$row[faultStatus]</td>";
		  echo "<td align='left'>$row[faultDesc]</td>";
		  echo "<td align='center'>$row[faultSubmitTime]</td>";
		  if($row['faultActionOwnerName']=='' && $row['statNum']!=6){$row['faultActionOwnerName']='pending...';}
		  echo "<td align='center'>$row[faultActionOwnerName]</td>";
		  echo "</tr>";
		}
		if($i<27){
		  for($j=$i;$j<((26+$i)-$i);$j++){
		  if ($j%2==0) {$rowcolor='#ffffff';}else{$rowcolor='#f0f0f0';}				  
		  echo "<tr bgcolor='$rowcolor' style='font-family:Verdana, Geneva, sans-serif;font-size:11px;height:27px'>";			
		  echo "<td align='center' bgcolor='#FFFFFF'>&nbsp;</td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";
		  echo "<td align='center'></td>";	  
		  echo "</tr>";
		  }
		}
      ?>
      
      </table>
    </td>
  </tr>
</table>
</form>
<!-- -------------------------------  content finish-------------------------------------- -->
</body>
</html>

<?php else: header("Location: ../lockout.php"); ?>
<?php endif; ?>