<?php
 include_once '../include/functions.php';
 include_once '../include/config.php';
 session_start();
 if (login_check($conn) == true) : 
 
 
 function tabmenu() {
    if(isset($_GET['t'])){$t = $_GET['t'];}else{$t = 1;}
        
        if(!isset($_GET['fid']))
        {
            $fid = "";
        }
        else
        {
            $fid = $_GET['fid'];
        } 
        if(!isset($_GET['5988id']))
        {
            $id5988= "";
        }
        else
        {
            $id5988 = $_GET['5988id'];
        } 

    $tabreturn = "tabmenu.php?t=" . $t . "&fid=" . $fid . "&5988id=" . $id5988;
    return $tabreturn;
 }
?>
<!DOCTYPE html>
<html>
<head>
<title><?php echo $sitename; ?></title>
<meta http-equiv=content-type content="text/html; charset=UTF-8">
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/thickbox.css" rel="stylesheet" type="text/css" media="screen" />

<script>
	var $j = jQuery.noConflict();
</script>

<script type="text/javascript" src="../../dist/js/jquery-1.11.2.js"></script>
<script src="../../dist/js/prototype.js" type="text/javascript"></script> 
<script src="../../dist/js/bootstrap.min.js" type="text/javascript"></script>
<script  src="../include/lib/js/dhtmlXCommon.js"></script>
<script  src="../include/lib/js/dhtmlXTabbar.js"></script>

<link rel="STYLESHEET" type="text/css" href="../../dist/css/dhtmlXTabbar.css">

<style>
indexouter {
	width:1024;
	background-color:#FFFFFF;
	margin-left:0px;
	margin-top:0px;
	top:0px;
	height:840px;
	border-left:0.1em solid;
	border-top:0.1em solid;
	border-right:0.1em solid;
	border-bottom:0.1em solid;
	border-color:#eeeeee;
	
}

maintable {
	align:center;
	border:0;
	cellpadding:0;
	cellspacing:0;
	bgcolor:#fffffff;
	width:100%;
	display: table;
}
</style>
<body bgcolor="#cccccc" style="font-family:Verdana, Geneva, sans-serif">

<!-- ------------------------------  content start -------------------------------------- -->
<?php include '../include/bendheader.php'; ?>

<div style="
    width:1024;
	background-color:#FFFFFF;
	margin-left:0px;
	margin-top:0px;
	top:0px;
	height:840px;
	border-left:0.1em solid;
	border-top:0.1em solid;
	border-right:0.1em solid;
	border-bottom:0.1em solid;
	border-color:#eeeeee;">

<div style="
	align:center;
	border:0;
	cellpadding:0;
	cellspacing:0;
	bgcolor:#fffffff;
	width:100%;
	display: table;">
	
  <table> style="display: table-row;">
    <div style="bgcolor:#ffffff; style:padding-left:10px; align:center; display: table-cell;">
      <div style="align:left; border:0 display: table;">
        <div style="display: table-row;">
          <div style="align: left; width: 100; display: table-cell;"><a href="tt.php"><img src="../images/tt.jpg" border="0"></a></div>
          <div style="align: left; width: 100; display: table-cell;"><a href="pmcs.php"><img src="../images/pmcs.jpg" border="0"></a></div>
        </div>
       </div> 
     </div>
   </div>
 
  
  <div style="display: table-row;">
    <div style="align: center; display: table-cell;">
      <div id="a_tabbar" style="width:996px;height:635px;margin-left:7px;">
        <script>
          var myTabbar = new dhtmlXTabBar({parent: "a_tabbar", mode: "top"});
          myTabbar.loadStruct("<?php echo tabmenu();?>");
		  myTabbar.setSkin("dhx_skyblue");
		  myTabbar.setSizes();
        </script>
      </div>
    </div>
   </div>
   
 </div>      
</table>

       

<!-- -------------------------------  content finish-------------------------------------- -->
<?php else : ?>
    <p>
        <span class="error">You are not authorized to access this page.</span> Please <a href="index.php">login</a>.
    </p>
<?php endif; ?>
<?php //include '../include/bendfooter.php'; ?>


</body>
</html>

