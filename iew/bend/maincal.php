<?php
include_once '../include/config.php';
include_once '../include/functions.php';
session_start();
if (login_check($conn) == true) :
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php echo $sitename; ?></title>
<meta http-equiv=content-type content="text/html; charset=UTF-8">
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
<script src="../include/lib/js/prototype.js" type="text/javascript"></script> 
<script src="../include/lib/js/menu.js" type="text/javascript"></script>

<script type="text/javascript">
  function goDATE(theUrl){
    document.location.['detail'].href = theUrl;
  }
</script>

<script type="text/javascript">
  function goURL(theUrl){
    top.document.location.href = theUrl;
  }
</script>

<script language="javascript" type="text/javascript">
  function MM_openBrWindow(theURL,winName,features) {
    window.open(theURL,winName,features);
  }
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }
</script>

<script language="JavaScript">
<!--
function confirmdelete(menudate)
	{	
	  if (confirm('Delete menu for '+menudate+'?    ')){
	    return true;
	    window.location.reload(true);
	  } else {  
	    return false;
	  }  
    }	
  //-->
</script>
<body bgcolor="#cccccc" topmargin="0" leftmargin="0">
<?php include '../include/bendheader.php'; ?>
<!-- ------------------------------  content start -------------------------------------- -->
<div style="width:1024;height:1000;background-color:#FFFFFF;margin-left:0px;margin-top:-11px;top:0px;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
      <table width=100% border="0" bordercolor="#666666" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="padding-top:2px">
        <tr>
          <td>
            <table align="left" width=100% border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" style='padding-left:1px;padding-bottom:3px;padding-right:6px;padding-top:2px;'>
                  <table width=100% border="0" cellpadding="1" cellspacing="1" align="center">
                    <tr>
                      <td style="padding-left:6px;padding-bottom:0px;">     
                        <table border="0" cellpadding="1" cellspacing="1" align="left" bgcolor="#000000" width=932>
                          <tr>
                            <td valign="top">                            
                              <table align="left" width=100% border="0" cellpadding="1" cellspacing="0" bgcolor="#254117">
                                <tr>
                                  <td valign="top" align="left">
                                    <table border="0" cellpadding="1" cellspacing="1" style="padding-left:1px;padding-right:1px;padding-bottom:0px;" width=100%>
                                      <tr>
                                        <?php 
										  $sql1 = "SELECT * FROM `vacc`.`visitoraccess`";							
										  $result1 = mysql_query($sql1);
										  $numrow = mysql_num_rows($result1);
										?>
                                        <td>
                                          <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td width=335 style="font-family:Verdana, Geneva, sans-serif; font-size:16px;font-weight:bold;color:#FFFFFF;padding-left:3px;">SECURITY REQUEST</td>
                                            </tr>
                                           </table>                                          
                                        </td>
                                        <td width=370 align="center">&nbsp;</td>
                                        <td width=240 align="right">                      
                                          <table border="0" cellpadding="3" cellspacing="3" bgcolor="#CCCCCC" align="right">
                                            <tr>
                                              <form name="search" method="post">
                                              <input type="hidden" name="action" value="search">
                                              <td align='right'><input type=text name="search" size="24" style='font-size:10px;background-color:#FFFFFF;font-weight:bold;' value='<?php echo $_POST['search']; ?>'></td>
                                              <td align='right'><input type=submit style='font-size:10px' value='Search'></td>							  
                                              </form>                      
                                            </tr>
                                          </table>
                                        </td>
                                        <td width=40 align="right">                      
                                          <table border="0" cellpadding="5" cellspacing="5" bgcolor="#87AFC7">
                                            <tr>
                                              <td align='center' style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px;font-weight:bold;'><a href='?search=' style="color:#FFFFFF;">ALL</a></td>             
                                            </tr>
                                          </table>
                                        </td>                                         
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>                            
                          </tr>
                        </table> 
                      </td>  
                      <td align='center' style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px;font-weight:bold;padding-top:1px;'><a href='securitycal.php' style="color:#FFFFFF;"><img src='../images/print.png' border="1"></a></td>                                                                                              
                    </tr>                          
                  </table>                                               
                </td>
              </tr>                                     

              <tr>
                <td align="left" style="padding-left:8px;padding-bottom:5px;">              
                  <table width="1012" bgcolor="#41627E" border="0" cellpadding="2" cellspacing="0">                  
                    <tr>
                      <td valign="top">
                        <iframe id="cal3000" id="cal3000" height="376" src="../include/lib/php/cal3000.php" width="100%" scrolling="no" frameborder="0"></iframe>
                      </td>
                      <td valign="top">
                        <iframe id="detail" name="detail" height="376" src="security_detail.php" width="100%" scrolling="no" frameborder="0"></iframe>
                      </td>                      
                    </tr>
                  </table>
                </td>
              </tr>

              
              <tr style="padding:0px;">
                <td align="center" style="padding-bottom:25px;">              
                  <table width="1000" bgcolor="#41627E" border="0" cellpadding="1" cellspacing="1" style="padding:0px;">                  
                    <tr style="font-size:10px;padding:0px;" bgcolor="#EEEEEE">
                      <td><img src='../images/dg_form_header.jpg'></td>
                    </tr>
                    <tr>
                      <?php
					  echo "<td colspan='10'>";
                        echo "<iframe name='theiframe' id='theiframe' src='dg_security.php?search=".$_POST['search']."&searchnames=".$_GET['searchnames']."' width='1008' height='300' frameborder='0' scrolling='yes'></iframe>";
					  echo "</td>";
                      ?>       
                    </tr>
                  </table>
                </td>
              </tr>                                                    
            </table>
          </td>
        </tr> 
      </table> 


<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>