<?php
include_once '../include/config.php';
include_once '../include/functions.php';
session_start();
if (login_check($conn) == true) :
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CMS</title>
<link href="../include/lib/css/main.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
<script src="../include/lib/js/prototype.js" type="text/javascript"></script> 
<script src="../include/lib/js/menu.js" type="text/javascript"></script>
<script type="text/javascript" src="../include/lib/js/CalendarPopup.js" language="javascript"></script>
<script type="text/javascript" language="javascript">var cal = new CalendarPopup();</script>
<script language="javascript" type="text/javascript">
  function MM_openBrWindow(theURL,winName,features) { //v2.0
    window.open(theURL,winName,features);
  }
</script>
</head>
<body bgcolor="#cccccc" onLoad="document.searchit.term.focus();">
<?php

  include '../include/bendheader.php';

  $sql1 = "SELECT `jobstatus` from `dd_status`";
  $result1 = mysql_query($sql1);
  $array1 = array(); 
  while ($x = mysql_fetch_array($result1)){
    $array1[] = current($x);
  }
  
  $sql2 = "SELECT `prioritylevel` from `dd_prioritylevel`";
  $result2 = mysql_query($sql2);
  $array2 = array(); 
  while ($x = mysql_fetch_array($result2)){
    $array2[] = current($x);
  }
  
  $sql3 = "SELECT `org` from `dd_org`";
  $result3 = mysql_query($sql3);
  $array3 = array(); 
  while ($x = mysql_fetch_array($result3)){
    $array3[] = current($x);
  }
  
  $sql4 = "SELECT `worktype` from `dd_worktype`";
  $result4 = mysql_query($sql4);
  $array4 = array(); 
  while ($x = mysql_fetch_array($result4)){
    $array4[] = current($x);
  }
  
  $sql5 = "SELECT `worksubtype` from `dd_worksubtype`";
  $result5 = mysql_query($sql5);
  $array5 = array(); 
  while ($x = mysql_fetch_array($result5)){
    $array5[] = current($x);
  }
  
  $sql6 = "SELECT `funding` from `dd_funding`";
  $result6 = mysql_query($sql6);
  $array6 = array(); 
  while ($x = mysql_fetch_array($result6)){
    $array6[] = current($x);
  }
?>
<!-- ------------------------------  content start -------------------------------------- -->
<div style="width:1024;margin-left:0px;margin-top:-11px;height:50%;background:#FFFFFF;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;"><table bgcolor="#FFFFFF" cellpadding="0" cellspacing="2" border="1" width=1024>
<form name="searchit" method="post">
<table width="100%" bgcolor="#333333" align="left" border="0" cellpadding="0" cellspacing="0" style="margin:5px;">
  <tr bgcolor="#FFFFFF">
    <td>
      <table width="100%" align="left" border="0" cellpadding="4" cellspacing="0">
        <tr bgcolor="#FFFFFF">
          <td colspan="2">
            <table align="left" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666"><tr><td>          
            <table align="left" width=100% align="center" border="0" cellpadding="6" cellspacing="0">
              <tr style="font-size:12px;font-weight:bold;" bgcolor="#FFFFCC" valign="middle">
                <td align="center"><img src='../images/search.gif'></td>
                <td>Find</td>
                <?php
                  echo "<td width=200 align=center>\n";
                  echo "<select name='status' style='width:100%;' onChange='submit()'>\n";
                  echo "<option value='All' selected>All</option>\n";				 
                  for($i=0;$i<count($array1);$i++){
				    if(isset($_POST['status']) && $_POST['status']!='All' && $_POST['status']==$i){$selected="selected";}else{;$selected="";}			  
	                echo "<option value='".$i."' $selected>".$array1[$i]."</option>\n";
                  }
                  echo "</select>\n";
                  echo "</td>\n";
                ?>
                <td align="center" width=150>reports with the term</td>
                <td align="left" width=250><input type=input name="term" style='width:100%' value="<?php echo $_POST['term']; ?>"></td>
                <td align="left" width=290></td>                
              </tr>
            </table>
            </td></tr></table>            
          </td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td>
            <table width=100% align="left" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666"><tr><td>
            <table width=100% align="left" border="0" cellpadding="4" cellspacing="0" bgcolor="#666666">
              <tr style="font-size:12px;font-weight:bold;" bgcolor="#333333">
                <td width=100 style='color:#FFFFFF;'>Starting</td>
                <td width=15 style='color:#FFFFFF;'></td>          
                <td width=100 style='color:#FFFFFF;'>Finishing</td>
                <td width=15 style='color:#FFFFFF;'></td>
                <td width=100 style='color:#FFFFFF;'>Closed On</td>
                <td width=15 style='color:#FFFFFF;'></td>                                
                <td width=50 align=left style='color:#FFFFFF;'>Priority</td>
                <td width=100 align=left style='color:#FFFFFF;'>Owned&nbsp;by</td>
                <td width=130 align=left style='color:#FFFFFF;'>Work&nbsp;Type</td>
                <td width=150 align=left style='color:#FFFFFF;'>Sub&nbsp;Type</td>
                <td width=100 align=left style='color:#FFFFFF;'>Funding</td>                         
              </tr>
              <tr style="font-size:12px;font-weight:bold;" bgcolor="#ffffff">
                <td><input type="input" name="startdate" style="width:100%" value="<?php echo $_POST['startdate'] ?>"></td>
                <td><a href='#' onClick="cal.select(document.forms['searchit'].startdate,'anchor1','yyyy-MM-dd'); return false;" NAME='anchor1' ID='anchor1'><img src='../images/cal.png' border="0"></a></td>
                <td><input type=input name="finishdate" style="width:100%" value="<?php echo $_POST['finishdate'] ?>"></td>
                <td><a href='#' onClick="cal.select(document.forms['searchit'].finishdate,'anchor1','yyyy-MM-dd'); return false;" NAME='anchor2' ID='anchor2'><img src='../images/cal.png' border="0"></a></td>
                <td><input type=input name="closedate" style="width:100%" value="<?php echo $_POST['closedate'] ?>"></td>
                <td><a href='#' onClick="cal.select(document.forms['searchit'].closedate,'anchor3','yyyy-MM-dd'); return false;" NAME='anchor3' ID='anchor3'><img src='../images/cal.png' border="0"></a></td>                
                <?php
                  echo "<td width=50 align=left>\n";
                  echo "<select name='priority' style='width:100%;' onChange='submit()'>\n";
                  echo "<option value=\"All\" selected>All</option>\n";
                  for($i=0;$i<count($array2);$i++){
				    if($_POST['priority']==$array2[$i]){$selected="selected";}else{$selected="";}
	                echo "<option value=\"".$array2[$i]."\" $selected>".$array2[$i]."</option>\n";
                  }
                  echo "</select>\n";
                  echo "</td>\n";

                  echo "<td align=left>\n";
                  echo "<select name='org' style='width:100%;' onChange='submit()'>\n";
                  echo "<option value=\"All\" selected>All</option>\n";
                  for($i=0;$i<count($array3);$i++){
				    if($_POST['org']==$array3[$i]){$selected="selected";}else{$selected="";}				  
	                echo "<option value=\"".$array3[$i]."\" $selected>".$array3[$i]."</option>\n";
                  }
                  echo "</select>\n";
                  echo "</td>\n";

                  echo "<td align=left>\n";
                  echo "<select name='worktype' style='width:100%;' onChange='submit()'>\n";
                  echo "<option value=\"All\" selected>All</option>\n";
                  for($i=0;$i<count($array4);$i++){
				    if($_POST['worktype']==$array4[$i]){$selected="selected";}else{$selected="";}					  
	                echo "<option value=\"".$array4[$i]."\" $selected>".$array4[$i]."</option>\n";
                  }
                  echo "</select>\n";
                  echo "</td>\n";

                  echo "<td align=left>\n";
                  echo "<select name='subtype' style='width:100%;' onChange='submit()'>\n";
                  echo "<option value=\"All\" selected>All</option>\n";
                  for($i=0;$i<count($array5);$i++){
				    if($_POST['subtype']==$array5[$i]){$selected="selected";}else{$selected="";}					  
	                echo "<option value=\"".$array5[$i]."\" $selected>".$array5[$i]."</option>\n";
                  }
                  echo "</select>\n";
                  echo "</td>\n";

                  echo "<td align=left>\n";
                  echo "<select name='funding' style='width:100%;' onChange='submit()'>\n";
                  echo "<option value=\"All\" selected>All</option>\n";
                  for($i=0;$i<count($array6);$i++){
				    if($_POST['funding']==$array6[$i]){$selected="selected";}else{$selected="";}					  
	                echo "<option value=\"".$array6[$i]."\" $selected>".$array6[$i]."</option>\n";
                  }
                  echo "</select>\n";
                  echo "</td>\n";

                  $firsthit='false';
                  $where = "WHERE (";
				  if($_POST['status']!='All'){$where .= "`jobstatus`='".$_POST['status']."'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}
				  
				  if($_POST['startdate']!=''){$where .= "$and `startdate`>='".$_POST['startdate']."'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}		
				  
				  if($_POST['finishdate']!=''){$where .= "$and `finishdate`<='".$_POST['finishdate']."'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}						  		  

				  if($_POST['closedate']!=''){$where .= "$and `closed` LIKE '%".$_POST['closedate']."%'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}	

				  if(isset($_POST['priority']) && $_POST['priority']!='All'){$where .= "$and `prioritylevel`='".$_POST['priority']."'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}
				  if(isset($_POST['org']) && $_POST['org']!='All'){$where .= "$and `org`='".$_POST['org']."'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}
				  if(isset($_POST['worktype']) && $_POST['worktype']!='All'){$where .= "$and `worktype`='".$_POST['worktype']."'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}
				  if(isset($_POST['subtype']) && $_POST['subtype']!='All'){$where .= "$and `worksubtype`='".$_POST['subtype']."'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}
				  if(isset($_POST['funding']) && $_POST['funding']!='All'){$where .= "$and `funding`='".$_POST['funding']."'";$firsthit='true';}else{$where.="";}
				  if($firsthit=='true'){$and=' AND';}
				  if(isset($_POST['term']) && $_POST['term']!=''){$where .= "$and (`workid` LIKE '%".$_POST['term']."%' OR `jobtitle` LIKE '%".$_POST['term']."%' OR `jobdesc` LIKE '%".$_POST['term']."%')";$firsthit='true';}else{$where.="";}

                  $where .= ")";
				  if($firsthit=='true'){$_SESSION['where'] = $where;}else{$_SESSION['where'] = '';}
				  if(!isset($_POST['status'])){$_SESSION['where'] = '';}				  
                ?>
              </tr>
            </table>
            </td></tr></table>
          </td>
            <td align="center" style="padding:8px;" bgcolor="#FFFFFF"><input type=submit value=Search></td>            
        </tr>        
      </table>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>
      <table bgcolor="#333333" align="center" border="0" cellpadding="1" cellspacing="0" width=1011 style="margin-left:0px;">
        <tr>
          <td align="center" height="500" bgcolor="#666666" class='textbox' style='color:#000000;' bordercolor="#000000">
            <iframe frameborder="0" scrolling="yes" src="iframe_showwork.php" style='width:100%;height:100%;' ></iframe>
          </td>
        </tr>
      </table>
    </td>
  </tr>  
  <tr bgcolor="#FFFFFF"><td><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td></tr>  
</table>
</form>
</div>
<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>