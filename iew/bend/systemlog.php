<?php
include_once '../include/config.php';
include_once '../include/functions.php';
session_start();
if (login_check($conn) == true) :
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sitename; ?></title>
<link href="../include/lib/css/main.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
  
<script src="../include/lib/js/prototype.js" type="text/javascript"></script> 
<script src="../include/lib/js/menu.js" type="text/javascript"></script>

</head>
<body bgcolor="#cccccc">
<?php
  include '../include/bendheader.php';
?>
<!-- ------------------------------  content start -------------------------------------- -->

<div style="width:1024;margin-left:0px;margin-top:-11px;height:50%;background:#FFFFFF;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
<table align="center" width=98% border="0" style='margin-top:5px;'>
  <tr>
    <td width='50%' style='font-size:11px;font-weight:bold;'>System Activity Log</td>
    <td align="right" width='50%' style='font-size:11px;font-weight:bold;'>Data as of: <?php echo date('H:i:s Y/m/d'); ?></td>    
  </tr>
  <tr>
    <td align="center" height="600" bgcolor="#666666" class='textbox' style='color:#000000;' bordercolor="#000000" colspan="2">
      <iframe frameborder="0" scrolling="yes" src="iframe_systemlog.php" style='width:100%;height:100%;' ></iframe>
    </td>  
  </tr>
  <tr><td width='100%' colspan="2"><br><br><br><br><br><br><br><br><br><br><br></td></tr>
</table>
</div>

<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>