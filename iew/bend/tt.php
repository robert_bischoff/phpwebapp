<?php
include_once '../include/config.php';
include_once '../include/functions.php';  
session_start();
  if (login_check($conn) == true) :
      
    //echo "<br><br>post=<br>";
    //print_r($_POST);
    //echo "<br><br>session=<br>";
    //print_r($_SESSION);

    $sql2 = "SELECT * FROM `5988` WHERE 'adminNum'= ?";
    
    $var1 = \filter_input(\INPUT_POST, 'adminNum');
    $stmt2 = $conn->prepare($sql2);
    $stmt2->bind_param('s', $var1);
    $stmt2->execute();
   
    $result2 = $stmt2->get_result();
    $row2 = $result2->fetch_assoc();
    //print_r($row2);

    $formaction = 'go';
    
    if (\filter_input(\INPUT_POST, 'action') === 'save' && \filter_input(\INPUT_POST, 'adminNum') === '')
    {
        $formaction = 'stop';
        $anumcolor = '#FFAAAA';
    }
    else
    {
        $anumcolor = '#FFFFFF';
    }
    
    if (\filter_input(\INPUT_POST, 'action') === 'save' && \filter_input(\INPUT_POST, 'system') === '' && $row2['system'] === '')
    {
        $formaction = 'stop';
        $systemcolor = '#FFAAAA';
    }
    else
    {
        $systemcolor = '#FFFFFF';
    }
    
    if (\filter_input(\INPUT_POST, 'action') === 'save' && \filter_input(\INPUT_POST, 'section') === '' && $row2['section'] === '')
    {
        $formaction = 'stop';
        $sectioncolor = '#FFAAAA';
    }
    else
    {
        $sectioncolor = '#FFFFFF';
    }
    
    if (\filter_input(\INPUT_POST, 'action') === 'save' && \filter_input(\INPUT_POST, 'network') === '')
    {
        $formaction = 'stop';
        $networkcolor = '#FFAAAA';
    }
    else
    {
        $networkcolor = '#FFFFFF';
    }
    if (\filter_input(\INPUT_POST, 'action') === 'save' && \filter_input(\INPUT_POST, 'loc') === '')
    {
        $formaction = 'stop';
        $loccolor = '#FFAAAA';
    }
    else
    {
        $loccolor = '#FFFFFF';
    }
    
    if (\filter_input(\INPUT_POST, 'action') === 'save' && \filter_input(\INPUT_POST, 'problem') === '')
    {
        $formaction = 'stop';
        $problemcolor = '#FFAAAA';
    }
    else
    {
        $problemcolor = '#FFFFFF';
    }

    if (\filter_input(\INPUT_POST, 'action') === 'save' && $formaction === 'go')
    {
        $now = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul");
        $sql0 = "SELECT `id` FROM `5988` WHERE `adminNum`= ? ";
        //echo "<br><br>".$sql0;
        $var0 = $_POST['adminNum'];
        $stmt0 = $conn->prepare($sql0);
        $stmt0->bind_param('s', $var0);
        $stmt0->execute();
        $result0 = $stmt0->get_result();
        $row0 = $result0->fetch_assoc();

        $autoincresult = $conn->query("show table status like '%troubleTicket%'");
        $autoincrow = $autoincresult->fetch_assoc();
        $autoinc = $autoincrow['Auto_increment'];

        $sql1 = "INSERT INTO `troubleTicket` (`id`,`5988id`,`statNum`,`section`,`system`,`adminNum`,`network`,`loc`,`desc`,`notes`,`owner`,`ownerIP`,`ownerHost`,`submitTime`) 
	VALUES (NULL,'" . $row0[0] . "','1','" . $_POST['section'] . "','" . $_POST['system'] . "','" . $_POST['adminNum'] . "','" . $_POST['network'] . "','" . $_POST['loc'] . "','" . $_POST['problem'] . "','" . $_POST['notes'] . "','" . $_SESSION['username'] . "','" . $_SERVER['REMOTE_ADDR'] . "','" . gethostbyaddr($_SERVER['REMOTE_ADDR']) . "','$now')";
        //echo "<br><br>".$sql1."<br><br>";
        $conn->query($sql1);
        header('location:tts.php?t=' . $autoinc);
    }
?>

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
    <title><?php echo $sitename; ?></title>
    <meta http-equiv=content-type content="text/html; charset=UTF-8">
    <link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" >
    <link href="../include/lib/css/thickbox.css" rel="stylesheet" type="text/css" media="screen"/>
    <script type="text/javascript" src="../../dist/js/jquery-1.11.2.js"></script>
    <script>
    	var $j = jQuery.noConflict();
    </script>
    <script src="../../dist/js/prototype.js" type="text/javascript"></script> 
    <script src="../../dist/js/menu.js" type="text/javascript"></script>

    <body bgcolor="#cccccc" style="font-family:Verdana, Geneva, sans-serif">
    <?php
    include '../include/bendheader.php';
    ?>
    <!-- ------------------------------  content start -------------------------------------- -->
    <div style="width:1024;background-color:#FFFFFF;margin-left:0px;margin-top:-11px;top:0px;height:840px;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
    <form name='ttform' action='tt.php' method='post'>
    <input type=hidden name=action value=save>
    <table align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%" style="padding-top:25px;">
      <tr>
        <td bgcolor="#ffffff">
          <table align="center" border="0" bgcolor="#000000" cellpadding="2" cellspacing="2" width="975">
            <tr><td style='color:#FFFFFF;font-size:12px;font-weight:bold;padding:3px'>Trouble Ticket Submission</td></tr>      
            <tr>
              <td>
                <table bgcolor=#FFFFCC width="975">
                  <tr>
                    <td style='padding-left:30px;padding-right:30px;padding-top:3px;padding-bottom:20px;'>
                      <table align="center" border="0" cellpadding="0" cellspacing="10" bgcolor=#FFFFCC>
                        <tr style='font-size:12px;padding:0px;font-family:Verdana, Geneva, sans-serif'><td colspan="6">Please enter the following information about your issue.</td></tr>
                        <tr>
                          <td width=100%>
                            <table width=100% border="0" cellpadding="5" cellspacing="1" bgcolor="#000000">
                              <tr style='font-size:12px;font-weight:bold;' bgcolor="#CAE1F9">
                                <td width=150 style='padding:6px;'>Admin#</td>
                                <td width=150 style='padding:6px;'>System</td>
                                <td width=150 style='padding:6px;'>Section</td>                            
                                <td width=150 style='padding:6px;'>Network</td>
                                <td width=150 style='padding:6px;'>Location</td>
                                </tr>
                                <tr style='font-size:11px;font-weight:bold;' bgcolor="#FFFFFF">
                                  <?php
                                  //Starts Select Boxes
                                
                                  //Admin# Dropdown
                                  $sqldd1 = "SELECT DISTINCT(`adminNum`) FROM `5988`";
                                  $stmt = $conn->prepare($sqldd1);
                                  $stmt->execute();
                                  $adminNum ="";
                                  $stmt->bind_result($adminNum);
                                  $stmt->store_result();
                                 
                                  while ($stmt->fetch())
                                  {
                                      $dd1array[] = array('adminNum'=>$adminNum);
                                  }
                                  //print_r($dd1array);
                                  $selected = "";
                                  ?>
                                  <td align=left bgcolor=#CCCCCC>
                                  <select name='adminNum' style='width:100%;font-size:14px;background-color: <?php echo $anumcolor; ?>'>
                                  <option value="<?php echo "Admin" . $selected; ?>">Select Admin#</option>
                                  <?php foreach ($dd1array as $value):
                                      if ($value == $_POST['adminNum'])
                                      {
                                          $selected = "selected";
                                      }
                                      else
                                      {
                                          $selected = "";
                                      }
                                  ?>
                                  <option value="<?php echo $value['adminNum'] . " " . $selected; ?>"><?php echo $value['adminNum'];?></option>
                                  <?php endforeach; ?>
                                  </select>
                                  </td>
                                  <?php $stmt->close(); ?>
                                  <?php
                                  //System Dropdown
                                  $sqldd1 = "SELECT DISTINCT(`system`) from `5988`";
                                  $stmt = $conn->prepare($sqldd1);
                                  $stmt->execute();
                                  $system ="";
                                  $stmt->bind_result($system);
                                  $stmt->store_result();
                                   
                                  while ($stmt->fetch())
                                      {
                                          $dd2array[] = array('system'=>$system);
                                      }
                                  $selected = "";
                                  ?>
        
                                  <td align=left bgcolor=#CCCCCC>
                                  <select name='system' style='width:100%;font-size:14px;background-color:<?php echo $systemcolor; ?>'>
                                  <option value="System<?php echo $selected; ?>">Select System</option>
                                  <?php 
                                  foreach ($dd2array as $value):
                                  if ($value['system'] == $_POST['system'] || $value['system'] == $row2['system'])
                                      {
                                           $selected = "selected";
                                      }
                                      else
                                      {
                                          $selected = "";
                                      } 
                                
                                ?>
                                <option value="<?php echo $value['system'] . " " . $selected; ?>"><?php echo $value['system']; ?></option>
                                <?php endforeach;?>
                                </select>
                                </td>
                                <?php $stmt->close(); ?>  
                                <?php
                                //Section Dropdown
                                $sqldd1 = "SELECT DISTINCT(`section`) from `5988`";
                                $stmt = $conn->prepare($sqldd1);
                                $stmt->execute();
                                $section ="";
                                $stmt->bind_result($section);
                                $stmt->store_result();
                                 
                                while ($stmt->fetch())
                                {
                                    $dd3array[] = array('section'=>$section);
                                }
                                
                                $selected = "";
                                ?>
                                <td align=left bgcolor=#CCCCCC>
                                <select name='section' style='width:100%;font-size:14px;background-color:<?php echo $sectioncolor; ?>'>
                                <option value="Select Section <?php echo $selected; ?>">Select Section</option>
                                <?php 
                                foreach ($dd3array as $value):
                                
                                    if ($value['section'] == $_POST['section'] || $value['section'] == $row2['section'])
                                    {
                                        $selected = "selected";
                                    }
                                    else
                                    {
                                        $selected = "";
                                    }
                                                               
                                ?>
                                <option value="<?php echo $value['section'] . " " . $selected; ?>"><?php echo $value['section']; ?></option>
                                <?php endforeach;?>
                                </select>
                                </td>
                                <?php $stmt->close(); ?>                                          
                                <?php
                                //Network Dropdown
                                $sqldd1 = "SELECT DISTINCT(`network`) from `5988`";
                                $stmt = $conn->prepare($sqldd1);
                                $stmt->execute();
                                $network = "";
                                $stmt->bind_result($network);
                                $stmt->store_result();
                                
                                
                                while ($stmt->fetch())
                                {
                                    if (is_null($network) == false)
                                    {
                                        $dd4array[] = array('network'=>$network);
                                    }
                                            
                                }
                                ?>
                                <td align=left bgcolor=#CCCCCC>
                                <select name='network' style='width:100%;font-size:14px;background-color:<?php echo $networkcolor; ?>'>
                                <option value="Selected Network  <?php echo " " . $selected; ?>">Select Network</option>
                                <?php 
                                foreach ($dd4array as $value):
                                    if ($value['network'] == $_POST['network'] || $value['network'] == $row2['network'])
                                    {
                                        $selected = "selected";
                                    }
                                    else
                                    {
                                        $selected = "";
                                    }
                                ?>
                                <option value="<?php echo $value['network'] . " " . $selected; ?>"><?php echo $value['network']; ?></option>
                                <?php endforeach; ?>
                                </select>
                                </td>
                                <td bgcolor="#CCCCCC"><input name='loc' type=text style='width:100%;font-size:14px;background-color:<?php echo $loccolor; ?>' value='<?php echo $_POST['loc']; ?>'></td>   
                                </tr>
                                <?php $stmt->close(); ?>
                            </table>
                          </td>
                        </tr>
                        
                        <tr>
                          <td width=100%>
                            <table width=100% border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                              <tr>
                                <td style='font-size:11px;font-weight:bold;padding:6px' colspan="6" bgcolor="#CAE1F9">Problem Description</td>
                              </tr>
                              <tr>
                                <?php $problem = "";?>
                                <td bgcolor="#EEEEEE"><textarea name='problem' rows="7" style='width:100%;background-color:<?php echo $problemcolor; ?>'><?php echo $problem; ?></textarea></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        
                        <tr>
                          <td width=100%>
                            <table width=100% border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                              <tr>
                                <td style='font-size:11px;font-weight:bold;padding:6px' colspan="6" bgcolor="#CAE1F9">Notes</td>
                              </tr>            
                              <tr>
                                <td bgcolor="#EEEEEE"><textarea name='notes' rows="5" style='width:100%;'><?php echo $_POST['notes']; ?></textarea></td>
                              </tr>    
                            </table>
                          </td>
                        </tr>
                        
                        <tr>
                          <td colspan=6 align="center"><input type="submit" src="../images/submit1.jpg" value="submit" onclick="submit()"></input></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    </form>
    </div>
    <!-- -------------------------------  content finish-------------------------------------- -->
    <?php include '../include/bendfooter.php'; ?>
    </body>
    </html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>