<?php
  session_start();
  include '../include/config.php';
  include '../include/functions.php';
  
  if (login_check($conn)== true):

  
  $sql2 = "SELECT * FROM `troubleticket` WHERE `id`= ?";
  $id = $_GET['t'];
  $stmt = $conn->prepare($sql2);
  $stmt->bind_param('s', $id);
  $stmt->execute();
  
  $result2 = $stmt->get_result();
  $row2 = $result2->fetch_assoc();
  
  //print_r($row2);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php echo $sitename; ?></title>
<meta http-equiv=content-type content="text/html; charset=UTF-8">
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/thickbox.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="../../dist/js/jquery-1.11.2.js"></script>
<script>
	var $j = jQuery.noConflict();
</script>
<script src="../../dist/js/prototype.js" type="text/javascript"></script> 
<script src="../../dist/js/menu.js" type="text/javascript"></script>

<body bgcolor="#cccccc" style="font-family:Verdana, Geneva, sans-serif">
<?php
  include '../include/bendheader.php';
?>
<!-- ------------------------------  content start -------------------------------------- -->
<div style="width:1024;background-color:#FFFFFF;margin-left:0px;margin-top:-11px;top:0px;height:840px;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
<table align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%" style="padding-top:25px;">
  <tr>
    <td bgcolor="#ffffff">
      <table align="center" border="0" bgcolor="#006600" cellpadding="2" cellspacing="2" width="975">
        <tr><td style='color:#FFFFFF;font-size:12px;font-weight:bold;padding:3px'>Submition Successful</td></tr>      
        <tr>
          <td>
            <table bgcolor=#FFFFCC>
              <tr>
                <td style='padding-left:30px;padding-right:30px;padding-top:3px;padding-bottom:20px;'>
                  <table align="center" border="0" cellpadding="0" cellspacing="10" bgcolor=#FFFFCC>
                    <tr style='font-size:16px;padding:0px;font-family:Verdana, Geneva, sans-serif'><td colspan="6"><b>Thank you!</b>  Your trouble ticket has been submitted.  It will be reviewed shortly.  You may check the status of your ticket at any time from the status view page.</td></tr>
                    <tr>
                      <td width=100%>
                        <table width=100% border="0" cellpadding="1" cellspacing="1" bgcolor="#000000">
                          <tr style='font-size:11px;font-weight:bold;color:#FFFFFF' bgcolor="#123456">
                            <td align="center" style='padding:6px;' width=50>Ticket&nbsp;Number</td>
                            <td style='padding:6px;' width=300>Owner</td>                                                  
                            <td style='padding:6px;' width=300>Time Submitted</td>
                            <td style='padding:6px;' width=300>Source</td>                            
                          </tr>
                          <tr style='font-size:11px;font-weight:bold;' bgcolor="#FFFFFF">
                            <td align="center" style='padding:6px;'><?php echo $_GET['t']; ?></td>
                            <td align="center" style='padding:6px;'><?php echo strtoupper($row2['faultOwner']); ?></td>
                            <td align="center"><?php echo strtoupper($row2['faultSubmitTime']); ?></td>
                            <td align="center" style='padding:6px;'><?php echo strtoupper($row2['faultOwnerHost']); ?></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td width=100%>
                        <table width=100% border="0" cellpadding="1" cellspacing="1" bgcolor="#000000">
                          <tr style='font-size:11px;font-weight:bold;' bgcolor="#CAE1F9">
                            <td style='padding:6px;' width=150>Admin #</td>
                            <td style='padding:6px;' width=150>System</td>
                            <td style='padding:6px;' width=150>Section</td>
                            <td style='padding:6px;' width=150>Network</td>
                            <td style='padding:6px;' width=150>Location</td>
                          </tr>
                          <tr style='font-size:11px;font-weight:bold;' bgcolor="#FFFFFF">
                            <td align="center" style='padding:6px;'><?php echo $row2['5988id']; ?></td>
                            <td align="center" style='padding:6px;'><?php echo $row2['system']; ?></td>
                            <td align="center" style='padding:6px;'><?php echo $row2['section']; ?></td>
                            <td align="center" style='padding:6px;'><?php echo $row2['network']; ?></td>
                            <td align="center" style='padding:6px;'><?php echo $row2['loc']; ?></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td width=100%>
                        <table width=100% border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                          <tr>
                            <td style='font-size:11px;font-weight:bold;padding:6px' colspan="6" bgcolor="#CAE1F9">Problem Description</td>
                          </tr>
                          <tr>
                            <td bgcolor="#EEEEEE"><textarea name='problem' rows="7" style='width:100%;font-size:13px;font-weight:bold;'><?php echo $row2['faultDesc']; ?></textarea></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td width=100%>
                        <table width=100% border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                          <tr>
                            <td style='font-size:11px;font-weight:bold;padding:6px' colspan="6" bgcolor="#CCCCCC">Notes</td>
                          </tr>            
                          <tr>
                            <td bgcolor="#EEEEEE"><textarea name='notes' rows="5" style='width:100%;font-size:13px;font-weight:bold;'><?php echo $row2['notes']; ?></textarea></td>
                          </tr>    
                        </table>
                      </td>
                    </tr>
                    <tr><td align="center"><a href='index.php'><img src="../images/back.jpg" border="0"></a></td></tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</div>
<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>