<?php
include_once '../include/config.php';
include_once '../include/functions.php';
session_start();
if (login_check($conn) == true) :
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php echo $sitename; ?></title>
<meta http-equiv=content-type content="text/html; charset=UTF-8">
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/thickbox.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="../include/lib/js/jquery-1.11.2.js"></script>
<script>
	var $j = jQuery.noConflict();
</script>

<script src="../include/lib/js/prototype.js" type="text/javascript"></script> 
<script src="../include/lib/js/menu.js" type="text/javascript"></script>

<link rel="STYLESHEET" type="text/css" href="../include/lib/css/dhtmlXTabbar.css">
<script  src="../include/lib/js/dhtmlXCommon.js"></script>
<script  src="../include/lib/js/dhtmlXTabbar.js"></script>

<script type="text/javascript">
  function goURL(theUrl){
    top.document.location.href = theUrl;
  }
</script>

<script language="javascript" type="text/javascript">
  function MM_openBrWindow(theURL,winName,features) { //v2.0
    window.open(theURL,winName,features);
  }
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }
</script>

<script language="JavaScript">
<!--
function confirmdelete(menudate)
	{	
	  if (confirm('Delete menu for '+menudate+'?    ')){
	    return true;
	    window.location.reload(true);
	  } else {  
	    return false;
	  }  
    }
  //-->
</script>
<body bgcolor="#cccccc" style="font-family:Verdana, Geneva, sans-serif">
<?php include '../include/bendheader.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CMS</title>
<link href="../include/lib/css/main.css" rel="stylesheet" type="text/css" />
<link href="../include/lib/css/menu.css" rel="stylesheet" type="text/css" />
  
<script src="../include/lib/js/prototype.js" type="text/javascript"></script> 
<script src="../include/lib/js/menu.js" type="text/javascript"></script>

</head>
<body bgcolor="#cccccc">
<!-- ------------------------------  content start -------------------------------------- -->
<div style="width:1024;margin-left:0px;margin-top:-11px;height:50%;background:#FFFFFF;border-left:0.1em solid;border-top:0.1em solid;border-right:0.1em solid;border-bottom:0.1em solid;border-color:#eeeeee;">
<table width='100%'>
  <tr><td width='100%'><br></td></tr>
  <tr><td align="center" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"><br><br><br></td></tr>     
  <tr>
    <td width='100%' align="center">
      <form method="post">
      <input type='hidden' name='form' value='pretem'>
      <input type='hidden' name='action' value='save'>
      <table width=600 align="center" border="0" cellpadding="10" cellspacing="1" bgcolor="#844A00">
        <tr>
          <td colspan="2">
            <table align="center" width=100% border="0" cellpadding="0" cellspacing="0" bgcolor="#844A00">
              <tr>
                <td align="left" bgcolor="#844A00" style='font-size:20px;font-weight:bold;color:#FFFFFF'>WEEKLY SNAPSHOT REPORT</td>
                <td align="right" bgcolor="#844A00" style='font-size:20px;font-weight:bold;color:#FFFFFF'></td>
              </tr>
            </table> 
          </td>
        </tr>
        <tr>
          <td width=50% bgcolor="#DDDDDD" style='font-size:14px;font-weight:bold;'>DAY:</td>
          <td width=50% bgcolor="#DDDDDD" style='font-size:14px;font-weight:bold;'>MONTH:</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
          <?php
          echo "<select name='month' style='width:100%;font-size:24px;font-weight:bold;'>";
          echo "<option style='background-color:#FF9999' value=\"\" selected></option>\n";
          for($i=1;$i<=12;$i++){
			if($i<10){$itext='0'.$i;}
            if($_POST['month']==$i){$selected="selected";}else{$selected="";}
            echo "<option value=\"$i\" $selected>$itext</option>\n";
          }
          echo "</select>";
		  ?>
          </td>
          <td bgcolor="#FFFFFF">
          <?php
          echo "<select name='year' style='width:100%;font-size:24px;font-weight:bold;'>";
          echo "<option style='background-color:#FF9999' value=\"\" selected></option>\n";
          for($j=(date('Y'));$j>2000;$j--){
            if($_POST['year']==$j){$selected="selected";}else{$selected="";}
            echo "<option value=\"$j\" $selected>$j</option>\n";
          }
          echo "</select>";
		  ?>
          </td>          
        </tr>
        <tr>
          <td bgcolor="#DADDAD" colspan="2" align="center"><input type=submit value='RUN REPORT' style='font-size:20px;font-weight:bold;'></td>
        </tr>
      </table>
      </form>
    </td>
  </tr>
  <tr><td align="center" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;">*Please choose, the month and year for the report you would like to run from the dropdowns above.</td></tr>
  <tr><td align="center" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;">Then click 'run report'.</td></tr>
  <!--<tr><td align="center" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"><br>*Last month will only show after the first day of the month after.</td></tr>-->  
  <tr><td width='100%'><br><br><br><br><br><br><br><br><br><br><br></td></tr>
  <tr><td width='100%'><br><br><br><br><br><br><br><br><br><br><br></td></tr>
  <tr><td width='100%'><br><br><br><br><br><br><br><br><br><br><br></td></tr>          
</table>
</div>
<!-- -------------------------------  content finish-------------------------------------- -->
<?php include '../include/bendfooter.php'; ?>
</body>
</html>

<?php else: header("Location: ../index.php"); ?>
<?php endif; ?>