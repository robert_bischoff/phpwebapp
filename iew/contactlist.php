<?php
  session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sitename; ?></title>
<link rel="stylesheet" href="include/lib/css/main.css" type="text/css" />
<script language="javascript" type="text/javascript">
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }
</script>
</head>
<body leftmargin="0" topmargin="0" bgcolor="#ffffff">
<?php include 'include/config.php'; ?>
<?php include 'include/header.php'; ?>

<table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" style="position:absolute; top: 72px; left: 16px;" width=600>
  <tr bgcolor="#f1f1d8">
    <td style="font-weight:bold;"><?php echo $sitename; ?> CONTACT LIST</td>
  </tr>
  <tr bgcolor="#ffffff" valign="top">
    <td width="100%">
      <table height="403" width="100%">
      <tr valign="top">
      <td>
      <table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" width=100%>    
        <tr bgcolor="#6D7B8D" valign="top" style="color:#FFFFFF; font-size:12px; font-weight:bold;">
          <td width="30%">Title</td>
          <td width="70%">Number</td>               
        </tr> 
        <tr bgcolor="#ffffff" valign="top">
          <td>1</td>
          <td>754-####</td>                   
        </tr> 
        <tr bgcolor="#ffffff" valign="top">
          <td>2</td>
          <td>754-####</td>                     
        </tr> 
        <tr bgcolor="#ffffff" valign="top">
          <td>3</td>
          <td>754-####</td>                  
        </tr>      
        <tr bgcolor="#ffffff" valign="top">
          <td>4</td>
          <td>754-####</td>                  
        </tr>      
        <tr bgcolor="#ffffff" valign="top">
          <td>5</td>
          <td>754-####</td>                  
        </tr>                                               
      </table>
      </td>
      </tr>
      <tr>
        <td>
          <table align="center" cellpadding="7" cellspacing="7">
            <tr valign="top" id="btn0" onmouseover="changeColor('#999999', this.id);" onmouseout="changeColor('#eeeeee', this.id);">
              <td align="center"><a href='index.php'><img src='images/mainmenu1.jpg' border="0" /></a></td>
            </tr>  
          </table>
        </td>
      </tr>    
      </table>
    </td>
  </tr>
</table>

<table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" style="position:absolute; top:72px; left: 625px;" width=350>
  <tr bgcolor="#ffff99">
    <td style="font-weight:bold">Login</td>
  </tr>
  <tr bgcolor="#ffffff" valign="top">
    <td width="100%">
<?php
if(!isset($_SESSION['loginstatus'])){
echo "
<form name='login' method='post' action='include/checklogin.php' enctype='multipart/form-data'>
  <input name='action' type='hidden' value='check'>
  <table border='0' width=95% align='center' cellpadding='2' cellspacing='0' bgcolor='#ffffff'>
    <tr>
      <td align='right' width='20%'>Username:</td>
      <td align='left'><input class='text' style='width:99%;background-color:#FFF9D9;' type='text' name='username' /></td>
    </tr>
	<tr>
      <td align='right' width='20%'>Password:</td>
	  <td align='left'><input class='text' style='width:99%;background-color:#FFF9D9;' type='password' name='pass' /></td>		
    </tr>
	<tr>	  
	  <td align='center' colspan='2'><input type='submit' style='background-color:#dddddd;'  value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'/></td>
    </tr>
  </table>
</form>
";
}else{
echo "
<form name='login' method='post' action='include/checklogin.php' enctype='multipart/form-data'>
  <input name='action' type='hidden' value='check'>
  <table border='0' width=95% align='center' cellpadding='2' cellspacing='0' bgcolor='#ffffff'>
    <tr>
      <td align='right' width='20%'>Username:</td>
      <td align='left'><input class='text' style='width:99%;background-color:#FEEEEE;' type='text' name='username' /></td>
    </tr>
	<tr>
      <td align='right' width='20%'>Password:</td>
	  <td align='left'><input class='text' style='width:99%;background-color:#FEEEEE;' type='password' name='pass' /></td>		
	<tr>	  
	  <td align='center' colspan='2'><input type='submit' style='background-color:#dddddd;'  value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'/></td>
    </tr>
  </table>
</form>
";
}
session_unset();
session_destroy();
?>    
    </td>
  </tr>
</table>

<?php include "box4.php"; ?>

</body>
</html>
