<?php
  session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sitename; ?></title>
<link rel="stylesheet" href="include/lib/css/main.css" type="text/css" />
<script language="javascript" type="text/javascript">
  function changeColor(color, ID) {
	document.getElementById(ID).bgColor = color;
  }
</script>
</head>
<body leftmargin="0" topmargin="0" bgcolor="#ffffff">
<?php include 'include/config.php'; ?>
<?php include 'include/header.php'; ?>

<table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" style="position:absolute; top: 72px; left: 16px;" width=600>
  <tr bgcolor="#f1f1d8">
    <td style="font-weight:bold;"><?php echo $sitename; ?> PASSWORD HELP</td>
  </tr>
  <tr bgcolor="#ffffff" valign="top">
    <td width="100%">
      <br />
      <table align="center" height="388" width="90%">
        <tr valign="top">
          <td>
            <table align="left" bgcolor="#333333" cellpadding="1" cellspacing="1" border="0" width=100%>    
              <tr bgcolor="#6D7B8D" valign="top" style="color:#FFFFFF; font-size:12px; font-weight:bold;">
                <td valign="middle" width="30%" style='height:20px;padding-left:5px;'>E-Mail my password</td>            
              </tr>
              <tr>
                <td bgcolor="#FFFFFF" colspan="2" style='height:80px;'>
                  <table align="center" bgcolor="#FFFFFF" cellpadding="3" cellspacing="3" border="0" width=90%>      
                    <tr bgcolor="#ffffff" valign="top">
                      <td>Please enter the email address you created your account with</td>            
                    </tr>  
                    <form method="post">  
                    <tr bgcolor="#ffffff" valign="top">
                      <td width=90%><input class='text' style='width:99%' type='text' name='email' /></td>  
                      <td><input style='width:99%; font-size:10px; background-color:#EEEEEE' type='submit' value='send' /></td>                      
                    </tr>
                    </form>
                    <tr>
                      <td valign="top" bgcolor="#FFFFFF" colspan="2" style='height:80px; font-size:11px;'>To retrieve your password please enter the "@mi.army.mil" email address you used when requesting your account.  A reminder e-mail will be sent to this address.  If you continue to have problems please contact Don Hegarty @ 754-3135.</td>
                    </tr>                     
                  </table>
                </td>
              </tr>                                                      
            </table>
        </td>
      </tr>
      <tr>
        <td>
          <table align="center" cellpadding="7" cellspacing="7">
            <tr valign="top" id="btn0" onmouseover="changeColor('#999999', this.id);" onmouseout="changeColor('#eeeeee', this.id);">
              <td align="center"><a href='index.php'><img src='images/mainmenu1.jpg' border="0" /></a></td>
            </tr>  
          </table>
        </td>
      </tr>    
      </table>
    </td>
  </tr>
</table>

<table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" style="position:absolute;top:72px;left:625px;" width=350>
  <tr bgcolor="#ffff99">
    <td style="font-weight:bold">Login</td>
  </tr>
  <tr bgcolor="#ffffff" valign="top">
    <td width="100%">
<?php
if(!isset($_SESSION['loginstatus'])){
echo "
<form name='login' method='post' action='include/checklogin.php' enctype='multipart/form-data'>
  <input name='action' type='hidden' value='check'>
  <table border='0' width=95% align='center' cellpadding='2' cellspacing='0' bgcolor='#ffffff'>
    <tr>
      <td align='right' width='20%'>Username:</td>
      <td align='left'><input class='text' style='width:99%;background-color:#FFF9D9;' type='text' name='username' /></td>
    </tr>
	<tr>
      <td align='right' width='20%'>Password:</td>
	  <td align='left'><input class='text' style='width:99%;background-color:#FFF9D9;' type='password' name='pass' /></td>		
    </tr>
	<tr>	  
	  <td align='center' colspan='2'><input type='submit' style='background-color:#dddddd;'  value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'/></td>
    </tr>
	<tr><td align=right colspan=2 style='font-size:11px'><a href='forgotpass.php'>Forgot password?</a></td></tr>
  </table>
</form>
";
}else{
echo "
<form name='login' method='post' action='include/checklogin.php' enctype='multipart/form-data'>
  <input name='action' type='hidden' value='check'>
  <table border='0' width=95% align='center' cellpadding='2' cellspacing='0' bgcolor='#ffffff'>
    <tr>
      <td align='right' width='20%'>Username:</td>
      <td align='left'><input class='text' style='width:99%;background-color:#FEEEEE;' type='text' name='username' /></td>
    </tr>
	<tr>
      <td align='right' width='20%'>Password:</td>
	  <td align='left'><input class='text' style='width:99%;background-color:#FEEEEE;' type='password' name='pass' /></td>		
	<tr>	  
	  <td align='center' colspan='2'><input type='submit' style='background-color:#dddddd;'  value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'/></td>
    </tr>
	<tr><td align=right colspan=2 style='font-size:11px'><a href='forgotpass.php'>Forgot password?</a></td></tr>
  </table>
</form>
";
}
session_unset();
session_destroy();
?>    
    </td>
  </tr>
</table>

<?php include "box4.php"; ?>

</body>
</html>
