<?php
include_once 'config.php';
include_once 'functions.php';
include_once '..\..\includes\functions.php';

session_start(); // Our custom secure way of starting a PHP session.


if (isset($_POST['username'], $_POST['p'])) {
    $username = sanitizeMySQL($conn, strtolower($_POST['username']));
    $password = sanitizeMySQL($conn, $_POST['p']); // The hashed password.
    
    if (login($username, $password, $conn) == true)
    {
        echo ("Success!");
        // Login success
        header('Location: ../bend/index.php');
        exit();
    }
    else
    {
        // Login failed
        header('Location: ../index.php');
        exit();
    }
}
else
{
    // The correct POST variables were not sent to this page.
    echo 'Invalid Request';
}


?>