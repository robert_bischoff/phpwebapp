   </td>
  </tr>
  <tr>
    <td colspan="3">
      <table align="center" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" width=98% style="font-size:10px;font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;">    
        <tr bgcolor="#eeeeee">
          <td align="left">This is a US Army web site. The security accreditation level of this site is Unclassified. Do not process, store, or transmit information above the accreditation level of this system. US Army web sites may be monitored for all lawful purposes, including to ensure their use is authorized, for management of the system, to facilitate protection against unauthorized access, and to verify security procedures, survivability, and operational security. Monitoring includes, but is not limited to, active attacks by authorized DoD entities to test or verify the security of this system. During monitoring, information may be examined, recorded, copied and used for authorized purposes. All information, including personal information, placed on or sent over this system may be monitored. Use of this US Army web site, authorized or unauthorized, constitutes consent to monitoring. Unauthorized use of this DoD web site may subject you to criminal prosecution. Evidence of unauthorized use collected during monitoring may be used for administrative, criminal or other adverse action.
          </td>
        </tr>
      </table>  
    </td>
  </tr>
</table>