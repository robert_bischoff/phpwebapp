<?php
include_once 'config.php';

function date_at_timezone($format, $locale, $timestamp=null){
    if (\is_null($timestamp))
    {
        $timestamp = time();
    }
    $current = time();
    $tz = date_default_timezone_get();
    date_default_timezone_set($locale);
    $offset = time() - $current;
    $output = date($format, $timestamp - $offset);
    date_default_timezone_set($tz);
    return $output;
}

function sec_session_start() {
    $session_name = 'sec_session_id';   // Set a custom session name
    $secure = SECURE;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params(/*$cookieParams["lifetime"],
    $cookieParams["path"],
    $cookieParams["domain"]*/0,'/','.local.com',
    $secure,
    $httponly);
    $cookieParams = session_get_cookie_params();
    
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session
    session_regenerate_id(true);    // regenerated the session, delete the old one.
}

function login($username, $password, $conn) 
{
    // Using prepared statements means that SQL injection is not possible.
   
    if ($stmt = $conn->prepare("SELECT id, pass, salt
        FROM user
       WHERE username = ?
        LIMIT 1"))
     {
        
        $user_id = 0; $db_password = ""; $salt = "";
        $stmt->bind_param('s', $username);  // Bind "$username" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();

        // get variables from result.
        $stmt->bind_result($user_id, $db_password, $salt);
        $stmt->fetch();

        // hash the password with the unique salt.
        $password = hash('sha512', $password.$salt); 
        if ($stmt->num_rows == 1) 
        {
            // If the user exists we check if the account is locked
            // from too many login attempts

            if (checkbrute($user_id, $conn) == true) 
            {
                // Account is locked
                // Send an email to user saying their account is locked
                return false;
            } 
            else 
            {
                // Check if the password in the database matches
                // the password the user submitted.
                if ($db_password == $password)
                {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    $query = "SELECT * FROM user WHERE id = '$user_id'";
                    $stmt = $conn->prepare($query);
                    $stmt->bind_param('s',$user_id);
                    $stmt->execute();
                    
                    $result = $stmt->get_result();
                    $row = $result->fetch_assoc();
                    
                    $_SESSION[username] = $row['username'];
                    $_SESSION[usertype] = $row['usertype'];
                    if ($_SESSION[usertype] == "user") {
                        $accesslevel = 0;
                    }
                    if ($_SESSION[usertype] == "tech") {
                        $accesslevel = 1;
                    }
                    if ($_SESSION[usertype] == "admin") {
                        $accesslevel = 2;
                    }
                    $_SESSION[useraccesslevel] = $accesslevel;
                    $_SESSION[unit] = $row['unit'];
                    $_SESSION[lastlogin] = $row['lastlogin'];
                    $_SESSION[bmess] = $row['bmess'];
                    $_SESSION[lmess] = $row['lmess'];
                    $query = "UPDATE `user` SET `lastlogin`=" . date('YmdHis') . " WHERE `username`='" . $row['username'] . "'";
                    $result = $conn->query($query);
                    $query = "INSERT INTO `watchdog_action_log` VALUES (NULL, '" . $row['username'] . "', '1', 'User logged into system', '" . $_SERVER['REMOTE_ADDR'] . "', '" . gethostbyaddr($_SERVER['REMOTE_ADDR']) . "', '" . time() . "')";
                    $result = $conn->query($query);
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user_id'] = $user_id;
                    
                    // XSS protection as we might print this value
                    $username = preg_replace("**/[^a-zA-Z0-9_--]+/**",
                        "",
                        $username);
                    $_SESSION['username'] = $username;
                    $_SESSION['login_string'] = hash('sha512',
                        $password . $user_browser);
                    // Login successful.
                    return true;
                } 
                else
                {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $conn->query("INSERT INTO login_attempts(user_id, time)
                        VALUES ('$user_id', '$now')");
                    return false;
                }
            }
        }
        else 
        {
            // No user exists.
            return false;
        }
    }
}

function checkbrute($user_id, $conn) 
{
    // Get timestamp of current time
    $now = time();

    // All login attempts are counted from the past 2 hours.
    $valid_attempts = $now - (2 * 60 * 60);

    if ($stmt = $conn->prepare("SELECT time
        FROM login_attempts
        WHERE user_id = ?
        AND time > '$valid_attempts'")) 
    {
        $stmt->bind_param('i', $user_id);

        // Execute the prepared query.
        $stmt->execute();
        $stmt->store_result();

        // If there have been more than 5 failed logins
        if ($stmt->num_rows > 5)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

function login_check($conn) {
    // Check if all session variables are set
    if (isset($_SESSION['user_id'],
              $_SESSION['login_string'])) 
    {

        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        //$username = $_SESSION['username'];

        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];

        if ($stmt = $conn->prepare("SELECT pass
                                  FROM user
                                  WHERE id = ? LIMIT 1"))
        {
        // Bind "$user_id" to parameter.
        $stmt->bind_param('i', $user_id);
        $stmt->execute();   // Execute the prepared query.
        $stmt->store_result();

        if ($stmt->num_rows == 1)
        {
            // If the user exists get variables from result.
            $password = "";
            $stmt->bind_result($password);
            $stmt->fetch();
            $login_check = hash('sha512', $password . $user_browser);

            if ($login_check == $login_string)
            {
                // Logged In!!!!
                return true;
            }
            else
            {
                // Not logged in
                return false;
            }
        }
        else
        {
            // Not logged in
            return false;
        }
        }
        else
        {
            // Not logged in
            return false;
        }
    }
    else
    {
        // Not logged in
        return false;
    }
}

function esc_url($url)
{

    if ('' == $url)
    {
        return $url;
    }

    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);

    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;

    $count = 1;
    while ($count)
    {
        $url = str_replace($strip, '', $url, $count);
    }

    $url = str_replace(';//', '://', $url);

    $url = htmlentities($url);

    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);

    if ($url[0] !== '/')
    {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    }
    else
    {
        return $url;
    }
}

function read($id)
{
    global $sess_save_path;

    echo "<p>Session identifier is: {$id}</p>";

    $sess_file = "{$sess_save_path}/sess_{$id}";

    if (is_readable($sess_file)) {
        $data = (string) file_get_contents($sess_file);
        echo "<p>Can read from file: {$sess_file}</p>";
        echo "<p>Data is: {$data}</p>";
    } else {
        echo "<p>Cannot read from file: {$sess_file}</p>";
    }

    return $data;
}
?>