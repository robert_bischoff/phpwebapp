<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
	<link href="../sample.css" rel="stylesheet" type="text/css"/> 
</head>
<body>
<table border="1">
<tr>
<form action="../sample_posteddata.php" method="post">
<td align="right"><input type="submit" value="Submit"/></td>
</tr>
<tr>
<td>
<?php
include("../../ckeditor.php");
$CKEditor = new CKEditor();
$CKEditor->returnOutput = true;
$CKEditor->basePath = '../../';
$CKEditor->config['width'] = 700;
$CKEditor->config['removePlugins'] = 'elementspath';
$CKEditor->config['enterMode'] = 2;
$CKEditor->textareaAttributes = array("cols" => 80, "rows" => 10);
$initialValue = '1234 2134 1234 2134 1234';
$config['toolbar'] = array(
	array('Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'Outdent', 'Indent', 'Link', 'Unlink', 'PasteFromWord', '-', 'Print'),
);
$config['skin'] = 'v2';
echo $CKEditor->editor("editor", $initialValue, $config);
?>
</form>
</td>
</tr>
</table>
</body>
</html>
