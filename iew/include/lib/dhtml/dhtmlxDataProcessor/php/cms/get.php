<?php
/*
Copyright Scand LLC http://www.scbr.com
This version of Software is free for using in non-commercial applications.
For commercial use please contact info@scbr.com to obtain license
*/
    require_once("uDataLink.php");
    require_once("db.php");
    //load configuration
    switch($_GET['ctrl']){
        case "tree":
                //print data as XML
                $temp=new uDataLink("tree_data.xml");
                $temp->printXMLHeader("tree","id='0'");
                break;
        default:
                //print data as XML
                $temp=new uDataLink("data.xml");
                $temp->printXMLHeader("rows");
                break;
    }

    $temp->getXML();
    $temp->printXMLFooter();
?>
