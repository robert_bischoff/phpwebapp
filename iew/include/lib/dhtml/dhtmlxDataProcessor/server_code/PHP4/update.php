<?php
/*
Copyright Scand LLC http://www.scbr.com
This version of Software is free for using in non-commercial applications.
For commercial use please contact info@scbr.com to obtain license
*/
error_reporting(E_ALL ^ E_NOTICE);

    require_once("uDataLink.php");
    require_once("db.php");

    //load configuration
    if (array_key_exists('ctrl',$_GET)) {

        switch($_GET['ctrl']){
            case "tree":
                    $temp=new uDataLink("tree_data.json");
                    break;
            default:
                    $temp=new uDataLink("data.json");
                    break;
        }

    } else {
           $temp = new uDataLink("data.json");
    }

    //sync with DB
    $temp->save($_GET);
?>
