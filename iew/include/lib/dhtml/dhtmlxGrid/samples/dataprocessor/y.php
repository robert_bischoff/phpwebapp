<!--conf
<sample in_favorites="true">
              <product version="1.4" edition="pro"/>
                     <modifications>
                            <modified date="070101"/>
                     </modifications>
               <sampledescription><![CDATA[Online sample demonstrating grid connection to server side datasource. You can add/delete/update records - all changes will be saved to database (your changes are available for you within php session time frame)]]></sampledescription></sample>
 --> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Save Data dynamicaly</title>
	
</head>

<body>
<link rel='STYLESHEET' type='text/css' href='../common/style.css'>
	<link rel="STYLESHEET" type="text/css" href="../../codebase/dhtmlxgrid.css">
	<link rel="STYLESHEET" type="text/css" href="../../codebase/dhtmlxgrid_skins.css">
	<script>_js_prefix = "../../codebase/";</script>
	<script  src="../../codebase/dhtmlxcommon.js"></script>
	<script  src="../../codebase/dhtmlxgrid.js"></script>		
	<script  src="../../codebase/dhtmlxgridcell.js"></script>
	<script  src="../../codebase/excells/dhtmlxgrid_excell_calendar.js"></script>
	<script  src="../../../dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
    <script  src="../../codebase/ext/dhtmlxgrid_srnd.js"></script>
    <script  src="../../codebase/ext/dhtmlxgrid_filter.js"></script> 
    
    
	<table width="900">
		<tr>
		<td width="100%" valign="top">
			<div id="gridbox" width="100%" height="250px" style="background-color:white;"></div>
		</td>  
		</tr>
	</table>
	
<br>
<script>
	//init grid and set its parameters (this part as always)
	mygrid = new dhtmlXGridObject('gridbox');
	mygrid.setImagePath("../../codebase/imgs/");
	mygrid.setHeader("Sales,Book Title,Author,Price,In Store,Shipping,Bestseller,Date of Publication");
	mygrid.attachHeader(" ,<input onclick='return !((arguments[0]||event).cancelBubble=true);' id='title_box' type='text' style='width:100%'>,<select onclick='return !((arguments[0]||event).cancelBubble=true);' id='author_box' style='width:100%'></select>, ,#cspan,#cspan,#cspan,#cspan");
	mygrid.setInitWidths("50,150,120,80,80,80,80,200")
	mygrid.setColAlign("right,left,left,right,center,left,center,center")
	mygrid.setColTypes("ed,ed,ed,price,ch,co,ro,ro");
    mygrid.getCombo(5).put(2,2);
  	mygrid.setColSorting("int,str,str,int,str,str,str,date")
	mygrid.init();
	mygrid.setSkin("light")
	//mygrid.enableSmartRendering(true);

    mygrid.loadXML("../dhtmlxDataProcessor/php/get.php",function(){
    	mygrid.makeFilter('First_box',2);
    	mygrid.makeFilter('Last_box',1);
    });	

//============================================================================================
	//mygrid.loadXML("../dhtmlxDataProcessor/php/get.php");
	myDataProcessor = new dataProcessor("../dhtmlxDataProcessor/php/update.php");
	//myDataProcessor.enableDebug(true);
	myDataProcessor.enableDataNames(true);
	myDataProcessor.setVerificator(1)
	myDataProcessor.setVerificator(3,checkIfNotZero)
	myDataProcessor.setUpdateMode("off");//available values: cell (default), row, off
	myDataProcessor.defineAction("error",myErrorHandler);
	myDataProcessor.setTransactionMode("POST");
	myDataProcessor.init(mygrid);
//============================================================================================



	//Example of error handler. It gets <action> tag object as incomming argument.
	function myErrorHandler(obj){
		alert("Error occured.\n"+obj.firstChild.nodeValue);
		myDataProcessor.stopOnError = true;
		return false;
	}
	
	//Example of verification function. It verifies that value is not 0 (zero).
	//If verification failed it should return false otherwise true.
	//Verification fucntion specified in setVerificator method will always get two argumentrs: value to verify and column name (use it for message)
	function checkIfNotZero(value,colName){
		if(value.toString()._dhx_trim()=="0"){
			showMessage(colName+ " should not be 0")
			return false
		}else
			return true;
	}
	
	
	
	
	
	
	//we use this function instead of alert to show messages on page (it is used in checkIfNotZero verification function).
	function showMessage(msg){
		var msger = document.getElementById("messanger");
		msger.innerHTML = msg;
		clearTimeout(toRef) 
		toRef = setTimeout("showMessage('&nbsp;')",5000)
	}
	
	
	
	
	
	
	
	//these functions doesn't have direct relation to data processor. It related to this example interface only.
	function doOnAutoupdateChecked(state){
		if(state){
			document.getElementById("updmdflt").click()
			//myDataProcessor.setUpdateMode('cell')
		}else
			myDataProcessor.setUpdateMode('off')
		document.getElementById('updatebutton').style.display=state?'none':'inline';
		document.getElementById('updatemodes').style.display=state?'':'none'
	}
	var toRef;
	
</script>

</body>
</html>


