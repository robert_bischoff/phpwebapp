<?php
/*
Copyright Scand LLC http://www.scbr.com
This version of Software is free for using in non-commercial applications.
For commercial use please contact info@scbr.com to obtain license
*/
error_reporting(E_ALL ^ E_NOTICE);
include_once('JSON.php');

//error_reporting(E_ALL);


class uDataLink{
        function uDataLink($config){
                 $json = new Services_JSON();
                 $data = file_get_contents($config);
                 $this->c = $json->decode($data);
        }

        function save($data){
                 $this->printXMLHeader();
                 if (!isset($data[(string)$this->c->action])) $data[(string)$this->c->action]=""; //echo (string)$this->c["type"];
                 if (((string)$this->c->type)=="tree"){
                         switch($data[(string)$this->c->action]){
                                 case 'deleted':
                                         $this->deleteTree($data);
                                         break;
                                 case 'inserted':
                                         $this->insertTree($data);
                                         break;
                                 default:
                                         $this->updateTree($data);
                                         break;
                         }
                 } else {
                         switch($data[(string)$this->c->action]){
                                 case 'deleted':
                                         $this->delete($data);
                                         break;
                                 case 'inserted':
                                         $this->insert($data);
                                         break;
                                 default:
                                         $this->update($data);
                                         break;
                         }

                 }
                 $this->printXMLFooter();
        }

        function insertTree($data){
                    $table= (string)$this->c->table->name;
                    $id=    (string)$this->c->table->key->name;
                    $pid=   (string)$this->c->table->pkey->name;
                    $order= (string)$this->c->table->order->name;
                    $name=  (string)$this->c->table->text->name;

                    $a_id=    (string)$this->c->table->key->value;
                    $a_pid=   (string)$this->c->table->pkey->value;
                    $a_order= (string)$this->c->table->order->value;
                    $a_name=  (string)$this->c->table->text->value;

                    $sql="UPDATE ".$table." SET ".$order."=".$order."+1 WHERE ".$pid."=".$data[$a_pid]." AND ".$order." > ".($data[$a_order]-1);
                    $nid=mysql_query($sql);

                    $sql="INSERT INTO ".$table."(".$pid.",".$order.",".$name.") VALUES(".$data[$a_pid].",".$data[$a_order].",\"".mysql_real_escape_string($data[$a_name])."\")";
                    $res=mysql_query($sql);

                    $this->printAction("insert",$data[$a_id],mysql_insert_id());
        }

        function deleteTree($data){
                    $table= (string)$this->c->table->name;
                    $id=    (string)$this->c->table->key->name;
                    $pid=   (string)$this->c->table->pkey->name;
                    $order= (string)$this->c->table->order->name;
                    $name=  (string)$this->c->table->text->name;

                    $a_id=   (string)$this->c->table->key->value;

                    $sql="SELECT * FROM ".$table." WHERE ".$id."=".$data[$a_id];
                    $res=mysql_query($sql);
                    $sdata=mysql_fetch_assoc($res);

                    $this->delete_rec($table,$id,$data[$a_id],$pid);

                    $sql="UPDATE ".$table." SET ".$order."=".$order."-1 WHERE ".$pid."=".$sdata[$pid]." AND ".$order." > ".$sdata[$order];
                    $nid=mysql_query($sql);
                    $this->printAction("delete",$data[$a_id],$data[$a_id]);
        }
        function delete_rec($table,$id,$idv,$pid){
                    $sql="SELECT * FROM ".$table." WHERE ".$pid."=".$idv;
                    $res=mysql_query($sql);
                    while ($sdata=mysql_fetch_assoc($res)){
                        $this->delete_rec($table,$id,$sdata[$id],$pid);
                    }

                    $sql="DELETE FROM ".$table." WHERE ".$id."=".$idv;
                    $res=mysql_query($sql);
        }
        function updateTree($data){

                    $table= (string)$this->c->table->name;
                    $id=    (string)$this->c->table->key->name;
                    $pid=   (string)$this->c->table->pkey->name;
                    $order= (string)$this->c->table->order->name;
                    $name=  (string)$this->c->table->text->name;

                    $a_id=    (string)$this->c->table->key->value;
                    $a_pid=   (string)$this->c->table->pkey->value;
                    $a_order= (string)$this->c->table->order->value;
                    $a_name=  (string)$this->c->table->text->value;

                    $sql="SELECT * FROM ".$table." WHERE ".$id."=".$data[$a_id];
                    $res=mysql_query($sql);
                    $sdata=mysql_fetch_assoc($res);

                    if (($sdata[$pid]!=$data[$pid])||($sdata[$order]!=$data[$a_order])) {
                         $sql="UPDATE ".$table." SET ".$order."=".$order."-1 WHERE ".$pid."=".$sdata[$pid]." AND ".$order." > ".$sdata[$order];
                         $nid=mysql_query($sql);
                         $sql="UPDATE ".$table." SET ".$order."=".$order."+1 WHERE ".$pid."=".$data[$a_pid]." AND ".$order." > ".($data[$a_order]-1);
                         $nid=mysql_query($sql);
                    }

                    $sql="UPDATE ".$table." SET ".$pid."=".$data[$a_pid].", ".$order."=".$data[$a_order].", ".$name."=\"".mysql_real_escape_string($data[$a_name])."\"";
                    $sql.=" WHERE ".$id."=".$data[$a_id];
                    $nid=mysql_query($sql);
                    //echo $sql;
                    $this->printAction("update",$data[$a_id],$data[$a_id]);
         }

         function update($data){
                 // sample of ADDRESS LINE LINK
                 // test/4/dhtmlxDataProcessor/server_code/PHP/update.php?gr_id=9&c0=d0&c1=d1&c2=d2&c3=d3&c4=d4&c5=d5&c6=d6&c7=d7

                 $C_STR = "#"; // value of UNIQUE CONNECT FIELD for current ROW
                 $u = $this->c->table[0]->connect_field; // $this->c->connect_field FROM table[0] ACCORDING TO pKey
                 $t = $this->c->table[0]->name;
                 $k_name = $this->c->table[0]->key->name;
                 $k_value = $data["gr_id"]; // row to modify by KEY
                 // DETECT SYMBS
                 $r = mysql_query("SELECT `$u` FROM `$t` WHERE `$k_name`='$k_value' LIMIT 1 ");
                 $out = mysql_fetch_object($r);
                 if (!empty($out->$u)) { $C_STR = $out->$u; }
                 mysql_free_result($r);
                 // update all tables
                 for ($q=0; $q<count($this->c->table); $q++) {
                         // getting UNIQUE CONNECT FIELD for current table
                         $u = $this->c->table[$q]->connect_field;
                         // check if updated row present
                         $t_name = $this->c->table[$q]->name;
                         $r = mysql_query("SELECT * FROM `$t_name` WHERE `$u`='$C_STR' LIMIT 1 ");
                         $is_present = false;
                         while ($out = mysql_fetch_object($r)) { $is_present = true; }
                         if ($is_present) {
                                 // preparing data for update
                                 $sql = "UPDATE `$t_name` SET ";
                                 for ($w=0; $w<count($this->c->table[$q]->param); $w++) {
                                         $d = $this->c->table[$q]->param[$w]->name;
                                         $v = $data[$this->c->table[$q]->param[$w]->value];
                                         if ($w>0) { $p = ","; } else { $p = ""; }
                                         $sql = $sql.$p."`$d`='$v'";
                                 }
                                 $sql = $sql." WHERE `$u`='".$C_STR."' LIMIT 1 ";
                                 mysql_query($sql);

                         } else {
                                 // create NEW in all tables where needed except table[0]
                                 $k_name = $this->c->table[$q]->key->name;
                                 $sql = "INSERT INTO `$t_name` (`$k_name`";
                                 for ($w=0; $w<count($this->c->table[$q]->param); $w++) {
                                         $d = $this->c->table[$q]->param[$w]->name;
                                         $sql = $sql.",`$d`";
                                 }
                                 $sql = $sql.",`$u`) VALUES (''";
                                 for ($w=0; $w<count($this->c->table[$q]->param); $w++) {
                                         $v = $data[$this->c->table[$q]->param[$w]->value];
                                         $sql = $sql.",'$v'";
                                 }
                                 $sql = $sql.",'".$C_STR."'); ";
                                 mysql_query($sql);
                         }
                 }
                 $this->printAction("update",$data[(string)$this->c->table[0]->key->value],0);
        }

        function insert($data){
                 // generation of UNIQUE field
                 $C_STR = md5(time()/rand(1,100));
                 // pass for all tables
                 for ($q=0; $q<count($this->c->table); $q++) {
                         $t_name = $this->c->table[$q]->name;
                         $u = $this->c->table[$q]->connect_field;
                         $k_name = $this->c->table[$q]->key->name;
                         $sql = "INSERT INTO `$t_name` (`$k_name`";
                         for ($w=0; $w<count($this->c->table[$q]->param); $w++) {
                                 $d = $this->c->table[$q]->param[$w]->name;
                                 $sql = $sql.",`$d`";
                         }
                         $sql = $sql.",`$u`) VALUES (''";
                         for ($w=0; $w<count($this->c->table[$q]->param); $w++) {
                                 $v = $data[$this->c->table[$q]->param[$w]->value];
                                 $sql = $sql.",'$v'";
                         }
                         $sql = $sql.",'".$C_STR."'); ";
                         //echo $sql."<br>";
                         mysql_query($sql);
                 }
                 $this->printAction("insert",$data[(string)$this->c->table[0]->key->value],mysql_insert_id());

        }

        function delete($data) {
                 // define KEY
                 $k_name = $this->c->table[0]->key->name;
                 $k_value = $data["gr_id"];
                 $C_STR = "#";
                 $u = $this->c->table[0]->connect_field;
                 $t_name = $this->c->table[0]->name;
                 // define UNIQUE field name and value
                 $r = mysql_query("SELECT `$u` FROM `$t_name` WHERE `$k_name`='$k_value' LIMIT 1 ");
                 while ($out = mysql_fetch_object($r)) { $C_STR = $out->$u; }
                 mysql_free_result($r);
                 // delete from all tables according
                 for ($q=0; $q<count($this->c->table); $q++) {
                         $t_name=$this->c->table[$q]->name;
                         $u = $this->c->table[$q]->connect_field;
                         mysql_query("DELETE FROM `$t_name` WHERE `$u`='$C_STR' LIMIT 1 ");
                 }
                 $this->printAction("delete",$data[(string)$this->c->table[0]->key->value],0);
        }

        function printXMLHeader($tag="data", $add=""){
                 header("Content-type:text/xml");
                 echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                 echo "<".$tag." ".$add." >";
                 $this->tag=$tag;
        }

        function printXMLFooter($tag="data"){
                 echo  "</".$this->tag.">";
        }

        function printAction($action,$sid,$tid){
                 echo  "<action type='".$action."' sid='".$sid."' tid='".$tid."'/>";
        }

        function getXML(){
                 switch((string)$this->c->type){
                         case "tree":
                                 $table= (string)$this->c->table->name;
                                 $id=    (string)$this->c->table->key->name;
                                 $pid=   (string)$this->c->table->pkey->name;
                                 $order= (string)$this->c->table->order->name;
                                 $name=  (string)$this->c->table->text->name;
                                 $userdata=""; //(string)$this->c->table["name"];
                                 $out=$this->getXMLTree($table, $id, $pid, $name, $userdata, 0, $order);
                                 echo $out;
                                 break;
                         default:
                                 return $this->getXMLGrid();
                                 break;
                         }
        }

        function getXMLTree($table, $id, $pid, $name, $userdata, $sid, $order){
                 $data=mysql_query("SELECT $id, $pid, $name".(($userdata=="")?"":(",".$userdata))." FROM ".$table." WHERE $pid=$sid ORDER BY ".$order);
                 $out="";
                 if ($data)
                         while($adata=mysql_fetch_assoc($data)) {
                                 $out.="<item id='".$adata[$id.""]."' text='".$adata[$name.""]."'>";
                                 $out.=$this->getXMLTree($table, $id, $pid, $name, $userdata, $adata[$id.""],$order);
                                 if ($userdata){ $out.="<userdata name='".$userdata."'>".$adata[$userdata.""]."</userdata>"; }
                                 $out.="</item>";
                         }
                 return $out;
        }

        function getXMLGrid() {
                 // field for connect rows is $this->c->connect_field
                 // first take all possible connected keys from table[0] // STRONGLY
                 $a = array(); // all unical values for selected elements will here
                 $i = $this->c->table[0]->connect_field;
                 $t = $this->c->table[0]->name;
                 $r = mysql_query("SELECT `$i` FROM `$t`");
                 while ($out = mysql_fetch_object($r)) { array_push($a,$out->$i); }
                 mysql_free_result($r);
                 // generating SQL's for every ROW
                 for ($q=0; $q<count($a); $q++) {
                         $connect_name = $this->c->table[0]->connect_field;
                         $k_name = $this->c->table[0]->key->name;
                         $t_name = $this->c->table[0]->name;
                         $i_value=$a[$q];
                         $res = mysql_query("SELECT `$k_name` FROM `$t_name` WHERE `$connect_name`='$i_value' ");
                         $o = mysql_fetch_object($res);
                         if (!empty($o)) {
                                 $k=$o->$k_name;
                                 // generating ROWs form all tables according $a[$q] - unic field
                                 echo "   <row id=\"".($k)."\">\n";
                                 for ($w=0; $w<count($this->c->table); $w++) {
                                         $connect_name = $this->c->table[$w]->connect_field;
                                         $t_name=$this->c->table[$w]->name;
                                         $where_add="";
                                         if ((string)$this->c->table->where!="") { $where_add=", ".(string)$this->c->table->where; }
                                         $r = mysql_query("SELECT * FROM `$t_name` WHERE `$connect_name`='$i_value' ".$where_add);
                                         $out = mysql_fetch_object($r);
                                         if (!empty($out)) {
                                                 for ($p=0; $p<count($this->c->table[$w]->param); $p++) {
                                                         $h = $this->c->table[$w]->param[$p]->name;
                                                         echo "      <cell>".$out->$h."</cell>\n";
                                                 }
                                         }
                                         mysql_free_result($r);
                                 }
                                 echo "   </row>\n";
                         }
                         mysql_free_result($res);
                 }
        }
}
?>
