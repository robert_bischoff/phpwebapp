-- MySQL dump 10.9
--
-- Host: localhost    Database: sampleDB
-- ------------------------------------------------------
-- Server version	4.1.13a-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table grid`
--

DROP TABLE IF EXISTS grid;
CREATE TABLE grid (
  `pKey` int(11) NOT NULL auto_increment,
  data1 varchar(100) default NULL,
  data3 varchar(100) default NULL,
  data2 varchar(100) default NULL,
  data4 varchar(100) default NULL,
  data5 varchar(100) default NULL,
  data6 varchar(100) default NULL,
  data7 varchar(100) default NULL,
  data8 varchar(100) default NULL,
  PRIMARY KEY  (pKey)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table grid
--


/*!40000 ALTER TABLE grid DISABLE KEYS */;
LOCK TABLES grid WRITE;
INSERT INTO grid VALUES ('1','-1500','A Time to Kill','John Grisham','12.99','1','24','0','05/01/1998'),('10','-100','Boris Godunov','Alexandr Pushkin','7.15','1','1','0','01/01/1999'),('11','-150','Alice in Wonderland','Lewis Carroll','6.15','1','1','0','01/01/1999'),('2','1000','Blood and Smoke','Stephen King','1','1','24','0','01/01/2000'),('3','-200','The Rainmaker','John Grisham','7.99','0','48','0','12/01/2001'),('4','350','The Green Mile','Stephen King','11.10','1','24','0','01/01/1992'),('5','700','Misery','Stephen King','7.70','0','na','0','01/01/2003'),('6','-1200','The Dark Half','Stephen King','0','0','48','0','10/30/1999'),('7','1500','The Partner','John Grisham','12.99','1','48','1','01/01/2005'),('8','500','It','Stephen King','9.70','0','na','0','10/15/2001'),('9','400','Cousin Bette','Honore de Balzac','0','1','1','0','12/01/1991');
UNLOCK TABLES;
/*!40000 ALTER TABLE grid ENABLE KEYS */;

--
-- Table structure for table `tree`
--

DROP TABLE IF EXISTS `tree`;
CREATE TABLE `tree` (
  `item_id` int(10) unsigned NOT NULL auto_increment,
  `item_nm` varchar(200) default '0',
  `item_order` int(10) unsigned default '0',
  `item_desc` text,
  `item_parent_id` int(10) unsigned default '0',
  PRIMARY KEY  (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tree`
--


/*!40000 ALTER TABLE `tree` DISABLE KEYS */;
LOCK TABLES `tree` WRITE;
INSERT INTO `tree` VALUES (2,'New Item222',0,'',0),(6,'New Item',0,'',2),(7,'New Item 0',0,'',6),(12,'New Item 1',1,'',15),(13,'New Item 2',1,'',6),(14,'New Item x',0,'',15),(15,'New Item 0',2,'',6),(16,'New Item 1',3,'',6),(18,'New Item',2,'',15),(20,'New Item',1,'',0),(21,'New Item3',1,'',2),(22,'New Item2',2,'',2),(23,'New Item x',1,'',25),(24,'New Item x2',2,'',0),(25,'New Item 2',0,'',22),(26,'New Item xxxx',0,'',25),(27,'New Itemxxx4',3,'',25),(28,'New Item 2',2,'',25);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tree` ENABLE KEYS */;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

