<?php
/*
Copyright Scand LLC http://www.scbr.com
This version of Software is free for using in non-commercial applications.
For commercial use please contact info@scbr.com to obtain license
*/

    error_reporting(E_ALL ^ E_NOTICE);
    class uDataLink{
        protected $c;
        protected $tag;
        function  uDataLink($config){
            $this->c=simplexml_load_file($config);
        }
        function save($data){
            $this->printXMLHeader(data);
            if (!isset($data[(string)$this->c->action]))
                $data[(string)$this->c->action]=""; //echo (string)$this->c["type"];
            if (((string)$this->c["type"])=="tree"){
              switch($data[(string)$this->c->action]){
                  case 'deleted':
                      $this->deleteTree($data);
                      break;
                  case 'inserted':
                      $this->insertTree($data);
                      break;
                  default:
                      $this->updateTree($data);
                      break;
              }
            }
            else{
              switch($data[(string)$this->c->action]){
                  case 'deleted':
                      $this->delete($data);
                      break;
                  case 'inserted':
                      $this->insert($data);
                      break;
                  default:
                      $this->update($data);
                      break;
              }

            }
            $this->printXMLFooter();
        }

        function insertTree($data){
                    $table= (string)$this->c->table["name"];
                    $id=    (string)$this->c->table->key["name"];
                    $pid=   (string)$this->c->table->pkey["name"];
                    $order= (string)$this->c->table->order["name"];
                    $name=  (string)$this->c->table->text["name"];

                    $a_table= (string)$this->c->table;
                    $a_id=    (string)$this->c->table->key;
                    $a_pid=   (string)$this->c->table->pkey;
                    $a_order= (string)$this->c->table->order;
                    $a_name=  (string)$this->c->table->text;


                    $sql="UPDATE ".$table."
                            SET ".$order."=".$order."+1
                            WHERE ".$pid."=".$data[$a_pid]."
                            AND ".$order." > ".($data[$a_order]-1);
                    $nid=mysql_query($sql);

                    $sql="INSERT INTO ".$table."(".$pid.",".$order.",".$name.")
                            VALUES(".$data[$a_pid].",".$data[$a_order].",\"".mysql_real_escape_string($data[$a_name])."\")";
                    $res=mysql_query($sql);
                    //echo $sql;
                    $this->printAction("insert",$data[$a_id],mysql_insert_id());
        }
        function deleteTree($data){
                    $table= (string)$this->c->table["name"];
                    $id=    (string)$this->c->table->key["name"];
                    $pid=   (string)$this->c->table->pkey["name"];
                    $order= (string)$this->c->table->order["name"];
                    $name=  (string)$this->c->table->text["name"];

                    $a_id=    (string)$this->c->table->key;

                    $sql="SELECT * FROM ".$table." WHERE ".$id."=".$data[$a_id];
                    $res=mysql_query($sql);
                    $sdata=mysql_fetch_assoc($res);

                    $this->delete_rec($table,$id,$data[$a_id],$pid);

                    $sql="UPDATE ".$table."
                            SET ".$order."=".$order."-1
                            WHERE ".$pid."=".$sdata[$pid]."
                            AND ".$order." > ".$sdata[$order];
                    $nid=mysql_query($sql);
                    $this->printAction("delete",$data[$a_id],$data[$a_id]);
        }
        function delete_rec($table,$id,$idv,$pid){
                    $sql="SELECT * FROM ".$table." WHERE ".$pid."=".$idv;
                    $res=mysql_query($sql);
                    while ($sdata=mysql_fetch_assoc($res)){
                        $this->delete_rec($table,$id,$sdata[$id],$pid);
                    }

                    $sql="DELETE FROM ".$table." WHERE ".$id."=".$idv;
                    $res=mysql_query($sql);


                    //echo $sql;
        }
        function updateTree($data){

                    $table= (string)$this->c->table["name"];
                    $id=    (string)$this->c->table->key["name"];
                    $pid=   (string)$this->c->table->pkey["name"];
                    $order= (string)$this->c->table->order["name"];
                    $name=  (string)$this->c->table->text["name"];

                    $a_table= (string)$this->c->table;
                    $a_id=    (string)$this->c->table->key;
                    $a_pid=   (string)$this->c->table->pkey;
                    $a_order= (string)$this->c->table->order;
                    $a_name=  (string)$this->c->table->text;

                    $sql="SELECT * FROM ".$table." WHERE ".$id."=".$data[$a_id];
                    $res=mysql_query($sql);
                    $sdata=mysql_fetch_assoc($res);

                    if (($sdata[$pid]!=$data[$pid])||($sdata[$order]!=$data[$a_order])){
                    $sql="UPDATE ".$table."
                            SET ".$order."=".$order."-1
                            WHERE ".$pid."=".$sdata[$pid]."
                            AND ".$order." > ".$sdata[$order];
                    $nid=mysql_query($sql);

                    $sql="UPDATE ".$table."
                            SET ".$order."=".$order."+1
                            WHERE ".$pid."=".$data[$a_pid]."
                            AND ".$order." > ".($data[$a_order]-1);
                    $nid=mysql_query($sql);
                    }

                    $sql="UPDATE ".$table."
                            SET ".$pid."=".$data[$a_pid].",
                                ".$order."=".$data[$a_order].",
                                ".$name."=\"".mysql_real_escape_string($data[$a_name])."\"";
                    $sql.=" WHERE ".$id."=".$data[$a_id];
                    $nid=mysql_query($sql);
                   // echo $sql;
                    $this->printAction("update",$data[$a_id],$data[$a_id]);
            }
        function  update($data){
            $sql="UPDATE ".$this->c->table["name"]." SET ";
            $count=0;
            foreach ($this->c->table->param as $param){
                if ($count) $sql.=",";
                $sql.=$param["name"]." = \"".mysql_real_escape_string($data[(string)$param])."\"";
                $count++;
                }
            $sql.=" WHERE ".$this->c->table->key["name"]."=\"".mysql_real_escape_string($data[(string)$this->c->table->key])."\"";
            mysql_query($sql);
            $this->printAction("update",$data[(string)$this->c->table->key],0);
        }
        function  insert($data){
            $strA="";
            $strB="";
            $count=0;
            foreach ($this->c->table->param as $param){
                if ($count) {
                        $strA.=",";
                        $strB.=",";
                    }
                $strA.=$param["name"];
                $strB.="\"".mysql_real_escape_string($data[(string)$param])."\"";
                $count++;
                }
            $sql="INSERT INTO  ".$this->c->table["name"]." (".$strA.") VALUES ( ".$strB." ) ";
            mysql_query($sql);
            $this->printAction("insert",$data[(string)$this->c->table->key],mysql_insert_id());
        }
        function  delete($data){
            $sql="DELETE FROM ".$this->c->table["name"]." WHERE ".$this->c->table->key["name"]."=\"".mysql_real_escape_string($data[(string)$this->c->table->key])."\"";
            mysql_query($sql);
            $this->printAction("delete",$data[(string)$this->c->table->key],0);
        }

        function printXMLHeader($tag="data",$add=""){
            header("Content-type:text/xml");
            echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            echo  "<".$tag." ".$add." >";
            $this->tag=$tag;
        }
        function printXMLFooter($tag="data"){
            echo  "</".$this->tag.">";
        }
        function printAction($action,$sid,$tid){
            echo  "<action type='".$action."' sid='".$sid."' tid='".$tid."'/>";
        }
        function getXML(){
            switch((string)$this->c["type"]){
                case "tree":
                    $id=    (string)$this->c->table->key["name"];
                    $pid=   (string)$this->c->table->pkey["name"];
                    $order= (string)$this->c->table->order["name"];
                    $table= (string)$this->c->table["name"];
                    $name=  (string)$this->c->table->text["name"];

                    $userdata="";//(string)$this->c->table["name"];
                    $out=$this->getXMLTree($table, $id, $pid, $name, $userdata, 0, $order);
                    echo $out;
                    break;
                default:
                    return $this->getXMLGrid();
                    break;
            }
        }

        function getXMLTree($table, $id, $pid, $name, $userdata, $sid, $order){
            $data=mysql_query("SELECT $id, $pid, $name".(($userdata=="")?"":(",".$userdata))."
                                FROM ".$table."
                                WHERE $pid=$sid ORDER BY ".$order);
            $out="";
            if ($data)
            while($adata=mysql_fetch_assoc($data)){
                $out.="<item id='".$adata[$id.""]."' text='".$adata[$name.""]."'>";
                $out.=$this->getXMLTree($table, $id, $pid, $name, $userdata, $adata[$id.""],$order);
                if ($userdata){
                    $out.="<userdata name='".$userdata."'>".$adata[$userdata.""]."</userdata>";
                }
                $out.="</item>";
            }
            return $out;
        }

        function getXMLGrid(){
            $where=(string)$this->c->table->where;
            $sql="SELECT * FROM ".$this->c->table["name"];
            if ($where!="")
                $sql.=" WHERE ".$where;
            $res=mysql_query($sql);
			if ($res)
                while ($data=mysql_fetch_assoc($res)){
                    echo "<row id=\"".$data[(string)$this->c->table->key["name"]]."\">";
                          foreach ($this->c->table->param as $param)
                            echo "<cell>".$data[(string)$param["name"]]."</cell>";
                    echo "</row>";
                }
        }
    }
?>
