<?php
/*
Copyright Scand LLC http://www.scbr.com
This version of Software is free for using in non-commercial applications.
For commercial use please contact info@scbr.com to obtain license
*/
error_reporting(E_ALL ^ E_NOTICE);

    require_once("uDataLink.php");
    require_once("db.php");

    if (array_key_exists('ctrl',$_GET)) {

        switch($_GET['ctrl']){
            case "tree":
                    //print data as XML
                    $temp=new uDataLink("tree_data.json");
                    $temp->printXMLHeader("tree","id='0'");
                    break;
            default:
                    // print data as XML
                    // $temp=new uDataLink("data.json");
                    // for some tables
                    $temp=new uDataLink("greed_test.json");
                    $temp->printXMLHeader("rows");
                    break;
        }
    } else {
          // as default in "case"
          // $temp=new uDataLink("data.json");
          // for some tables
          $temp=new uDataLink("greed_test.json");
          $temp->printXMLHeader("rows");
    }
    $temp->getXML();
    $temp->printXMLFooter();

?>
