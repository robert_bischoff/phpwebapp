<?php
include_once 'functions.php';
sec_session_start();

// Unset all session values
$_SESSION = array();

// get session parameters
$params = session_get_cookie_params();

// Delete the actual cookie.
setcookie(session_name(),
'', time() - 42000,
$params["path"],
$params["domain"],
$params["secure"],
$params["httponly"]);

// Destroy session
session_destroy();
header('Location: ../index.php');
/**
  session_start();
  require_once('config.php');
  $connection = mysql_connect($DBhost,$DBuser,$DBpass);
  @mysql_select_db("$DBname");
  $nowtime = date_at_timezone("Y-m-d H:i:s", "Asia/Seoul", $t);
  $sqlact="INSERT INTO `watchdog_action_log` VALUES (NULL, '".$_SESSION['username']."', '2', 'User logged out', '".$_SERVER['REMOTE_ADDR']."', '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."', '".$nowtime."')";
  mysql_query($sqlact);	  
  session_unset();
  session_destroy();  
  header("Location: ../index.php");
**/
?>