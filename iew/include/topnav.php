<?php
  //print_r($_SESSION);
  error_reporting(0);
  include 'config.php';
  
  $var1 = ""; $var2 = ""; $var3 = "$_SESSION[useraccesslevel]";
  $query = "SELECT * FROM topnav WHERE (level1!=? && level2=? && level<=?)";
  
  $stmt = $conn->prepare($query);
  $stmt->bind_param('sss', $var1, $var2, $var3);
  $stmt->execute();
  
  $result = $stmt->get_result();
    
  $num_rows = $result->num_rows;
  //print_r($num_rows);
?>
<link href="lib/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<?php if ($num_rows !=0 ) : ?>
 
 <ul class="nav nav-tabs">   
 <?php 
    for($i=0;$i<$num_rows;$i++):
      $row = $result->fetch_assoc();
      $menuid = strtoupper($row['id']);
      $link = strtoupper($row['link']);
      $url = $row['url'];
      $target = $row['target'];
      $active = $row['active'];

         
        if ($active!='0') :
                             	  
          $query1 = "SELECT * FROM topnav WHERE (level1=? && level2!=? && level<=?) ORDER BY 'order' ASC";
          $var1 = $link; $var2 = ''; $var3 = $_SESSION[useraccesslevel];
          $stmt1 = $conn->prepare($query1);
          $stmt1->bind_param('sss', $var1, $var2, $var3);
          $stmt1->execute();
          
          $result1 = $stmt1->get_result();
          $num_rows1 = $result1->num_rows;
?>     
        
        <?php       
            if ($num_rows1!=0) : 
        ?>
        
                <li role = "nav" class = "dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo "#".$url;?>" role="tab" aria-expanded="false">
                    <?php echo $link?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" >
            <?php 
                for($j=0;$j<$num_rows1;$j++) :
                    $row1 = $result1->fetch_assoc();
                    $link1 = strtoupper($row1['link']);
                    $url1 = $row1['url'];
                    $target1 = $row1['target'];
                    $active1 = $row1['active'];
              
                    
                    if($active1!='0') : 
            ?>
                            
                            <li><a href='<?php echo $url1 ?>'><?php echo $link1 ?></a></li>
                            
                        <?php endif;?>
                    <?php endfor; ?>
                    </ul>    
        <?php else: ?>
        <li><a href="<?php echo $url; ?>"><?php echo $link; ?></a></li>       
        <?php endif;?>  

                             
    
    <?php endif;?>
                			  
  <?php endfor;?>
</ul>
<?php endif;?>
<?php 
  $stmt->close();
  $stmt1->close();
?>      

