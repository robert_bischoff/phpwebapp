<?php
include_once 'include/functions.php';
include_once 'include/header.php';
session_start();
 
if (login_check($conn) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>IEW Ticket System</title>
    <link rel="stylesheet" href="include/lib/css/main.css" type="text/css" />
        <script type="text/javascript">
          function changeColor(color, ID) {
        	document.getElementById(ID).bgColor = color;
          }
        </script>
        <script type="text/JavaScript" src="../dist/js/sha512.js"></script> 
        <script type="text/JavaScript" src="../dist/js/forms.js"></script>
<style type="text/css">
#login {
    width: 15em;  height: 2em;
}        
</style>
        
    </head>
<body>
<?php
    
    if (isset($_GET['error'])) {
        echo '<p class="error">Error Logging In!</p>';
    }
?>

<table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" style="position:absolute; top: 72px; left: 16px;" width=600>
  <tr bgcolor="#f1f1d8">
    <td style="font-weight:bold;"><?php echo strtoupper($sitename); ?></td>
  </tr>
  <tr bgcolor="#ffffff" valign="top">
    <td width="100%">
      <table height="200" width="100%">
      <tr valign="top">
      <td>
            
      <table align="left" bgcolor="#ffffff" cellpadding="3" cellspacing="0" border="0" width=100%>    
        <tr bgcolor="#ffffff" valign="top"><td width="100%"><b>Welcome to <?php echo strtoupper($sitename); ?>!</b><br /></td></tr>
        <tr bgcolor="#ffffff" valign="top"><td width="100%">If you are a new user to the <?php echo $sitename; ?> system please click the <b>"create new account"</b> button and fill out the short form.  If you are having difficultly setting up an account, or have questions about the <?php echo $sitename; ?> system, please call 754-3135 for help.</td></tr>
        <tr bgcolor="#ffffff" valign="top"><td width="100%" style='color:#990000'>*<i>All accounts <u>must be reviewed</u> before activation.</i><br /><br /></td></tr>
        <tr><td>
        <table align="center" cellpadding="7" cellspacing="7">
        <tr valign="top" id="btn0" onmouseover="changeColor('#999999', this.id);" onmouseout="changeColor('#ffffff', this.id);">
          <td align="center"><a href="register.php"><img src="images/createnewaccount.jpg" border="0" /></a></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr bgcolor="#ffffff" valign="top"><td align="right" width="100%" style='color:#cccccc;font-size:9px;'>vvr <?php echo date('sHmidy'); ?></td></tr>        
      </table>
      </td>
      </tr>
      </table>
    </td>
  </tr>
</table>



<table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" style="position:absolute; top:72px; left: 625px;" width=350>
  <tr bgcolor="#ffff99">
    <td style="font-weight:bold">Login</td>
  </tr>
  <tr bgcolor="#ffffff" valign="top">
    <td width="100%">
		<?php // if(!isset($_SESSION['loginstatus'])):?>
       
        <form method='post' action='include/checklogin.php' name='login_form'>
          <input name='action' type='hidden' value='check'>
          <table border='0' width=95% align='center' cellpadding='2' cellspacing='0' bgcolor='#ffffff'>
            <tr>
              <td align='right' width='20%'>Username:</td>
              <td align='left'><input class='text' style='width:99%;background-color:#FFF9D9;' type='text' name='username' /></td>
            </tr>
            <tr>
              <td align='right' width='20%'>Password:</td>
              <td align='left'><input class='text' style='width:99%;background-color:#FFF9D9;' type='password' name='password' /></td>		
            </tr>
            <tr>	  
              <td align='right' colspan='2'>
              <input type="button" id="login" style="background-color:#dddddd;" value="Login" onclick="formhash(this.form, this.form.password);">
              </td>
            </tr>
            <tr><td align=right colspan=2 style='font-size:11px'>&nbsp;</td></tr>
          </table>
        </form>
        
        <?php //else: ?>
        <!--  <form name='login' method='post' action='include/checklogin.php' enctype='multipart/form-data'>
          <input name='action' type='hidden' value='check'/>
          <table border='0' width=95% align='center' cellpadding='2' cellspacing='0' bgcolor='#ffffff'>
            <tr>
              <td align='right' width='20%'>Username:</td>
              <td align='left'><input class='text' style='width:99%;background-color:#FEEEEE;' type='text' name='username' /></td>
            </tr>
            <tr>
              <td align='right' width='20%'>Password:</td>
              <td align='left'><input class='text' style='width:99%;background-color:#FEEEEE;' type='password' name='pass'/></td>		
            <tr>	  
              <td align='center' colspan='2'><input type='submit' style='background-color:#dddddd;'  value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'/></td>
            </tr>
            <tr><td align=right colspan=2 style='font-size:11px'>&nbsp;</td></tr>
          </table>
        </form>
        -->
        <?php
        if (login_check($conn) == true)
        {
            echo '<p>Currently logged ' . $logged . ' as ' . htmlentities($_SESSION['username']) . '.</p>'; 
            echo '<p>Do you want to change user? <a href="includes/logout.php">Log out</a>.</p>';
        } else
        {
            echo '<p>Currently logged ' . $logged . '.</p>';
            echo "<p>If you don't have a login, please <a href='register.php'>register</a></p>";
        }
        ?>      
        
        <?php
        // session_unset();
        // session_destroy();
        ?>   
         
        <?php //endif;?>
        
    </td>
  </tr>
</table>

<table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" style="position:absolute; top: 316px; left: 16px;" width=600>
  <tr bgcolor="#f1f1d8">
    <td style="font-weight:bold;">&nbsp;</td>
  </tr>
  <tr bgcolor="#ffffff" valign="top">
    <td width="100%">
      <table align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" width=570 style="padding-top:10px;padding-bottom:10px;height:159px">
        <tr valign="top">
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table align="left" bgcolor="#333333" cellpadding="3" cellspacing="1" border="0" style="position:absolute; top:225px; left: 625px;" width=350>
  <tr bgcolor="#f1f1d8">
    <td style="font-weight:bold;color:#000000;">&nbsp;</td>
  </tr>
  <tr bgcolor="#eeeeee" valign="top" align="center">
    <td width="100%">
      <table align="center" bgcolor="#333333" cellpadding="12" cellspacing="0" border="0" width=100% height="250">
	    <tr bgcolor="#eeeeee" valign="top">
		  <td nowrap="nowrap">
            <table bgcolor="#eeeeee" cellpadding="7" cellspacing="7" border="0">
              <tr valign="top" id="btn1" onmouseover="changeColor('#999999', this.id);" onmouseout="changeColor('#eeeeee', this.id);">
		        <td nowrap="nowrap"> </td>
              </tr>                                         
            </table>
          </td>                 
	    </tr>
      </table>      
    </td>
  </tr>  
</table>

</body>
</html>
