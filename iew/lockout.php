<?php include "include/config.php"; ?>
<table width="99%" height="80%">
  <tr>
    <td>
      <table border="0" cellpadding="3" cellspacing="3" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold;">
        <tr>
          <td align="center">Due to page inactivity you have been logged out of the <?php echo $sitename; ?> system.</td>
        </tr>
        <tr>
          <td align="center">Please click on the link below to login.  Thank you.</td>
        </tr>
        <tr>
          <td align="center"><br></td>
        </tr>               
        <tr>
          <td align="center"><a href='include/logout.php' target="_top"><img src='images/login.jpg'></a></td>
        </tr>        
      </table>
    </td>
  </tr>
</table>