<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
<script src="https://apis.google.com/js/client:platform.js" async defer></script>
<script type="text/JavaScript" src="dist/js/forms.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The Bischoffs</title>

<!-- Bootstrap -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<style>
h1 {text-align: center;}
p {text-align: center;}
</style>
</head>
<body role="document">
   
   <?php include 'includes/navbar.php';?>
    <br>
	<br>


<?php print_r($_SERVER); ?>
	<div class="container" role="main">
		
    
		<!-- Main jumbotron for a primary marketing message or call to action -->
		<div class="jumbotron">

			<h1>The Bischoff Family</h1>
			<p>Welcome to the Bischoff Family Website.</p>
			<p>Please check our blog in the link above to see how things are
				going.</p>
			<p>You will find pictures, updates, news and more.</p>
			<p>You will need a wordpress account to view the blog.</p>
			<p>Check the Contact Page for our contact info.</p>


		</div>


		<div class="page-header">
			<h1>The Bischoffs</h1>
		</div>


		<img src="includes/img/DSC_0155.JPG" class="img-thumbnail"
			alt="A generic 
           square placeholder image with a white border around it, making it 
           resemble a photograph taken with an old instant camera">
	</div>
	<br>
	<br>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="dist/js/jquery-1.11.2.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="dist/js/bootstrap.js"></script>

</body>
</html>
