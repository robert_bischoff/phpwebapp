<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
<script src="https://apis.google.com/js/client:platform.js" async defer></script>
<script type="text/JavaScript" src="dist/js/forms.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The Bischoffs</title>

<!-- Bootstrap -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<style>
h1 {text-align: center;}
p {text-align: center;}
</style>
</head>
<body role="document">
   
   <ul class="nav nav-tabs" id="supernav">
    <li id="page1"><a href="/page1.html"><i class="icon-home" style="margin-top:4px;"></i> Page 1</a></li>
    <li class="dropdown" id="page2">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Page 2 <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li id="page2_home"><a href="/page2.html">Home</a></li>
            <li class="divider"></li>
            <li id="page2_2"><a href="/page2.2.html">Page 2.2</a></li>
            <li id="page2_3"><a href="/page2.3.html">Page 2.3</a></li>
        </ul>
    </li>
    <li class="dropdown" id="page3">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Page 3 <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li id="page3_home"><a href="/page3.html">Home</a></li>
            <li class="divider"></li>
            <li class="dropdown-submenu" id="page3_2">
                <a href="/page3.2.html">Page 3.2</a>
                <ul class="dropdown-menu">
                    <li id="page3_2_1"><a href="/page3.2.1.html">Page 3.2.1</a></li>
                    <li id="page3_2_2"><a href="/page3.2.2.html">Page 3.2.2</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li id="page4"><a href="/page4.html">Page 4</a></li>
</ul>

	<script src="dist/js/jquery-1.11.2.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="dist/js/bootstrap.js"></script>

</body>
</html>